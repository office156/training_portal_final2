import mongoose from "mongoose";
const { Schema } = mongoose;

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: true,
    },
    email: {
      type: String,
      trim: true,
      required: true,
      unique: true,
    },
    institute: {
      type: String,
      required: true,
    },
    address: {
      type: String,
      trim: true,
      required: true,
    },
    phone: {
      type: Number,
      trim: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
      min: 8,
      max: 64,
    },
    hodemail: {
      type: String,
      trim: true,
      required: true,
    },
    eduqua: {
      type: String,
      required: true,
    },
    domain: {
      type: String,
      required: true,
    },
    photo: {
      type: String,
    },
    aadharcard: {
      type: String,
    },
    isApproved: {
      type: Boolean,
      default: false,
    },
    role: {
      type: [String],
      default: ["Learner"],
      enum: ["Learner", "Instructor"],
    },
    stripe_account_id: "",
    stripe_seller: {},
    stripeSession: {},
    // passwordResetCode: {
    //   data: String,
    //   default: "",
    // },
    enrolledCourses: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Course"
    }]
  },
  { timestamps: true }
);

export default mongoose.model("User", userSchema);
