import mongoose from "mongoose";
const { Schema } = mongoose;

const adminSuggestionSchema = new mongoose.Schema({
    lesson: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Lesson",
      required: true,
    },
    text: {
      type: String,
      required: true,
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
});
  
export default mongoose.model("AdminSuggestionSchema", adminSuggestionSchema);