import React, { useEffect, useState } from 'react'
import SidebarInstructor from '../../Components/SidebarInstructor/SidebarInstructor'
import axios from 'axios';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { ToastContainer, toast } from 'react-toastify';

  

export default function CertificateRequests() {
    const [rows, setRows] = useState([]);
    const [requestRows, setRequestRows] = useState([]);
    const userFromSessionStorage = sessionStorage.getItem('InstructorId');
    const token = sessionStorage.getItem('Token');
    const [selectedCourse , setSelectedCourse] = useState("");
    const [showRequest , setShowRequest ] = useState(false);
    useEffect(() => {
        // fetchRequests();
        fetchAllCourses();
      }, []);


      const fetchAllCourses = () =>{
        const url = "http://localhost:8000/api/course/instructor/" + userFromSessionStorage;
        // console.log(url);
        // console.log(userFromSessionStorage);
        axios.get(url).then((response) => {
          const filteredResults = response.data.courses.filter(item =>
            item.adminApproval === true
          );
          setRows(filteredResults);
        })
          .catch((error) => {
            // console.log(error)
            // console.log(error.response.data)
            // alert()
          });
      }

    const fetchRequests = async(course)=>{
        
        const url2 = "http://localhost:8000/api/user/" + userFromSessionStorage + "/" + course + "/instructor/generate-certificate-requests";
        await axios.get(url2,{
            headers: {
              'Authorization': `Bearer ${token}`,
            }
          }).then((response) => {
           //   console.log(response.data)
            //  setRequestRows(response.data.certificateRequests)
            //  console.log(response.data.certificateRequests)

             const filteredResults = response.data.certificateRequests.filter(item =>
              item.status === "pending"
            );
            setRequestRows(filteredResults);
           })
             .catch((error) => {
               // console.log(error)
               // console.log(error.response.data)
               // alert()
             });
    }

    const approveRequest = (id)=>{
        console.log("clicked on approve")
        const data = { "name" : "name", "description" : "description"};
        const url2 = "http://localhost:8000/api/instructor/" + id + "/approve-certificate";
        axios.post(url2,data , {
            headers: {
              'Authorization': `Bearer ${token}`,
            }
          }).then((response) => {
            //  console.log(response.data.message)
            fetchRequests()
             toast.success(response.data.message)
           })
             .catch((error) => {
               // console.log(error)
               // console.log(error.response.data)
               // alert()
             });
    }

    const rejectRequest = (id)=>{
        console.log("outside")
        const url2 = "http://localhost:8000/api/instructor/" + id + "/disapprove-certificate";
        const data = { "name" : "name", "description" : "description"};
         axios.post(url2,data,{
            headers: {
              'Authorization': `Bearer ${token}`,
            }
          }).then((response) => {
            //  console.log(response.data.message)
             fetchRequests()
             toast.error(response.data.message)
           })
             .catch((error) => {
               // console.log(error)
               // console.log(error.response.data)
               // alert()
             });
    }


  return (
    <div className='userListTop'>
    <SidebarInstructor className="adminPanelSidebartag"></SidebarInstructor>
    <div className="userListSecond">
      <div className="topLineUserLIstPage py-5 container d-flex justify-content-between">
        <div className="headingUserList">
          Courses
        </div>
      </div>




<div className="tableUsers container d-flex justify-content-center">
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell align="right">Requests</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {(rows).map((row) => (
                  <TableRow
                    key={row._id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      <span>{row.name}</span>
                    </TableCell>
                    <TableCell align="right">
                <button onClick={()=>{
                  if(!showRequest){
                    fetchRequests(row._id)
                    setShowRequest(!showRequest);
                  }
                }}>
                <i class="fa-solid fa-chevron-down"></i>
                </button>
              </TableCell>

                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>


        { showRequest && <div className='container mt-5'>
<TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Course</TableCell>
            <TableCell align="left">User</TableCell>
            <TableCell align="left">Status</TableCell>
            <TableCell align="right">Approve</TableCell>
            <TableCell align="right">Reject</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {requestRows.map((row) => (
            <TableRow
              key={row._id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.course.name}
              </TableCell>
              <TableCell align="left">{row.user.name}</TableCell>
              <TableCell align="left">{row.status}</TableCell>
              <TableCell align="right">
                <button onClick={()=>{
                    console.log("inside")
                    approveRequest(row._id)
                }}>
                <i class="fa-solid fa-check"></i>
                </button>
              </TableCell>
              <TableCell align="right">
                <button onClick={()=>{
                    rejectRequest(row._id)
                }}>
                <i class="fa-solid fa-xmark"></i>
                </button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
</div> 
}

      </div>
      <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
      </div>
  )
}
