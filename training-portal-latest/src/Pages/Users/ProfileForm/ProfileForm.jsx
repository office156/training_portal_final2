import React from 'react'
import "./ProfileForm.css"
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { ToastContainer , toast } from 'react-toastify';
import axios from 'axios';

export default function ProfileForm(props) {


  const navigate = useNavigate();
  const [name , setName] = useState(props.name);
  const [institute , setInstitue] = useState(props.institute);
  
  const [email , setemail] = useState(props.email);
  const [hodemail , sethodemail] = useState(props.hodemail);
  const [password , setpassword] = useState("");
  const [phone , setphone] = useState(props.phone);
  const [eduqua , seteduqua] = useState(props.eduqua);
  const [domain , setdomain] = useState(props.domain);
  const [address , setaddress] = useState(props.address);

  const handleSubmit = (event) => {
  event.preventDefault();

    const data1 = { "name" : name, "email" : email , "institute":institute , "address":address , "phone":phone , "hodemail":hodemail , "eduqua":eduqua , "domain":domain};
    const userFromSessionStorage = sessionStorage.getItem('userId');
    const url = "http://localhost:8000/api/admin/user/details/update/" + userFromSessionStorage
  axios.patch(url, data1).then((response) => {
      // console.log(response)
      setName(response.data.name);
      setemail(response.data.email);
      setInstitue(response.data.institute);
      setaddress(response.data.address);
      setphone(response.data.phone);
      props.setname(response.data.name)
      sethodemail(response.data.hodemail);
      seteduqua(response.data.eduqua);
      setdomain(response.data.domain);
      // setName(response.data.name);
      // setemail(response.data.email);
      // setInstitue(response.data.institute);
      // setaddress(response.data.address);
      // setphone(response.data.phone);

      // sethodemail(response.data.hodemail);
      // seteduqua(response.data.eduqua);
      // setdomain(response.data.domain);

      toast.success("Details updated successfully")

    })
    .catch((error)=>{
      console.log(error)
      // console.log(error.response.data)
      // alert()
      // toast.error(error.response.data)

    });
  }

  return (

      <div className='UserProfileDetailsTop pt-5'>
        <h1 className='text-center ResetPageHeading'>Profile Details</h1>
        <form >
            <div className="inputFields">
            <label htmlFor="current-password">Name</label>
            <br />
            <input type="text" placeholder={name}  onChange={e => setName(e.target.value)} className='userPasswordResetInputs' name="current-password" id="current-password" />
            </div>
            <div className="inputFields">
            <label htmlFor="new-password">Email</label>
            <br />
            <input type="text"  placeholder={email}  onChange={e => setemail(e.target.value)} className='userPasswordResetInputs' name="current-password" id="new-password" />
            </div>
            <div className="inputFields">
            <label htmlFor="confirm-password">Phone</label>
            <br />  
            <input type="number" placeholder={phone}  onChange={e => setphone(e.target.value)} className='userPasswordResetInputs' name="current-password" id="confirm-password" />
            </div>
            <div className="inputFields">
            <label htmlFor="confirm-password">Institute</label>
            <br />
            <input type="text"  placeholder={institute}  onChange={e => setInstitue(e.target.value)} className='userPasswordResetInputs' name="current-password" id="confirm-password" />
            </div>
            <div className="inputFields">
            <label htmlFor="confirm-password">HOD Email</label>
            <br />
            <input type="text" placeholder={hodemail}  onChange={e => sethodemail(e.target.value)} className='userPasswordResetInputs' name="current-password" id="confirm-password" />
            </div>
            <div className="inputFields">
            <label htmlFor="confirm-password">Educational Qualification</label>
            <br />
            <input type="text"  placeholder={eduqua}  onChange={e => seteduqua(e.target.value)} className='userPasswordResetInputs' name="current-password" id="confirm-password" />
            </div>
            <div className="inputFields">
            <label htmlFor="confirm-password">Dom</label>
            <br />
            <input type="text"  placeholder={domain}  onChange={e => setdomain(e.target.value)} className='userPasswordResetInputs' name="current-password" id="confirm-password" />
            </div>
            {/* <div className="inputFields">
            <label htmlFor="confirm-password">Domain</label>
            <br />
            <input type="text" value={domain} placeholder={domain}   onChange={e => setphone(e.target.value)} className='userPasswordResetInputs' name="current-password" id="confirm-password" />
            </div> */}
            <div className="inputFields">
            <label htmlFor="confirm-password">Address</label>
            <br />
            <input type="text"  placeholder={address}  onChange={e => setaddress(e.target.value)} className='userPasswordResetInputs' name="current-password" id="confirm-password" />
            </div>

<div className="userResetFormButtons mt-5">
<button className='me-4 userUpdatePasswordButton' onClick={handleSubmit}>
                Update details
            </button>

            <button className='border-0 userCancelButton'>
                Cancel
            </button>
</div>
        </form>


        <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
    </div>
  
  )
}
