import React from 'react'
import "./MyCourse.css"
import { useState } from 'react';
import axios from 'axios';
import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';


export default function MyCourses(props) {

  const myArray = props.arrayProp;
  const navigate = useNavigate()

  const handleCourseClick = (itemID) => {
    sessionStorage.setItem('selectedCourseId', itemID);
    console.log("Item ID= " + itemID);
    navigate('/course-info');

  }


  return (
    <div className='userCoursesTop'>
      <h1 className="userEnrolledCourses text-center">
        Enrolled Courses
      </h1>
      <div className="AllEnrolledCourseListTop">
      <div>

        {/* <div className="row row-cols-2"></div> */}
      {myArray.map((item) => {
        return (<div className="EachEnrolledCourse d-flex justify-content-between courseCard" onClick={()=>handleCourseClick(item._id)}>

<div className='d-flex'>
<img className="courseImageTest" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToQeYeClZMrqqq2aaTT2UdbuWtVktz8jQ9gw&usqp=CAU" alt=""  />
        <div className="enrolledCourseInfo">
          <div className="enrolledCourse">
          <div className="courseMaintitle mb-2">
          {item.name}
          </div>
          <span className="enrolledCourseSubtext">
            {item.category}
          </span>
          </div>
          <div className="totalLessonsEnrolledCourse">
            Total 17 lessons
          </div>
        </div> 
</div>
        <div className="nextPageIcon me-5">
        <i class="fa-solid fa-angle-right"></i>
        </div>
      </div>);
      })}
    </div>
      {/* {props.showCourses.map((course) => (
        <div className="EachEnrolledCourse d-flex">

        <img className="courseImage" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToQeYeClZMrqqq2aaTT2UdbuWtVktz8jQ9gw&usqp=CAU" alt=""  />

        <div className="enrolledCourseInfo">
          <div className="enrolledCourse">
          <div className="courseMaintitle mb-2">
          {course.name}
          </div>
          <span className="enrolledCourseSubtext">
            {course.category}
          </span>
          </div>
          <div className="totalLessonsEnrolledCourse">
            Total 17 lessons
          </div>
        </div>

        <div className="nextPageIcon">
        <i class="fa-solid fa-angle-right"></i>
        </div>
      </div>
      ))} */}
        

        {/* <div className="EachEnrolledCourse d-flex">

          <img className="courseImage" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToQeYeClZMrqqq2aaTT2UdbuWtVktz8jQ9gw&usqp=CAU" alt=""  />

          <div className="enrolledCourseInfo">
            <div className="enrolledCourse">
            <div className="courseMaintitle mb-2">
            Introduction to High performance
computing and parallel programming
            </div>
            <span className="enrolledCourseSubtext">
              CDAC
            </span>
            </div>
            <div className="totalLessonsEnrolledCourse">
              Total 17 lessons
            </div>
          </div>

          <div className="nextPageIcon">
          <i class="fa-solid fa-angle-right"></i>
          </div>
        </div> */}

        {/* <div className="EachEnrolledCourse d-flex">

          <img className="courseImage" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToQeYeClZMrqqq2aaTT2UdbuWtVktz8jQ9gw&usqp=CAU" alt=""  />

          <div className="enrolledCourseInfo">
            <div className="enrolledCourse">
            <div className="courseMaintitle mb-2">
            Introduction to High performance
computing and parallel programming
            </div>
            <span className="enrolledCourseSubtext">
              CDAC
            </span>
            </div>
            <div className="totalLessonsEnrolledCourse">
              Total 17 lessons
            </div>
          </div>

          <div className="nextPageIcon">
          <i class="fa-solid fa-angle-right"></i>
          </div>
        </div> */}

      </div>
    </div>
  )
}
