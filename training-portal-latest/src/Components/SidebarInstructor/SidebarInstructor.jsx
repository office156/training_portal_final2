import React from 'react'
import { useNavigate } from 'react-router-dom'
import { useEffect , useState } from 'react'
import axios from 'axios'

export default function SidebarInstructor() {

    const [instructorName , setinstructorName] = useState("")
    const [instructorEmail , setinstructorEmail] = useState("")
    const tokenFromSessionStorage = sessionStorage.getItem('Token');
    const navigate = useNavigate()

    useEffect(() => {
        getInstructorInfo()

    }, [])

    const cleardata = ()=>{
        sessionStorage.removeItem("InstructorId")
        sessionStorage.removeItem("Token")
        sessionStorage.removeItem("Role")
    }


    const getInstructorInfo = async() =>{
        try {
            const url = "http://localhost:8000/api/current-instructor"
            const response = await axios.get(url, {
              headers: {
                'Authorization': `Bearer ${tokenFromSessionStorage}`,
              }
            });
            // setAnnouncements([...announcements, response.data]);
            // setNewAnnouncement({ title: '', text: '' });
            console.log(response.data)
            setinstructorEmail(response.data.email)
            setinstructorName(response.data.name)
          } catch (error) {
            console.error(error);
          }
    }
    

    

  return (
    <div className='SideBarTop'>
        <div className="AdminSidebarHeader text-center">Dashboard</div>
        <div className="sidebarListItems d-flex flex-column justify-content-between align-items-between">
        <div className="listItemsTopSidebar">
            <ul className='p-0'>
                {/* <li className='sideBarTopListElements py-2' onClick={()=>{
                    navigate("/instructor")
                }}>
                <i class="fa-solid px-2 sidebarTopIcons fa-house"></i>
                <p className='sidebarlistItemNames p-0 m-0'>Home</p>
                </li> */}
                <li className='sideBarTopListElements py-2' onClick={()=>{
                    navigate("/instructor-courses")
                }}>
                <i class="fa-solid px-2 sidebarTopIcons fa-layer-group"></i>
                <p className='sidebarlistItemNames p-0 m-0'>Courses</p>
                </li>
                <li className='sideBarTopListElements py-2' onClick={()=>{
                    navigate("/instructor-enrolled-courses")
                }}>
                <i class="fa-solid px-2 sidebarTopIcons fa-layer-group"></i>
                <p className='sidebarlistItemNames p-0 m-0'>My enrolled Courses</p>
                </li>
                <li className='sideBarTopListElements py-2' onClick={()=>{
                    navigate("/instructor-course-requests")
                }}>
                <i class="fa-solid px-2 sidebarTopIcons fa-layer-group"></i>
                <p className='sidebarlistItemNames p-0 m-0'>Requests</p>
                </li>
                {/* <li className='sideBarTopListElements py-2' onClick={()=>{
                    navigate("/instructor-suggestions")
                }}>
                <i class="fa-solid px-2 sidebarTopIcons fa-layer-group"></i>
                <p className='sidebarlistItemNames p-0 m-0'>Admin suggestions</p>
                </li> */}
            </ul>
        </div>
        <div className='listItemsBottomSidebar'>
        <ul className='p-0'>
                <li className='sideBarTopListElements py-2'>
                <i class="fa-solid px-2 fa-life-ring"></i>
                <p className='sidebarlistItemNames p-0 m-0'>Support</p>
                </li>
                <li className='sideBarTopListElements py-2'>
                <i class="fa-solid px-2 fa-gear"></i>
                <p className='sidebarlistItemNames p-0 m-0'>Settings</p>
                </li>
            </ul>
            <hr className='mx-2' />
            <div className="adminProfileMini d-flex align-items-center">
                {/* <img src="https://i0.wp.com/newdoorfiji.com/wp-content/uploads/2018/03/profile-img-1.jpg?ssl=1" alt="" className="profilePhotoSidebar me-2" /> */}
                <div className="text">
                    <h4 className='sidebarAdminHeading'>{instructorName}</h4>
                    <p className="p-0 m-0 adminEmail">{instructorEmail}</p>
                </div>
                <i class="ps-1 fa-solid fa-right-from-bracket LogOutButton" onClick={()=>{
                    cleardata()
                    navigate("/")
                }}></i>
            </div>
        </div>
        </div>
    </div>
  )
}
