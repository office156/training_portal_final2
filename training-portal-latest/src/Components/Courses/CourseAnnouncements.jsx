import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import './CourseAnnouncements.css'; 


const CourseAnnouncements = () => {
  const [announcements, setAnnouncements] = useState([]);
  const [userRoles, setUserRoles] = useState([]);
  const [isFormOpen, setIsFormOpen] = useState(false); 
  const [newAnnouncement, setNewAnnouncement] = useState({
    title: '',
    text: '',
  });
  const [isEditing, setIsEditing] = useState(false);
  const [editAnnouncementId, setEditAnnouncementId] = useState('');


  const courseIdFromSessionStorage = sessionStorage.getItem('selectedCourseId');
  const tokenFromSessionStorage = sessionStorage.getItem('Token');

  useEffect(() => {
    fetchUserDetails();
  }, []);

  const fetchUserDetails = async () => {
    try {
      // Make a GET request to fetch the user details
      const response = await axios.get('http://localhost:8000/api/current-user', {
        headers: {
          'Authorization': `Bearer ${tokenFromSessionStorage}`,
        }
      });
      setUserRoles(response.data.user.role);
      // console.log(userRoles.includes('Instructor'));
    } catch (error) {
      console.error('Error fetching user details:', error);
    }
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setNewAnnouncement((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };


  useEffect(() => {
    fetchAnnouncements();
  }, [announcements]);

  const fetchAnnouncements = async () => {
    try {
      const url = 'http://localhost:8000/api/course/' + courseIdFromSessionStorage + '/announcements';
      const response = await axios.get(url);
      setAnnouncements(response.data);
    } catch (error) {
      console.error('Error fetching announcements:', error);
    }
  };

  const handleSubmitAnnouncement = async (event) => {
    event.preventDefault();
    try {
      const url = `http://localhost:8000/api/course/${courseIdFromSessionStorage}/announcement/post`;
      const response = await axios.post(url, newAnnouncement, {
        headers: {
          'Authorization': `Bearer ${tokenFromSessionStorage}`,
        }
      });
      setAnnouncements([...announcements, response.data]);
      setNewAnnouncement({ title: '', text: '' });
    } catch (error) {
      console.error('Error posting announcement:', error);
    }
  };

  const handleEditAnnouncement = (announcement) => {
    setIsFormOpen(true);
    setIsEditing(true);
    setEditAnnouncementId(announcement._id);
    setNewAnnouncement({
      title: announcement.title,
      text: announcement.text,
    });
  };

  const handleUpdateAnnouncement = async () => {
    try {
      const url = `http://localhost:8000/api/course/${courseIdFromSessionStorage}/announcement/${editAnnouncementId}/edit`;
      await axios.patch(url, newAnnouncement, {
        headers: {
          'Authorization': `Bearer ${tokenFromSessionStorage}`,
        }
      });
      fetchAnnouncements();
      setIsEditing(false);
      setEditAnnouncementId('');
      setNewAnnouncement({ title: '', text: '' });
    } catch (error) {
      console.error('Error updating announcement:', error);
    }
  };

  const handleDeleteAnnouncement = async (announcementId) => {
    try {
      const url = `http://localhost:8000/api/course/${courseIdFromSessionStorage}/announcement/${announcementId}/delete`;
      await axios.delete(url, {
        headers: {
          'Authorization': `Bearer ${tokenFromSessionStorage}`,
        }
      });
      fetchAnnouncements();
    } catch (error) {
      console.error('Error deleting announcement:', error);
    }
  };


  const handlePostAnnouncement = async () => {
    try {
      const url = `http://localhost:8000/api/course/${courseIdFromSessionStorage}/announcement/post`;
      const response = await axios.post(
        url,
        { title: newAnnouncement.title, text: newAnnouncement.text },
        {
          headers: {
            'Authorization': `Bearer ${tokenFromSessionStorage}`,
          }
        }
      );

      // Add the new announcement to the announcements list
      setAnnouncements((prevAnnouncements) => [...prevAnnouncements, response.data]);
      setIsFormOpen(false); // Close the form after posting the announcement
      setNewAnnouncement({ title: '', text: '' }); // Reset the newAnnouncement state
    } catch (error) {
      console.error('Error posting announcement:', error);
    }
  };

  const handleFormToggle = () => {
    setIsFormOpen((prevIsFormOpen) => !prevIsFormOpen);
  };

  return (
    <div className="announcement-container">
      <h2>Announcements</h2>
      {announcements.map((announcement) => (
        <div key={announcement._id} className="announcement">

          <div className='text-start announcement'>
            <Accordion>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography><h3>{announcement.title}</h3></Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography>
                  <p>{announcement.text}</p>
                </Typography>
              </AccordionDetails>
            </Accordion>
            </div>

          {userRoles.includes('Instructor') ? ( <div>
          <button onClick={() => handleEditAnnouncement(announcement)}>
            Edit
          </button>
          <button onClick={() => handleDeleteAnnouncement(announcement._id)}>
            Delete
          </button>
          </div>
          ) : (<></>)}
          
        </div>
      ))}
      {userRoles.includes('Instructor') && (
        // Show the button only for users with the role of "Instructor"
        isFormOpen ? (
          <form onSubmit={isEditing ? handleUpdateAnnouncement : handleSubmitAnnouncement}>
            <input
              type="text"
              placeholder="Enter Title"
              value={newAnnouncement.title}
              onChange={(e) => setNewAnnouncement({ ...newAnnouncement, title: e.target.value })}
            />
            <textarea
              placeholder="Enter Text"
              value={newAnnouncement.text}
              onChange={(e) => setNewAnnouncement({ ...newAnnouncement, text: e.target.value })}
            />
            <button type="button" onClick={handleFormToggle}>Cancel</button>
            <button type="submit">
              {isEditing ? 'Update Announcement' : 'Post Announcement'}
            </button>
          </form>
        ) : (
          <button onClick={handleFormToggle}>
            Post New Announcement
          </button>
        )
      )}
    </div>
  );
};


export default CourseAnnouncements;
