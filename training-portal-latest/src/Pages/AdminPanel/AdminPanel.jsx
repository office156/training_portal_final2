import React from 'react'
import Navbar from '../../Components/Navbar/Navbar'
import Sidebar from '../../Components/Sidebar/Sidebar'
import "./AdminPanel.css"
import { useState , useEffect } from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'

export default function AdminPanel() {
  
  const [approvedusercount , setusercount] = useState("");
  const [unapprovedusercount , setunapprovedusercount] = useState("");
  const [coursescount , setcoursescount] = useState("");
  const [adminname , setadminname] = useState("")
  const [adminmail , setadminmail] = useState("")
  const navigate = useNavigate();

  



  useEffect(() => {

    if(sessionStorage.getItem("userId") === "admin"){
      
      axios.get("http://localhost:8000/api/admin/allusers/approved/count").then((response) => {
        setusercount(response.data.count);
        })
          .catch((error) => {
            console.log(error.data)
          });
    
       
        axios.get("http://localhost:8000/api/admin/allusers/unapproved/count").then((response) => {
        setunapprovedusercount(response.data.count);
        })
          .catch((error) => {
            console.log(error.data)
          });
    
        
        axios.get("http://localhost:8000/api/totalCourses").then((response) => {
        setcoursescount(response.data.totalCourses);
        })
          .catch((error) => {
            console.log(error.data)
          });

          setadminmail(sessionStorage.getItem("email"))
          setadminname(sessionStorage.getItem("name"))
    }else{
      navigate("/admin-login")
    }
    
 
    },[]);

  return (
    <div className="adminPanelTop">
      <Sidebar className = "adminPanelSidebartag" name={adminname} mail={adminmail}></Sidebar>


<div className="contentAdminPanel">
  <div className="container">
    <div className="row py-5 d-flex justify-content-around">
      <div className="col-3 adminPanelHomeStatusCards">
        <div className="line1 d-flex justify-content-between pt-3 px-1">
          <p className="line1text1">
          Total Courses
          </p>
          <i class="fa-solid fa-ellipsis-vertical"></i>
        </div>
        <div className="line2 d-flex justify-content-between align-items-end pb-3 px-1">
        <p className="line1text2">
          {coursescount}
          </p>
          <a href='/admin-courses' className='viewAllLink'>View all</a>
        </div>
      </div>
      <div className="col-3 adminPanelHomeStatusCards">
        <div className="line1 d-flex justify-content-between pt-3 px-1">
          <p className="line1text1">
          Total Active Users
          </p>
          <i class="fa-solid fa-ellipsis-vertical"></i>
        </div>
        <div className="line2 d-flex justify-content-between align-items-end pb-3 px-1">
        <p className="line1text2">
          {approvedusercount}
          </p>
          <a className='viewAllLink' href='/admin-users'>View all</a>
        </div>
      </div>
      <div className="col-3 adminPanelHomeStatusCards">
        <div className="line1 d-flex justify-content-between pt-3 px-1">
          <p className="line1text1">
          Total Inactive Users
          </p>
          <i class="fa-solid fa-ellipsis-vertical"></i>
        </div>
        <div className="line2 d-flex justify-content-between align-items-end pb-3 px-1">
        <p className="line1text2">
          {unapprovedusercount}
          </p>
          <a className='viewAllLink' href='/admin-users' >View all</a>
        </div>
      </div>
    </div>
  </div>
</div>

    </div>
  )
}
