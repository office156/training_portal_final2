import React, { useEffect } from 'react';
import "./Navbar.css"
import { Link ,useNavigate  } from 'react-router-dom';
import { useContext } from 'react';
import { AuthContext } from '../../App';
// Put any other imports below so that CSS from your
// components takes precedence over default styles.

export default function Navbar() {

  const user = sessionStorage.getItem('userId')
  const role = sessionStorage.getItem('Role')
  const username = sessionStorage.getItem('name')
  const navigate = useNavigate();
  const { loggedIn , setLoggedIn} = useContext(AuthContext);

  function handleClick() { 
    navigate('/registration');
  }


  function logOut() { 
    sessionStorage.removeItem('userId')
    sessionStorage.removeItem('Role')
    sessionStorage.removeItem('Token')
    console.log(loggedIn)
    setLoggedIn(false)
    console.log(loggedIn)
    navigate("/login")
  }
  
  useEffect(() => {

    // console.log(user)
    // console.log(loggedIn)
    if (user === null) {
      setLoggedIn(false)
      // navigate("/login")
    }else if (role === "Learner") {
      setLoggedIn(true)
    }
    
  }, []);

  return <div>
 {/* <!-- Navigation --> */}

<nav class="navbar navbar-expand-lg bg-light">
  <div class="container">
    <a class="navbar-brand NavbarBrand" onClick={(e)=>{
            e.preventDefault();
            navigate('/home');
          }} >HPCTraining</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ms-auto">
        <li class="nav-item learningPathLink">
          <a class="nav-link" aria-current="page" onClick={(e)=>{
            e.preventDefault();
            navigate('/home');
          }}>Home</a>
        </li>
        <li class="nav-item learningPathLink">
          <a class="nav-link" aria-current="page" onClick={(e)=>{
            e.preventDefault();
            navigate('/about');
          }}>About Us</a>
        </li>
        <li class="nav-item learningPathLink">
          <a onClick={(e)=>{
            e.preventDefault();
            navigate('/courses');
          }} class="nav-link" >Courses</a>
        </li>

        {/* <li class="nav-item learningPathLink">
          <a onClick={(e)=>{
            e.preventDefault();
            navigate('/learning-path');
          }} class="nav-link">Learning Path</a>
        </li> */}


{/* testing if else block  */}
{loggedIn ? (
  <>
                <li class="nav-item learningPathLink">
                <a onClick={(e)=>{
                  e.preventDefault();
                  navigate('/user-profile');
                }} class="nav-link" >My Courses</a>
              </li>
              <div class="dropdown">
  {/* <button class="dropbtn">Dropdown</button> */}
  <div className='d-flex'>
    <img src="https://img.freepik.com/premium-photo/global-business-logistics-import-export-container-cargo-freight-ship-during-loading-industrial-port-by-crane-container-handlers-cargo-plane-truck-highway-generative-ai-high-quality-phot_493343-30751.jpg" alt="" className='UserNavbarPhoto'/>
    <div className="UserElements ps-2 dropbtn">
    <div className='UserNameNavbar'>
      {username}
    </div>
    {/* <div className="UserEmailNavbar">
      h@gmail.com
    </div> */}
    </div>
  </div>
  <div class="dropdown-content">
    <a className='NavbarUserLinks' onClick={(e)=>{
                  e.preventDefault();
                  navigate('/user-profile');
                }}>My Courses</a>
    {/* <a className='NavbarUserLinks' onClick={(e)=>{
                  e.preventDefault();
                  navigate('/user-profile');
                }}>My Profile</a> */}
    <a className='NavbarUserLinks' onClick={logOut}>Logout</a>
  </div>
</div>
                </>
      ) : (
        <>
        <li class="nav-item loginLink">
        <a onClick={(e)=>{
          e.preventDefault();
          navigate('/login');
        }} class="nav-link">Login</a>
      </li>
      <li class="nav-item">
        <button type="button"  onClick={handleClick} class="NavbarSignUpButton p-2">Sign up</button>
        {/* <Link to="/registration" ></Link> */}
      </li>
      </>
      )}
{/* end block  */}


    </ul>
    </div>
  </div>
</nav>

  </div>;
}
