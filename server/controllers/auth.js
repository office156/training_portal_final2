import User from "../models/user";
import Bulk from "../models/bulk";
import Email from "../models/email";
import AdminUser from "../models/adminuser";
import { hashPassword, comparePassword } from "../utils/auth";
import jwt from "jsonwebtoken";
import nodemailer from "nodemailer";
import { nanoid } from "nanoid";
import passwordValidator from "password-validator";
import path from 'path';
import fs from 'fs';

const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 465,
  secure: false,
  service: "Gmail",
  auth: {
    user: process.env.MAIL_USERNAME,
    pass: process.env.MAIL_PASSWORD,
  },
});

export { transporter };

export const registeruser = async (req, res) => {
  try {
    const {
      name,
      email,
      institute,
      address,
      phone,
      password,
      hodemail,
      eduqua,
      domain,
    } = req.body;

    const photo = req.files["photo"][0].filename;
    const aadharcard = req.files["aadharcard"][0].filename;

    if (!name) return res.send(400).send("Name is required");

    var schema = new passwordValidator();

    schema
      .is()
      .min(8) // Minimum length 8
      .is()
      .max(100) // Maximum length 100
      .has()
      .uppercase() // Must have uppercase letters
      .has()
      .lowercase() // Must have lowercase letters
      .has()
      .digits(1) // Must have at least 1 digits
      .has()
      .not()
      .spaces() // Should not have spaces
      .is()
      .not()
      .oneOf(["12345678", "Password", "Password123", "password"]); // Blacklist these values

    if (!schema.validate(password)) {
      return res.status(400).send(
        "Password must be of minimum 8 character and should contain atleast one uppercase and lowercase letter and 1 digit"
      );
    }

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+(\.[^\s@]+)?$/;

    if (!emailRegex.test(email)) {
      return res.status(400).send("Email is not valid");
    }

    if (!emailRegex.test(hodemail)) {
      return res.status(400).send("HOD Email is not valid");
    }

    const phoneRegex = /^[0-9]{10}$/;
    if (!phoneRegex.test(phone)) {
      return res.status(400).send("Phone number is invalid");
    }

    let userExist = await User.findOne({ email }).exec();

    if (userExist) return res.status(400).send("Email is already registered");

    const hashedPassword = await hashPassword(password);

    const user = new User({
      name,
      email,
      institute,
      address,
      phone,
      password: hashedPassword,
      hodemail,
      eduqua,
      domain,
      photo,
      aadharcard,
    });

    await user.save();

    const mailOptions = {
      from: "himanshumuj@gmail.com",
      to: `${email}`,
      subject: "Account Verification - HPCTraining Portal",
      text: `Dear ${name},\n\nThank you for registring with us. Currently your account is inactive. You will be able to login after Photo and Aadhar card verification. You will get a confirmation email on your registered email id after successful verification.\n\nBest regards,\nHPCTraining Team`,
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });

    return res.json({ ok: true });
  } catch (err) {
    console.log(err);
    return res.status(400).send("Error in registering user");
  }
};

export const login = async (req, res) => {
  try {

    const { email, password } = req.body;
    const user = await User.findOne({ email }).exec();

    if (!user) return res.status(400).send("No user found with this email");

    const match = await comparePassword(password, user.password);

    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET, {
      expiresIn: "7d",
    });

    user.password = undefined;

    // res.cookie("token", token, {
    //   httpOnly: true,
    // });

    if (user.isApproved == true) {
      if (match) {
        //         res.json(user);
        return res.json({ token, user });

      } else {
        // return res.status(400).send("Incorrect username or password");
        return res.header("Authorization", "Bearer " + token).json(user);
      }
    } else {
      return res.status(400).send("User is not approved");
    }
  } catch {
    console.log(err);
    return res.status(400).send("Error in logging in");
  }
};

export const logout = async (req, res) => {
  try {
    res.clearCookie("token");
    return res.json({ message: "Signout successful" });
  } catch {
    console.log(err);
    return res.status(400).send("Error in logging out");
  }
};

export const currentUser = async (req, res) => {
  try {
    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res.status(401).json({ error: 'Error : No authorization header found' });
    }

    const token = authHeader.split(' ')[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    // Find the user
    const user = await User.findById(userId).exec();
    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    // Return the user data to the client
    return res.json({ user });
  } catch (err) {
    console.error(err);
    return res.status(500).json({ error: 'Internal server error' });
  }
};

// export const approveUser = async (req, res) => {
//     try {
//         const { userId } = req.params;

//         await User.findByIdAndUpdate(userId, { isApproved: true });
//         res.send('User is approved.');
//     }
//     catch (error) {
//         res.status(400).send(error.message);
//     }
// };

export const approveUser = async (req, res) => {
  try {
    const { email } = req.params;

    await User.findOneAndUpdate({ email }, { isApproved: true });

    const mailOptions = {
      from: "himanshumuj@gmail.com",
      to: `${email}`,
      subject: "Account Verification - HPCTraining Portal",
      text: `Dear User,\n\nYour HPCTraining account is approved and is active now.\n\nBest regards,\nHPCTraining Team`,
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });

    res.send("User is approved.");
  } catch (error) {
    res.status(400).send(error.message);
  }
};

export const unapproveUser = async (req, res) => {
  try {
    const { email } = req.params;

    await User.findOneAndUpdate({ email }, { isApproved: false });

    res.send("User is unapproved.");
  } catch (error) {
    res.status(400).send(error.message);
  }
};

export const getAllUsers = async (req, res) => {
  try {
    const users = await User.find();
    res.json(users);
  } catch (err) {
    console.log(err);
  }
};

export const getAllInstructores = async (req, res) => {
// app.get('/instructors', async (req, res) => {
  try {
    // Find users with the role "Instructor"
    const instructors = await User.find({ role: 'Instructor' });

    // Send the list of instructors as a response
    res.json(instructors);
  } catch (error) {
    // If there's an error, return an error response
    res.status(500).json({ error: 'Internal server error' });
  }
};

export const getAllApprovedUsers = async (req, res) => {
  try {
    const users = await User.find({ isApproved: true });
    res.json(users);
  } catch (err) {
    console.log(err);
  }
};

export const getAllUnapprovedUsers = async (req, res) => {
  try {
    const users = await User.find({ isApproved: false });
    res.json(users);
  } catch (err) {
    console.log(err);
  }
};

export const countTotalUsers = async (req, res) => {
  try {
    const userCount = await User.countDocuments();
    res.status(200).json({ count: userCount });
  } catch (err) {
    console.error(err);
    res.status(500).send("Something went wrong");
  }
};

export const countApprovedUsers = async (req, res) => {
  try {
    const approvedUsersCount = await User.countDocuments({ isApproved: true });
    res.status(200).json({ count: approvedUsersCount });
  } catch (err) {
    console.error(err);
    res.status(500).send("Something went wrong");
  }
};

export const countUnapprovedUsers = async (req, res) => {
  try {
    const unapprovedUsersCount = await User.countDocuments({
      isApproved: false,
    });

    res.status(200).json({ count: unapprovedUsersCount });
  } catch (err) {
    console.error(err);
    res.status(500).send("Something went wrong");
  }
};

export const bulkRegister = async (req, res) => {
  try {
    const {
      name,
      email
    } = req.body;

    const aadharcardPath = req.files["aadharcard"][0].filename;

    // Check if the uploaded file in the "excel" field is an Excel file
    const excelFile = req.files["excel"][0];
    if (excelFile.mimetype !== 'application/vnd.ms-excel' && excelFile.mimetype !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      return res.status(400).send("Only Excel files are allowed in the 'excel' field");
    }
    const excelPath = excelFile.filename;

    if (!name) return res.status(400).send("Name is required");

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+(\.[^\s@]+)?$/;

    if (!emailRegex.test(email)) {
      return res.status(400).send("Email is not valid");
    }

    const bulk = new Bulk({
      name, email, aadharcard: aadharcardPath, excel: excelPath
    });

    await bulk.save();

    res.json({ ok: true });
  } catch (err) {
    console.log(err);
    res.status(400).send("Error in fetching excel sheet");
  }
};

export const getUserRegistrationDocumentsPhoto = async (req, res) => {
  try {
    const user = await User.findById(req.params.userId);
    if (!user) {
      return res.status(404).send('User record not found');
    }

    res.sendFile(path.join(__dirname, "../public/userImages", user.photo));

  } catch (err) {
    console.log(err);
    return res.status(400).send('Error in fetching user photo');
  }
};

export const getUserRegistrationDocumentsAadhar = async (req, res) => {
  try {
    const user = await User.findById(req.params.userId);
    if (!user) {
      return res.status(404).send('User record not found');
    }

    res.sendFile(path.join(__dirname, "../public/userImages", user.aadharcard));

  } catch (err) {
    console.log(err);
    return res.status(400).send('Error in fetching bulk document');
  }
};

export const getAllUserDocuments = async (req, res) => {
  try {
    const user = await User.findById(req.params.userId);
    if (!user) {
      return res.status(404).send('User not found');
    }
    return res.json({
      photo: user.photo,
      aadharcard: user.aadharcard,
    });
  } catch (err) {
    console.log(err);
    return res.status(400).send('Error in fetching user documents');
  }
};

export const getUserBulkRegistrationDocumentsAadhar = async (req, res) => {
  try {
    const bulk = await Bulk.findById(req.params.bulkId);
    if (!bulk) {
      return res.status(404).send('Bulk record not found');
    }

    res.sendFile(path.join(__dirname, "../public/userImages", bulk.aadharcard));

  } catch (err) {
    console.log(err);
    return res.status(400).send('Error in fetching bulk document');
  }
};

export const getUserBulkRegistrationDocumentsExcel = async (req, res) => {
  try {
    const bulk = await Bulk.findById(req.params.bulkId);
    if (!bulk) {
      return res.status(404).send('Bulk record not found');
    }

    res.sendFile(path.join(__dirname, "../public/userImages", bulk.excel));

  } catch (err) {
    console.log(err);
    return res.status(400).send('Error in fetching bulk document');
  }
};

export const getAllBulkDocs = async (req, res) => {
  try {
    const bulkDocs = await Bulk.find();
    res.json(bulkDocs);
  } catch (err) {
    console.log(err);
  }
};

export const downloadUserDocs = async (req, res) => {
  try {
    const id = req.params.id;
    const user = await User.findById(id);

    if (!user) {
      return res.status(404).send('User not found');
    }

    const photoFilename = user.photo;
    const aadharcardFilename = user.aadharcard;
    const photoPath = path.join(__dirname, '/../public/userImages', photoFilename);
    const aadharcardPath = path.join(__dirname, '/../public/userImages', aadharcardFilename);
    const photoFile = fs.createReadStream(photoPath);
    const aadharcardFile = fs.createReadStream(aadharcardPath);

    res.set({
      'Content-Type': 'application/octet-stream',
      'Content-Disposition': `attachment; filename="${photoFilename}"`,
    });

    photoFile.pipe(res, { end: false });

    res.set({
      'Content-Type': 'application/octet-stream',
      'Content-Disposition': `attachment; filename="${aadharcardFilename}"`,
    });

    aadharcardFile.pipe(res).on('finish', () => {
      res.end();
    });
  } catch (err) {
    console.log(err);
    res.status(500).send('Error in downloading files');
  }
};

export const downloadBulkDocs = async (req, res) => {
  try {
    const id = req.params.id;
    const bulk = await Bulk.findById(id);

    if (!bulk) {
      return res.status(404).send('Bulk registration not found');
    }

    const aadharcardFilename = bulk.aadharcard;
    const excelFilename = bulk.excel;
    const aadharcardPath = path.join(__dirname, '/../public/userImages', aadharcardFilename);
    const excelPath = path.join(__dirname, '/../public/userImages', excelFilename);
    const aadharcardFile = fs.createReadStream(aadharcardPath);
    const excelFile = fs.createReadStream(excelPath);

    res.set({
      'Content-Type': 'application/octet-stream',
      'Content-Disposition': `attachment; filename="${aadharcardFilename}"`,
    });

    aadharcardFile.pipe(res, { end: false });

    res.set({
      'Content-Type': 'application/octet-stream',
      'Content-Disposition': `attachment; filename="${excelFilename}"`,
    });

    excelFile.pipe(res).on('finish', () => {
      res.end();
    });
  } catch (err) {
    console.log(err);
    res.status(500).send('Error in downloading files');
  }
};

export const sendEmailToUser = async (req, res) => {
  try {
    const { subject, text } = req.body;
    const receiver = req.params.email;

    const mailOptions = {
      from: "himanshumuj@gmail.com",
      to: `${receiver}`,
      subject: `${subject}`,
      text: `${text}`
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });

    res.send("Email is sent to the user");
  } catch (error) {
    res.status(400).send(error.message);
  }
};

// get in touch
export const getInTouch = async (req, res) => {
  try {
    const { sender, subject, text } = req.body;
    const receiver = 'himanshumuj@gmail.com';

    const mailOptions = {
      from: `${sender}`,
      to: `${receiver}`,
      subject: `${subject}`,
      text: `${text}`
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });
    
      // Save the email content in the database
      const emailData = { sender, subject, text };
      const savedEmail = await Email.create(emailData);

      res.json({ message: "Email is sent to the user and saved in the database.", savedEmail });
    } 
    catch (error) 
    {
    res.status(400).json({ error: error.message });
    }
};

export const allGetInTouchEmails = async (req, res) => {
  try {
    const allEmails = await Email.find();

    res.json(allEmails);
  } catch (error) {
    res.status(500).json({ error: 'Internal server error' });
  }
};

// get user details by email id
// export const getUserDetailsByEmail = async (req, res) => {
//   try {
//     const email = req.params.email;
//     const user = await User.findOne({ email: email });
//     if (!user) {
//       return res.status(404).send('User not found');
//     }
//     res.send(user);
//   } catch (err) {
//     console.error(err.message);
//     res.status(500).send('Server Error');
//   }
// };

export const getUserDetailsByEmail = async (req, res) => {
  try {
    const email = req.params.email;
    const users = await User.find({ email: { $regex: email, $options: 'i' } });
    
    if (users.length === 0) {
      return res.status(404).send('No users found with the provided email');
    }

    res.send(users);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
};


// get user details by id
export const getUserDetailsById = async (req, res) => {
  try {
    const id = req.params.id;
    const user = await User.findOne({ _id: id });
    if (!user) {
      return res.status(404).send('User not found');
    }
    res.send(user);
  } catch (err) {
    console.error(err.message);
    res.status(500).send('Server Error');
  }
};

// Update user detail by id
export const updateUserDetailsById = async (req, res) => {
  try {
    const userId = req.params.id;
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).send({ error: 'User not found' });
    }

    if (req.body.name) user.name = req.body.name;
    if (req.body.email) user.email = req.body.email;
    if (req.body.institute) user.institute = req.body.institute;
    if (req.body.address) user.address = req.body.address;
    if (req.body.phone) user.phone = req.body.phone;
    if (req.body.hodemail) user.hodemail = req.body.hodemail;
    if (req.body.eduqua) user.eduqua = req.body.eduqua;
    if (req.body.domain) user.domain = req.body.domain;

    const updatedUser = await user.save();

    res.status(200).send(updatedUser);
  } catch (error) {
    console.log(error);
    res.status(500).send({ error: 'Internal server error' });
  }
};

const moment = require('moment'); // for date manipulation


export const usersMonthWise = async (req, res) => {
  try {
    const registrations = await User.aggregate([
      {
        $group: {
          _id: {
            year: { $year: '$createdAt' },
            month: { $month: '$createdAt' },
          },
          count: { $sum: 1 },
        },
      },
      {
        $sort: { '_id.year': 1, '_id.month': 1 },
      },
    ]);

    const registrationCounts = registrations.map((entry) => ({
      month: moment({ year: entry._id.year, month: entry._id.month - 1 }).format('MMMM YYYY'),
      count: entry.count,
    }));

    res.json(registrationCounts);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

// export const loginadmin = async (req, res) => {
//   try {

//     const { email, password } = req.body;
//     const admin = await Admin.findOne({ email }).exec();

//     if (!admin) return res.status(400).send("No user found with this email");

//     const match = await comparePassword(password, admin.password);

//     const token = jwt.sign({ _id: admin._id }, process.env.JWT_SECRET, {
//       expiresIn: "7d",
//     });

//     admin.password = undefined;

//     // res.cookie("token", token, {
//     //   httpOnly: true,
//     // });

//     if (admin.isApproved == true) {
//       if (match) {
//         //         res.json(user);
//         return res.json({ token, admin });

//       } else {
//         // return res.status(400).send("Incorrect username or password");
//         return res.header("Authorization", "Bearer " + token).json(admin);
//       }
//     } else {
//       return res.status(400).send("Admin is not approved");
//     }
//   } catch (err) {
//     console.log(err);
//     return res.status(400).send("Error in logging in");
//   }
// };

// export const loginadmin = async (req, res) => {
//   try {

//     const { email, password } = req.body;
//     const admin = await Admin.findOne({ email }).exec();

//     if (!admin) return res.status(400).send("No admin found with this email");

//     const match = await comparePassword(password, admin.password);

//     const token = jwt.sign({ _id: admin._id }, process.env.JWT_SECRET, {
//       expiresIn: "7d",
//     });

//     admin.password = undefined;

//     // res.cookie("token", token, {
//     //   httpOnly: true,
//     // });


//       if (match) {
//         //         res.json(user);
//         return res.json({ token, admin });

//       } else {
//         // return res.status(400).send("Incorrect username or password");
//         return res.header("Authorization", "Bearer " + token).json(admin);
//       }

//   } catch (err){
//     console.log(err);
//     return res.status(400).send("Error in logging in");
//   }
// };




export const loginadminuser = async (req, res) => {
  try {
    const { email, password } = req.body;
    const admin = await AdminUser.findOne({ email }).exec();

    if (!admin) return res.status(400).send("No user found with this email");

    if (password === admin.password) {
      const token = jwt.sign({ _id: admin._id }, process.env.JWT_SECRET, {
        expiresIn: "7d",
      });
      admin.password = undefined;
      return res.json({ token, admin });
    } else {
      return res.status(400).send("Incorrect password");
    }
  } catch (err) {
    console.log(err);
    return res.status(400).send("Error in logging in");
  }
};

