import React from 'react'
import "./VerticalStepper.css"

export default function VerticalStepper() {
  return (
    // <div className='VerticalStepperTop'>
  //       <div class="stepper d-flex flex-column mt-5 ml-2">
  //   <div class="d-flex mb-1">
  //     <div class="d-flex flex-column pr-4 align-items-center">
  //       <div class="rounded-circle py-2 px-3 bg-primary text-white mb-1">1</div>
  //       <div class="line h-100"></div>
  //     </div>
  //     <div>
  //       <h5 class="text-dark text-start px-4 StepperMainLabels">Foundation (2 Courses)</h5>
  //       <p class="lead text-muted pb-3 px-4 subtextMainLabels">These foundation courses cover introductory part about HPC domain</p>

  //       <div className="coursesVerticalStepper">
  //       <div className="">
  //                 <div className="row">
  //                 <div class="col-4 mx-auto mt-3">
  //           <div class="card card2">
  //               <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
  //                   <div class="card-body">
  //                       <h5 class="card-title card2-title">CUDA programming</h5>
  //                   </div>
  //           </div>
  //       </div>
  //       <div class="col-4 mx-auto mt-3">
  //           <div class="card card2">
  //               <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
  //                   <div class="card-body">
  //                       <h5 class="card-title card2-title">SPACK utility</h5>
  //                   </div>
  //           </div>
  //       </div>
  //                 </div>
  //             </div>
  //       </div>
        
  //     </div>

  //     {/* <div className="coursesVerticalStepper">
          
  //     </div> */}
  //   </div>
  //   <div class="d-flex mb-1">
  //     <div class="d-flex flex-column pr-4 align-items-center">
  //       <div class="rounded-circle py-2 px-3 bg-primary text-white mb-1">2</div>
  //       <div class="line h-100"></div>
  //     </div>
  //     <div>
  //       <h5 class="text-dark text-start px-4 StepperMainLabels">Intermediatory (2 Courses)</h5>
  //       <p class="lead text-muted pb-3 px-4 subtextMainLabels">These intermediatory courses covers about OpenMP and MPI</p>

  //       <div className="coursesVerticalStepper">
  //       <div className="">
  //                 <div className="row">
  //                 <div class="col-4 mx-auto mt-3">
  //           <div class="card card2">
  //               <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
  //                   <div class="card-body">
  //                       <h5 class="card-title card2-title">CUDA programming</h5>
  //                   </div>
  //           </div>
  //       </div>
  //       <div class="col-4 mx-auto mt-3">
  //           <div class="card card2">
  //               <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
  //                   <div class="card-body">
  //                       <h5 class="card-title card2-title">SPACK utility</h5>
  //                   </div>
  //           </div>
  //       </div>
  //                 </div>
  //             </div>
  //       </div>
        
  //     </div>

  //     {/* <div className="coursesVerticalStepper">
          
  //     </div> */}
  //   </div>
  //   <div class="d-flex mb-1">
  //     <div class="d-flex flex-column pr-4 align-items-center">
  //       <div class="rounded-circle p-2 px-3 bg-primary text-white mb-1">3</div>
  //       {/* <div class="line h-100"></div> */}
  //     </div>
  //     <div>
  //       <h5 class="text-dark text-start px-4 StepperMainLabels">Advanced (2 Courses)</h5>
  //       <p class="lead text-muted pb-3 subtextMainLabels">These courses cover about advanced part of HPC domain</p>

  //       <div className="coursesVerticalStepper">
  //       <div className="">
  //                 <div className="row">
  //                 <div class="col-4 mx-auto mt-3">
  //           <div class="card card2">
  //               <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
  //                   <div class="card-body">
  //                       <h5 class="card-title card2-title">CUDA programming</h5>
  //                   </div>
  //           </div>
  //       </div>
  //       <div class="col-4 mx-auto mt-3">
  //           <div class="card card2">
  //               <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
  //                   <div class="card-body">
  //                       <h5 class="card-title card2-title">SPACK utility</h5>
  //                   </div>
  //           </div>
  //       </div>
  //                 </div>
  //             </div>
  //       </div>
        
  //     </div>

  //     {/* <div className="coursesVerticalStepper">
          
  //     </div> */}
  //   </div>
  // </div>
    // </div>
    // "relative m-0 w-full list-none overflow-hidden p-0 transition-[height] duration-200 ease-in-out
    <ul
  class="verticalStepperStyle1 d-none d-sm-block"
  >
  <li
    
    class="">
    <div
      
      class="d-flex">
      <span
        
        class="me-3 d-flex text-center makeCircle ">
        1
      </span>
      <span
        
        class="VerticalStepperHeading">
        Foundation (2 Courses)
        <p class="m-0 my-2 VerticalStepperSubtext">These foundation courses cover introductory part about HPC domain</p>
      </span>
    </div>
    <div
      
      class="d-flex adjustHeight">
        
        <div className="v1"></div>
        <div className='StepperContent d-flex'>
        <div class="">
            <div class="card card2 CardCustomization">
                <img src={require("../../Images/Mask2.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title VerticalStepperCardTitle">CUDA programming</h5>
                        <p className='VerticalStepperCardSubtext'>CDAC</p>
                    </div>
            </div>
        </div>
        <div class=" ">
            <div class="card card2 CardCustomization">
                <img src={require("../../Images/Mask2.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title VerticalStepperCardTitle">SPACK utility</h5>
                        <p className='VerticalStepperCardSubtext'>CDAC</p>
                    </div>
            </div>
        </div>
        {/* <div className="col"></div>
        <div className="col"></div> */}
        </div>
        {/* <!-- <div class="col"></div>
        <div class="col"></div> --> */}
                
             
        
    </div>
  </li>
  <li
    
    class="">
    <div
      
      class="d-flex">
      <span
        
        class="me-3 d-flex text-center makeCircle ">
        2
      </span>
      <span
        
        class="VerticalStepperHeading">
        Intermediate (2 Courses)
        <p class="m-0 my-2 VerticalStepperSubtext">These foundation courses cover introductory part about HPC domain</p>
      </span>
    </div>
    <div
      
      class="d-flex adjustHeight">
        
        <div className="v1"></div>
        <div className='StepperContent d-flex'>
        <div class="">
            <div class="card card2 CardCustomization">
                <img src={require("../../Images/Mask2.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title VerticalStepperCardTitle">CUDA programming</h5>
                        <p className='VerticalStepperCardSubtext'>CDAC</p>
                    </div>
            </div>
        </div>
        <div class=" ">
            <div class="card card2 CardCustomization">
                <img src={require("../../Images/Mask2.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title VerticalStepperCardTitle">SPACK utility</h5>
                        <p className='VerticalStepperCardSubtext'>CDAC</p>
                    </div>
            </div>
        </div>

        </div>
        {/* <!-- <div class="col"></div>
        <div class="col"></div> --> */}
                
             
        
    </div>
  </li>
  <li
    
    class="">
    <div
      
      class="d-flex">
      <span
        
        class="me-3 d-flex text-center makeCircle ">
        3
      </span>
      <span
        
        class="VerticalStepperHeading">
        Advaced (2 Courses)
        <p class="m-0 my-2 VerticalStepperSubtext">These foundation courses cover introductory part about HPC domain</p>
      </span>
    </div>
    <div
      
      class="d-flex adjustHeight">
        
        <div className="v2"></div>
        <div className='StepperContent d-flex'>
        <div class="">
            <div class="card card2 CardCustomization">
                <img src={require("../../Images/Mask2.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title VerticalStepperCardTitle">CUDA programming</h5>
                        <p className='VerticalStepperCardSubtext'>CDAC</p>
                    </div>
            </div>
        </div>
        <div class=" ">
            <div class="card card2 CardCustomization">
                <img src={require("../../Images/Mask2.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title VerticalStepperCardTitle">SPACK utility</h5>
                        <p className='VerticalStepperCardSubtext'>CDAC</p>
                    </div>
            </div>
        </div>

        </div>
        {/* <!-- <div class="col"></div>
        <div class="col"></div> --> */}
                
             
        
    </div>
  </li>
  {/* <li
    
    class="">
    <div
      
      class="d-flex">
      <span
        
        class="me-3 d-flex text-center makeCircle ">
        3
      </span>
      <span
        
        class="VerticalStepperHeading">
        Advanced (2 Courses)
        <p class="m-0 my-2 VerticalStepperSubtext">These foundation courses cover introductory part about HPC domain</p>
      </span>
    </div>
    <div
      
      class="d-flex adjustHeight">
        
        <div className=""></div>
        <div className='StepperContent d-flex'>
        <div class="">
            <div class="card card2 CardCustomization">
                <img src={require("../../Images/Mask2.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title VerticalStepperCardTitle">CUDA programming</h5>
                        <p className='VerticalStepperCardSubtext'>CDAC</p>
                    </div>
            </div>
        </div>
        <div class=" ">
            <div class="card card2 CardCustomization">
                <img src={require("../../Images/Mask2.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title VerticalStepperCardTitle">SPACK utility</h5>
                        <p className='VerticalStepperCardSubtext'>CDAC</p>
                    </div>
            </div>
        </div>
        </div>

                
             
        
    </div>
  </li> */}
</ul>
  )
}
