// const { expressjwt: jwt } = require("express-jwt");
import User from "../models/user";
import AdminUser from "../models/adminuser";

import jwt from "jsonwebtoken";

// export const requireSignin = jwt({
//   getToken: (req, res) => req.cookies.token,
//   secret: process.env.JWT_SECRET,
//   algorithms: ["HS256"],
// });

export const isInstructor = async (req, res, next) => {
  try {

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res.status(401).json({ error: 'Error : No authorization header found' });
    }

    const token = authHeader.split(' ')[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    const user = await User.findById(userId).exec();

    if (!user.role.includes("Instructor")) {
      return res.status(403).json({
        error: "User need to be instructor in order to create course",
      });
    }
    next();
  } catch (err) {
    return res.status(403).json({
      error: "Access denied",
    });
  }
};

export const isAdmin = async (req, res, next) => {
  try {

    // Get the admin ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res.status(401).json({ error: 'Error : No authorization header found' });
    }

    const token = authHeader.split(' ')[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const adminId = decoded._id;

    const admin = await AdminUser.findById(adminId).exec();

    if (!admin.role.includes("Admin")) {
      return res.status(403).json({
        error: "User need to be admin in order to update/delete course or lesson",
      });
    }
    next();
  } catch (err) {
    return res.status(403).json({
      error: "Access denied",
    });
  }
};