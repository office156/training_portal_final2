import React from 'react'
import "./Reset.css"
import { ToastContainer , toast } from 'react-toastify';
import { useState } from 'react';
import axios from 'axios';

export default function Reset() {


  const [password , setpassword] = useState("");
  const [newPassword , setnewPassword] = useState("");
  const [confirmPassword , setconfirmPassword] = useState("");

  const clearInputs = (event)=>{
    event.preventDefault();
    setnewPassword("");
    setconfirmPassword("");
    setnewPassword("");
  }

  const handleSubmit = (event) => {
    event.preventDefault();
  
    const data1 = { "password" : password, "new-password" : newPassword , "confirm-password":confirmPassword };

      const userFromSessionStorage = sessionStorage.getItem('userId');
      const url = "http://localhost:8000/api/admin/user/details/update/" + userFromSessionStorage
    axios.patch(url, password).then((response) => {

      console.log(response)
      // toast.success("Details updated successfully")
  
      })
      .catch((error)=>{
        toast.error(error.response.data)
  
      });
    }

  return (
    <div className='UserPasswordResetTop'>
        <h1 className='text-center ResetPageHeading'>Reset Password</h1>
        <form >
            <div className="inputFields">
            <label htmlFor="current-password">Current password</label>
            <br />
            <input type="password" className='userPasswordResetInputs' onChange={e => setpassword(e.target.value)} name="current-password" id="current-password" />
            </div>
            <div className="inputFields">
            <label htmlFor="new-password">New password</label>
            <br />
            <input type="password" className='userPasswordResetInputs' onChange={e => setnewPassword(e.target.value)} name="current-password" id="new-password" />
            </div>
            <div className="inputFields">
            <label htmlFor="confirm-password">Confirm new password</label>
            <br />
            <input type="password" className='userPasswordResetInputs' onChange={e => setconfirmPassword(e.target.value)} name="current-password" id="confirm-password" />
            </div>


<div className="userResetFormButtons mt-5">
<button className='me-4 userUpdatePasswordButton'  onClick={handleSubmit}>
                Update password
            </button>

            <button className='border-0 userCancelButton' onClick={clearInputs}>
                Cancel
            </button>
</div>
        </form>
    </div>
  )
}
