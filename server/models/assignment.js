import mongoose from "mongoose";
const { ObjectId } = mongoose.Schema.Types;

const assignmentSchema = new mongoose.Schema(
    {
      title: {
        type: String,
        trim: true,
        minlength: 3,
        maxlength: 320,
        required: true,
      },
      description: {
        type: String,
        required: true,
      },
      lesson: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Lesson',
        required: true,
      },
      submissions: [
        {
          user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
          },
          file: String,
          createdAt: {
            type: Date,
            default: Date.now,
          },
        },
      ],
    },
    { timestamps: true }
  );
  
export default mongoose.model("Assignment", assignmentSchema);

