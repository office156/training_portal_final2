import User from "../models/user";
import Bulk from "../models/bulk";
import ExcelFormat from "../models/excelFormat"
import { hashPassword, comparePassword } from "../utils/auth";
import jwt from "jsonwebtoken";
import nodemailer from "nodemailer";
import { nanoid } from "nanoid";
import passwordValidator from "password-validator";
import user from "../models/user"
import bulk from "../models/bulk"
import path from 'path';
import fs from 'fs';

const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false,
    service: "Gmail",
    auth: {
        user: process.env.MAIL_USERNAME,
        pass: process.env.MAIL_PASSWORD,
    },
});

export { transporter };

// export const getAllUsers = async (req, res) => {
//     try {
//         const users = await User.find();
//         res.json(users);
//     } catch (err) {
//         console.log(err);
//     }
// };

export const bulkRegisterExcelUpload = async (req, res) => {
    try {
        const {
            name,
        } = req.body;

        const excelFile = req.file;

        if (!name) return res.status(400).send("File name is required");
        if (!excelFile) return res.status(400).send("Excel file is required");

        if (excelFile.mimetype !== 'application/vnd.ms-excel' && excelFile.mimetype !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            return res.status(400).send("Only Excel files are allowed");
        }

        const excelPath = excelFile.filename;

        const excelFormat = new ExcelFormat({
            name, excel: excelPath
        });

        await excelFormat.save();

        res.json({ ok: true });
    } catch (err) {
        console.log(err);
        res.status(400).send("Error in uploading excel sheet format");
    }
};

export const getExcelFormatDocument = async (req, res) => {
    try {
        const excelFormatDocument = await ExcelFormat.findById(req.params.excelFormatId);
        if (!excelFormatDocument) {
            return res.status(404).send('Excel sheet format not found');
        }

        res.sendFile(path.join(__dirname, "../public/userImages", excelFormatDocument.excel));

    } catch (err) {
        console.log(err);
        return res.status(400).send('Error in fetching excel format document');
    }
};
