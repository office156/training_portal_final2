import React from 'react'
import HPCCards from '../../Components/HPCCards/HPCCards'
import LearningPathStepper from '../../Components/LearningPathStepper/LearningPathStepper'
import VerticalStepper from '../../Components/VerticalStepper/VerticalStepper'
import VStepper from '../../Components/VStepper/VStepper'
import "./LearningPathHpc.css"

export default function LearningPathHpc() {
  return (
    <div className='LearningPathHpcTop container'>
        <div className="courseCards">
            <HPCCards></HPCCards>
        </div>

        <div className="LearningPathMiddlePart text-start">
            <div className="LearningPathMiddlePartHeader">
                Learning Path for HPC
            </div>
            <div className='LearningPathMiddleSubtext1 py-3'>
            If you are unaware of HPC or have no idea about which course to start with, Please follow below learning path.
            </div>
            <div className='LearningPathMiddleSubtext2 py-5'>
            High performance computing (HPC) is the ability to process data and perform complex calculations at high speeds. To put it into perspective, 
a laptop or desktop with a 3 GHz processor can perform around 3 billion calculations per second. While that is much faster than any human 
can achieve, it pales in comparison to HPC solutions that can perform quadrillions of calculations per second. 
            </div>
        </div>

        <div className="LearningPathStepper">
            {/* <LearningPathStepper></LearningPathStepper> */}
            <VerticalStepper></VerticalStepper>
            {/* <VStepper></VStepper> */}
        </div>

    </div>
  )
}
