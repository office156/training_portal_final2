import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';


export default function BasicTable({rows,setRows,token,userFromSessionStorage}) {

  const navigate = useNavigate();



  const navigateToTheCourse = (courseId) => {
    // console.log('Course ID : ' + courseId + " "  + courseName);
    sessionStorage.setItem('selectedCourseId', courseId)
    navigate('/check-submissions')
  } 

  const navigateToTheProgress = (courseId) => {
    // console.log('Course ID : ' + courseId + " "  + courseName);
    sessionStorage.setItem('selectedCourseId', courseId)
    navigate('/check-progress')
  } 

  const UnPublishCourse = async (id) => {
    // /instructor/request-publish/:courseId", instructorRequestCoursePublish
    const url = "http://localhost:8000/api/instructor/request-unpublish/" + id 
    await axios.post(url,{
       headers: {
         'Authorization': `Bearer ${token}`,
       }
     }).then((response)=>{
       console.log(response)
       toast.success("Unpublish requested successfully");
       window.location.reload()
 
     }).catch((error)=>{
       toast.error(error);
       console.log(error);
     })
  }

  const handleunpublishAction = (id) => {
    const userConfirmed = window.confirm('Are you sure you want to perform this action?');

    if (userConfirmed) {
      UnPublishCourse(id)
    } else {
      // The user clicked 'Cancel', do nothing or provide feedback
      // alert('Action canceled.');
    }
  }

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Course name</TableCell>
            <TableCell align="right">Category</TableCell>
            <TableCell align="right">Price</TableCell>
            <TableCell align="right">Check submitions</TableCell>
            <TableCell align="right">Unpublish</TableCell>
            <TableCell align="right">Check student progress</TableCell>
            {/* <TableCell align="right">Carbs&nbsp;(g)</TableCell>
            <TableCell align="right">Protein&nbsp;(g)</TableCell> */}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.category}</TableCell>
              <TableCell align="right">{row.price}</TableCell>
              <TableCell align="right">
                <button type='button' className='ApproveUser' >
                        <i class="fa-solid fa-file-lines deleteIcon border p-2" onClick={()=>{
                          navigateToTheCourse(row._id)
                        }}></i>
                      </button>
              </TableCell>
              <TableCell align="right">
                <button type='button' className='ApproveUser' >
                        <i class="fa-solid fa-lock-open deleteIcon border p-2" onClick={() => handleunpublishAction(row._id) }></i>
                      </button>
              </TableCell>
              <TableCell align="right">
                <button type='button' className='ApproveUser' >
                        <i class="fa-regular fa-eye deleteIcon border p-2" onClick={() => navigateToTheProgress(row._id) }></i>
                      </button>
              </TableCell>
              {/* <TableCell align="right">{row.carbs}</TableCell>
              <TableCell align="right">{row.protein}</TableCell> */}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}