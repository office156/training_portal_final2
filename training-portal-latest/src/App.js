import './App.css';
import Navbar from './Components/Navbar/Navbar';
import Topbar from './Components/Topbar/Topbar';
// import BulkRegistration from './Pages/BulkRegistration/BulkRegistration';
import FileUpload from './Pages/File_upload/File_upload';
import Final_form from './Pages/Final_form/Final_form';
import Home from './Pages/Home/Home';
import { BrowserRouter as Router, Switch, Route, Routes } from 'react-router-dom';
import Login from './Pages/Login/Login';
import RegDone from './Pages/RegsiatrationDone/RegDone';
import AdminPanel from './Pages/AdminPanel/AdminPanel';
// import MyCourses from './Pages/MyCourses/MyCourses';
import UserList from './Components/UserList/UserList';
import LearningPathHpc from './Pages/LearningPathHPC/LearningPathHpc';
import Footer from './Components/Footer/Footer';
import AboutUs from './Pages/About/About';
import ContactUs from './Pages/Contact/Contact';
import CourseBasic from './Pages/CoursesBasic/CourseBasic';
import BulkRegistration1 from './Pages/BulkRegistration1/BulkRegistration';
import BulkRegDone from './Pages/BulkRegistrationDone/BulkRegDone';
import Testing from './Components/Testing/Testing';
import BulkList from './Components/BulkList/BulkList';
import { createContext , useState} from 'react';
import Profile from './Pages/Users/UserProfilePage/Profile';
import BecomeInstructor from './Pages/BecomeInstructor/BecomeInstructor';
import Success from './Pages/Stripe/Success';
import Cancel from './Pages/Stripe/Cancel';
import CourseInfoPage from './Pages/Course/CourseInfoPage/CourseInfoPage';
import AllCourses from './Pages/Course/AllCourses/AllCourses';
import CourseList from './Components/Admin/CourseList/CourseList';
import InstructorCourses from './Pages/Instructor/InstructorCourses/InstructorCourses';
import InstructorAddCourseInfo from './Pages/Instructor/InstructorAddCourseInfo/InstructorAddCourseInfo';
import AdminLogin from './Pages/AdminLogin/AdminLogin';
import InstrucEnrolledCourses from './Pages/Instructor/InstrucEnrolledCourses/InstrucEnrolledCourses';
import SimpleTests from './Pages/SimpleTests/SimpleTests';
import Statistics from './Pages/Statistics/Statistics';
import AdminCourseSuggestions from './Pages/AdminCourseSuggestions/AdminCourseSuggestions';
import AdminPublishRequests from './Components/Admin/AdminPublishRequests/AdminPublishRequests';
import Preview from './Pages/Instructor/PreviewCourse/Preview';
import AssignmentSubmissions from './Pages/AssignmentSubmissions/AssignmentSubmissions';
import Message from './Pages/RegDoneMessage/Message';
import CertificateRequests from './Pages/CertificateRequests/CertificateRequests';
import CheckProgress from './Pages/CheckProgress/CheckProgress';
import Home1 from './componentss/Home1';
import PlayQuizEntry from './componentss/PlayQuizEntry';
import CourseBasic1 from './Pages/CoursesBasic/CourseBasic1';
import {disableReactDevtools} from '@fvilers/disable-react-devtools'



export const AuthContext = createContext();

function App() {

  // To remove later 
  const[alert, setAlert] = useState(null);
  const showAlert =(message,type)=> { //to show alert messages
    setAlert({
      msg: message,
      type: type
    })
    setTimeout(() =>{
      setAlert(null)
    },1500)
  }

  const [loggedIn, setLoggedIn] = useState(false);

  return (

    <AuthContext.Provider value={{ loggedIn, setLoggedIn }}>
    <Router>
    <div className="App">
      <Routes>
                 <Route exact path='/testing' element={< Testing />}></Route>
                 <Route exact path='/simple-testing' element={< SimpleTests />}></Route>
                 <Route exact path='/instructor' element={< BecomeInstructor />}></Route>
                 <Route exact path='/instructor-preview' element={< Preview />}></Route>
                 <Route exact path='/instructor-courses' element={< InstructorCourses />}></Route>
                 <Route exact path='/instructor-course-requests' element={< CertificateRequests />}></Route>
                 <Route exact path='/instructor-enrolled-courses' element={< InstrucEnrolledCourses />}></Route>
                 <Route exact path='/check-submissions' element={< AssignmentSubmissions />}></Route>
                 <Route exact path='/check-progress' element={< CheckProgress />}></Route>

                 <Route exact path='/update-course' element={< InstructorAddCourseInfo />}></Route>
                 <Route exact path='/add-quiz' element={< Home1 showAlert={showAlert} />}></Route>
                 <Route exact path='/play-quiz' element={< PlayQuizEntry />}></Route>

                 <Route exact path='/admin-course-suggestions' element={< AdminCourseSuggestions />}></Route>
                 <Route exact path='/stripe/callback' element={< Testing />}></Route>
                 <Route exact path='/checkout-success' element={< Success />}></Route>
                 <Route exact path='/checkout-cancel' element={< Cancel />}></Route>
                 <Route exact path='/user-profile' element={<><Navbar/><Profile/></>}></Route>
                 <Route exact path='/navbar' element={<><Navbar/></>}></Route>
                 <Route exact path='/' element={< Home />}></Route>
                 <Route exact path='/home' element={< Home />}></Route>
                 <Route exact path='/about' element={< AboutUs />}></Route>
                 <Route exact path='/contact' element={< ContactUs />}></Route>
                 <Route exact path="/registration" element={<><Navbar/><Final_form/></>}/>
                 {/* <Route exact path="/bulk-registration" element={<><Navbar/><BulkRegistration/></>}/> */}
                 <Route exact path="/bulk-registration" element={<><Navbar/><BulkRegistration1/></>}/>
                 <Route exact path="/login" element={<Login/>}/>
                 <Route exact path="/done" element={<Message/>}/>
                 <Route exact path="/bulkregdone" element={<BulkRegDone/>}/>
                 <Route exact path="/admin" element={<AdminPanel/>}/>
                 <Route exact path="/admin-users" element={<UserList/>}/>
                 <Route exact path="/admin-publish-req" element={<AdminPublishRequests/>}/>
                 <Route exact path="/admin-courses" element={<CourseList/>}/>
                 <Route exact path="/admin-bulk" element={<BulkList/>}/>
                 {/* <Route exact path="/my-courses" element={<MyCourses/>}/> */}
                 <Route exact path="/courses" element={<><Navbar /><AllCourses/><Footer /></>}/>
                 <Route exact path="/courseEnroll" element={<><Navbar /><CourseBasic1/><Footer /></>}/>
                 {/* <Route exact path="/courseEnroll" element={<><Navbar /><CourseBasic/><Footer /></>}/> */}
                 <Route exact path="/course-info" element={<><Navbar /><CourseInfoPage/><Footer /></>}/>
                 <Route exact path="/learning-path" element={<><Navbar /><LearningPathHpc/> <Footer /></>}/>
                 


                 {/* <Route exact path="/admin-login" element={<><AdminLogin /></>}/> */}
                 <Route exact path="/admin-login" element={<><AdminLogin /></>}/>
                 <Route exact path="/admin-statistics" element={<><Statistics /></>}/>
          </Routes>
    </div>
    </Router>
    </AuthContext.Provider>
    
  );
}

export default App;
