import React, { useEffect, useRef, useState } from 'react'
import { Link, useHistory } from 'react-router-dom';
import AboutCourse from '../../../Components/Courses/AboutCourse';
import CourseDiscussion from '../../../Components/Courses/CourseDiscussion';
import CourseLearning from '../../../Components/Courses/CourseLearning';
import CourseAnnouncements from '../../../Components/Courses/CourseAnnouncements';

import axios, * as others from 'axios'; 
import { PDFViewer } from "@react-pdf/renderer";
import "./Preview.css"


export default function Preview() {
  
  const [courseInfoArray, setCourseInfoArray] = useState([]);
  const courseIdFromSessionStorage = sessionStorage.getItem('selectedCourseId');
  const userIdFromSessionStorage = sessionStorage.getItem('userId');
  const tokenFromSessionStorage = sessionStorage.getItem('Token');

  const [selectedLesson, setSelectedLesson ] = useState('');
  const [selectedComponent, setSelectedComponent] = useState('about');
  const [value, setvalue] = useState('about');

  const [completedLessons, setCompletedLessons] = useState({});


  const handleComponentClick = (componentName) => {
    setSelectedComponent(componentName);
    setvalue(componentName);
  };



  useEffect(() => {
    const url = "http://localhost:8000/api/course/" + courseIdFromSessionStorage + "/getlessons";
    // console.log(url);
      axios.get(url, {
        headers: {
          'Authorization': `Bearer ${tokenFromSessionStorage}`,
        }
      })
      .then((response) => {
        // console.log(response);
        console.log(response.data);
        setCourseInfoArray(response.data);
        
        if (response.data.length > 0) {
          const firstLessonId = response.data[0]._id;
          setSelectedLesson(`http://localhost:8000/api/course/${courseIdFromSessionStorage}/lesson/${firstLessonId}/video`);
        }
      })
      .catch((error) => {
        console.log(error + " errrrr");
      })
  }, []);

  const markAsComplete = (lessonId) =>{
    // :courseId/:lessonId/:isCompleted
    const url = "http://localhost:8000/api/togglelessoncompletion/" + courseIdFromSessionStorage + "/" + lessonId + "/" + userIdFromSessionStorage;
    // console.log(url);
      axios.post(url, {
        headers: {
          'Authorization': `Bearer ${tokenFromSessionStorage}`,
        }
      })
      .then((response) => {
        console.log(response.data)
      })
      .catch((error) => {
        console.log(error + " error");
        
      })
      console.log("marking ...")
  } 


  const renderList = () => {
    return courseInfoArray.map((lesson, index) => {
      const isCompleted = completedLessons[lesson._id] || false;

      return (
        <div key={index} className="LessonInfo py-3 ">
          <div className='d-flex align-items-center'>
          <input
            type="checkbox"
          />
          <span className="ms-2">{index + 1} : </span>
          <span
            onClick={() => {
              handleLessonVideoClick(lesson);
            }}
            className="ms-2 lessonnames"
          >
            {lesson.title}
          </span>
          </div>
          <i
            className="fa-solid fa-cloud-arrow-down FooterNavbarCommonLinks"
            onClick={() => handleDownloadPDF(lesson)}
          ></i>
        </div>
      );
    });
  };

  const handleLessonCompletionToggle = (lessonId) => {
    setCompletedLessons((prevState) => ({
      ...prevState,
      [lessonId]: !prevState[lessonId],
    }));
    console.log("you clicked handleLessonCompletionToggle")
    markAsComplete(lessonId)
  };


  const handleDownloadPDF = (lesson) => {
    console.log("Lesson ID "+lesson._id);

    const downloadUrl = 'http://localhost:8000/api/course/' +courseIdFromSessionStorage +'/lesson/'
    + lesson._id+'/pdf' ; 

    axios({
      url:downloadUrl ,
      method:'GET',
      responseType: 'blob',
    }).then((response)=>{
      // console.log(response.data);
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute('download', 'file.pdf');
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);

    }).catch((err)=>{
      console.error('Error downloading the file:', err);
    });
  };

  const handleLessonVideoClick= (lesson) => {
    // console.log("THis is the video" + lesson._id);
    const lessonId = lesson._id;
    // console.log("http://localhost:8000/api/course/" + courseIdFromSessionStorage +"/lesson/" + lessonId + "/video");
    setSelectedLesson("http://localhost:8000/api/course/" + courseIdFromSessionStorage + "/lesson/" + lessonId + "/video")
  };


  // const Component1 = () => {
  //   return (
  //     <div className={selectedComponent === 'Component1' ? 'selected' : ''} onClick={() => handleComponentClick('Component1')}>
  //       Component 1
  //     </div>
  //   );
  // };

  // const Component2 = () => {
  //   return (
  //     <div className={selectedComponent === 'Component2' ? 'selected' : ''} onClick={() => handleComponentClick('Component2')}>
  //       Component 2
  //     </div>
  //   );
  // };


  return (
    <div className=''>
      <h1 className="text-center my-5 text-decoration-underline">
        Course preview
      </h1>
      <div className="CourseInfoMaincontent">
        <div className="row w-100 m-0">
          <div className="CourseInfoAndVideo col-lg-8 p-0">
            <div className="courseJustVideo text-center w-100">
              { 
                selectedLesson &&  
                <video className="CourseInfoVideo courseLessonVideo" src={selectedLesson} controls autoPlay></video>
              }
            </div>

            <div className="CourseInfoPageNav pt-2 d-flex justify-content-around">
              <div className={selectedComponent === 'about' ? 'selected' : 'about'} onClick={() => handleComponentClick('about')}>
                About
              </div>
              <div className={selectedComponent === 'learning' ? 'selected' : 'learning'} onClick={() => handleComponentClick('learning')}>
                Learning Materials
              </div>
            </div>

            <div className="SelectedComponentValue pt-4">
              {value === 'learning' && <CourseLearning></CourseLearning>}
              {value === 'discussion' && <CourseDiscussion></CourseDiscussion>}
              {value === 'about' && <AboutCourse></AboutCourse>}
              {value === 'announcements' && <CourseAnnouncements />}
            </div>
          </div>
          <div className="CourseInfoLessons col-lg-4">
            <p className="LessonsHeader">
              Lessons
            </p>
            {renderList()}

            

          </div>
        </div>
      </div>
    </div>
  )
};
