import mongoose from "mongoose";
const { ObjectId } = mongoose.Schema.Types;
const lessonSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            trim: true,
            minlength: 3,
            maxlength: 320,
            required: true,
        },
        quiz: {
          type: Boolean,
          default: false,
        },
        content: {
            type: String,
            required: true,
        },
        video: {
            type: String,
        },
        pdf: {
            type: String,
        },
        assignment: {
          title: {
            type: String,
            required: false,
          },
          description: {
            type: String,
            required: false,
          },
          assignmentPdf: {
            type: String,
            required: false,
          },
          submissions: [
            {
              user: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "User",
              },
              file: {
                type: String,
              },
              submittedAt: {
                type: Date,
                default: Date.now,
              },
            },
          ],
        },
        suggestions: [
            {
              // admin: {
              //   type: mongoose.Schema.Types.ObjectId,
              //   ref: "Admin",
              // },
              text: {
                type: String,
                required: true,
              },
              createdAt: {
                type: Date,
                default: Date.now,
              },
            },
          ],
    },
    { timestamps: true }
);

export default mongoose.model("Lesson", lessonSchema);
