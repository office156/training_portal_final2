const pdf = require('pdf-creator-node');
const fs = require('fs');
const path = require('path');
const Course = require('../models/course');
const User = require('../models/user');
const jwt = require('jsonwebtoken');


// export const generateCertificate = async (req, res) => { 
//   const courseId = req.params.courseId;
//   const userId = req.params.userId;

//   try {
//     const course = await Course.findById(courseId);
//     const user = await User.findById(userId);

//     // Check if user has completed all the lessons of the course
//     const lessonsCompleted = user.enrolledCourses.find(
//       (enrollment) =>
//         enrollment.courseId.toString() === courseId.toString()
//     ).lessonsCompleted;

//     if (lessonsCompleted.length !== course.lessons.length) {
//       return res.status(400).json({ error: 'All course lessons not completed' });
//     }

//     // Load certificate template file
//     const templatePath = path.join(__dirname, '..', 'templates', 'certificate.html');
//     const templateHtml = fs.readFileSync(templatePath, 'utf8');

//     // Define data for PDF generation
//     const data = {
//       name: user.name,
//       courseName: course.name,
//       completionDate: new Date().toLocaleDateString(),
//     };

//     // Define options for PDF generation
//     const options = {
//       format: 'A4',
//       orientation: 'portrait',
//       border: '10mm',
//       header: {
//         height: '45mm',
//       },
//       footer: {
//         height: '28mm',
//       },
//     };

//     // Generate PDF from template
//     const pdfBuffer = await pdf.create(templateHtml, data, options);

//     // Send generated PDF as response
//     res.set('Content-Type', 'application/pdf');
//     res.send(pdfBuffer);
//   } catch (error) {
//     console.log(error);
//     res.status(500).json({ error: 'Server error' });
//   }
// };

// export const generateCertificate = async (req, res) => { 
//   const courseId = req.params.courseId;

//   try {
//     // Get the user ID from the token
//     const authHeader = req.headers.authorization;
//     if (!authHeader) {
//       return res.status(401).json({ error: 'Error: No authorization header found' });
//     }

//     const token = authHeader.split(' ')[1];
//     const decoded = jwt.verify(token, process.env.JWT_SECRET);
//     const userId = decoded._id;

//     const course = await Course.findById(courseId);
//     const user = await User.findById(userId);

//     // Check if user has completed all the lessons of the course
//     const lessonsCompleted = user.enrolledCourses.find(
//       (enrollment) =>
//         enrollment.courseId.toString() === courseId.toString()
//     ).lessonsCompleted;

//     if (lessonsCompleted.length !== course.lessons.length) {
//       return res.status(400).json({ error: 'All course lessons not completed' });
//     }

//     // Load certificate template file
//     const templatePath = path.join(__dirname, '..', 'templates', 'certificate.html');
//     const templateHtml = fs.readFileSync(templatePath, 'utf8');

//     // Define data for PDF generation
//     const data = {
//       name: user.name,
//       courseName: course.name,
//       completionDate: new Date().toLocaleDateString(),
//     };

//     // Define options for PDF generation
//     const options = {
//       format: 'A4',
//       orientation: 'portrait',
//       border: '10mm',
//       header: {
//         height: '45mm',
//       },
//       footer: {
//         height: '28mm',
//       },
//     };

//     // Generate PDF from template
//     const pdfBuffer = await pdf.create(templateHtml, data, options);

//     // Send generated PDF as response
//     res.set('Content-Type', 'application/pdf');
//     res.send(pdfBuffer);
//   } catch (error) {
//     console.log(error);
//     res.status(500).json({ error: 'Server error' });
//   }
// };
