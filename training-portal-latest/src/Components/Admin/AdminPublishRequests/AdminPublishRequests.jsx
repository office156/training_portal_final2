import React, { useState } from 'react';
import "./CourseList.css";
import Sidebar from '../../Sidebar/Sidebar';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import { CircularProgress, Button } from '@mui/material';

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};





export default function AdminPublishRequests() {


  const navigate = useNavigate();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [rows, setRows] = useState([])
  const [selectedEmail, setSelectedEmail] = useState();
  const [inputValue , setInputValue] = useState("");
  const [message , setMessage] = useState("");
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [lessonId , setlessonId] = useState("");
  const [lessonTitle , setlessonTitle] = useState("");
  const [adminname , setadminname] = useState("")
  const [adminmail , setadminmail] = useState("")

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };


  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };



  useEffect(() => {
    fetchRequests()
    
    setadminmail(sessionStorage.getItem("email"))
    setadminname(sessionStorage.getItem("name"))
    }, []);


    const fetchRequests = () =>{
      const url = "http://localhost:8000/api/admin/list-requests";
      // console.log(url);
      // console.log(userFromSessionStorage);
      axios.get(url).then((response) => {
        console.log(response.data)
        setRows(response.data)
      })
        .catch((error) => {
          // console.log(error)
          // console.log(error.response.data)
          // alert()
        });
    }
  

  const check = "E:/training-portal/src/Images/ales.jpg"

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  // useEffect(() => {
  //   fetchCourses()
  // }, []);

  // const fetchCourses = () => {
  //   axios.get('http://localhost:8000/api/allcourses').then((response) => {
  //     console.log(response.data)
  //     setRows(response.data)
  //   })
  //     .catch((error) => {
  //       console.log(error)
  //     });
  // }

  const handleAction = (id) => {
    const userConfirmed = window.confirm('Please give suggestions on why you are rejecting this request. Are you sure you want to reject ?');
    if (userConfirmed) {
      RejectCourse(id)
    } else {
      // The user clicked 'Cancel', do nothing or provide feedback
      // alert('Action canceled.');
    }
  }

//   router.post("/admin/approve-publish/:courseId", adminApproveCoursePublishRequest);
// router.post("/admin/unapprove-publish/:courseId", adminUnapproveCoursePublishRequest);

  const AcceptCourse = (id) => {
    const url = "http://localhost:8000/api/admin/approve-publish/" + id 
    axios.post(url).then((response) => {
      console.log(response.data)
      fetchRequests()
      // setRows(response.data)
      // fetchCourses()
    })
      .catch((error) => {
        console.log(error)

      });
  };

  const RejectCourse = (id) => {
    const url = "http://localhost:8000/api/admin/unapprove-publish/" + id 
    axios.post(url).then((response) => {
      console.log(response.data)
      fetchRequests()
      // setRows(response.data)
      // fetchCourses()
    })
      .catch((error) => {
        console.log(error)

      });
  };

  const AddSuggestion = async () => {
    // /admin/suggestion/course/:courseId/
    const url = "http://localhost:8000/api/admin/suggestion/course/" + lessonId
    const text = inputValue
    try {
      const response = await axios.post(url, { text });

      // Handle the response here, e.g., display a success message
      setInputValue("")
      console.log(response.data);
      handleClose()
    } catch (error) {
      // Handle errors, e.g., show an error message
      console.error('Error posting data:', error);
    }
  };

  // const AddSuggestion = async() => {
  //   // Do something with the input value (e.g., save it or use it as needed)
  //   console.log(inputValue);

  //   const url2 = "http://localhost:8000/api/admin/suggestion/course/" + courseFromSessionStorage + "/lesson/"+ lessonId
  //   console.log(lessonTitle)

  //   const text = lessonTitle + " :- " + inputValue

  //     try {
  //       const response = await axios.post(url2, { text });
  
  //       // Handle the response here, e.g., display a success message
  //       setInputValue("")
  //       console.log(response.data);
  //     } catch (error) {
  //       // Handle errors, e.g., show an error message
  //       console.error('Error posting data:', error);
  //     }



  // };

  return (
    <div className='userListTop'>
                  <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2" className='mb-3'>
            Enter your suggestion
          </Typography>
          {/* <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
          </Typography> */}
          <TextField id="outlined-basic" label="suggestion" variant="outlined" sx={{width:"100%"}} value={inputValue}
          onChange={handleInputChange}/>
          <br />
          <div className='mt-5 text-end'>
          <Button variant="outlined" onClick={AddSuggestion}>Enter</Button>
          </div>
          
        </Box>
      </Modal>
      <Sidebar className="adminPanelSidebartag" name={adminname} mail={adminmail}></Sidebar>
      <div className="userListSecond">
        <div className="topLineUserLIstPage py-5 container d-flex justify-content-between">
          <div className="headingUserList">
            Publish requests
          </div>
          {/* <div className="UserListLinks d-flex justify-content-between">
            <div className="UserListTopLink1 px-3">Download in excel file</div>
            <div className="UserListTopLink1">Create New User</div>
          </div> */}
        </div>
        <div className="tableUsers container d-flex justify-content-center">
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Course name</TableCell>
                  {/* <TableCell align="right">Price</TableCell> */}
                  <TableCell align="right">Suggestions</TableCell>
                  <TableCell align="right">Accept</TableCell>
                  <TableCell align="right">Reject</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {(rowsPerPage > 0
                  ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  : rows
                ).map((row) => (
                  <TableRow
                    key={row._id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    {/* <TableCell align="right">{row.price}</TableCell>
                    <TableCell align="right">{row.category}</TableCell> */}
                   <TableCell align="right">
                      <button type='button' className='ApproveUser' onClick={()=>{
                        setlessonId(row._id)
                        setlessonTitle(row.name)
                        handleOpen()
                      }}>
                        <i class="fa-solid fa-pen deleteIcon border p-2"></i>
                      </button>

                    </TableCell>
                    <TableCell align="right">
                    <button type='button' className='ApproveUser' onClick={() => AcceptCourse(row._id) }>
                        {/* {row.isApproved ? <i class="fa-solid fa-minus" onClick={() => DeleteCourse(row._id) }></i> : <i class="fa-solid fa-check" onClick={() => DeleteCourse(row._id) }></i>} */}
                        <i class="fa-solid fa-check deleteIcon border p-2"></i>
                      </button>
                    </TableCell>
                    <TableCell align="right">
                      <button type='button' className='ApproveUser' onClick={() => handleAction(row._id) } >
                        {/* {row.isApproved ? <i class="fa-solid fa-minus" onClick={() => DeleteCourse(row._id) }></i> : <i class="fa-solid fa-check" onClick={() => DeleteCourse(row._id) }></i>} */}
                        <i class="fa-solid fa-xmark deleteIcon border p-2"></i>
                      </button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputProps: {
                        'aria-label': 'rows per page',
                      },
                      native: true,
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </TableContainer>
        </div>

        {/* <div className="suggestionInput container mt-5">
          Suggestion :
          <input className='ms-2' type="text" />
          <button className='ms-2 btn btn-dark' onClick={()=>{
            Givesuggestion()
          }}>Add</button>
        </div> */}

      </div>

      <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
      
    </div>
  )
}

