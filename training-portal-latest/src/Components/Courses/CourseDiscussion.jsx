import React, { useState, useEffect } from 'react';
import axios from 'axios';
import "./CourseDiscussion.css"
import InstructorSlider from '../InstructorSlider/InstructorSlider';

export default function CourseDiscussion() {

  const [comments, setComments] = useState([]);
  const [commentContent, setCommentContent] = useState('');

  const [replyCommentId, setReplyCommentId] = useState(null);
  const [replyContent, setReplyContent] = useState('');

  const courseIdFromSessionStorage = sessionStorage.getItem('selectedCourseId');
  const instructorIdFromSessionStorage = sessionStorage.getItem('InstructorId');
  const userIdFromSessionStorage = sessionStorage.getItem('userId');
  const roleFromSessionStoage = sessionStorage.getItem('Role');
  const tokenFromSessionStorage = sessionStorage.getItem('Token');


  useEffect(() => {
    fetchComments();
  }, []);

  const fetchComments = async () => {
    // console.log(userIdFromSessionStorage);
    try {
      const url = "http://localhost:8000/api/course/" + courseIdFromSessionStorage + "/forum/posts";
      const response = await axios.get(url, {
        headers: {
          'Authorization': `Bearer ${tokenFromSessionStorage}`,
        }
      }).then((response) => {
        // console.log(response.data);
        setComments(response.data)
      }).catch((error) => {
        console.log(error);
      })
    } catch (error) {
      console.error('Error fetching comments:', error);
    }
  };

  const handleCommentContentChange = (event) => {
    setCommentContent(event.target.value);
  }

  const handleSubmitComment = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8000/api/course/"
      + courseIdFromSessionStorage + "/forum";
    // console.log('handle submit  url ' + url + ' and ' + commentContent);
    try {
      const response = await axios.post(url, {
        headers: {
          'Authorization': `Bearer ${tokenFromSessionStorage}`
        },
        userId: roleFromSessionStoage === 'Instructor' ? instructorIdFromSessionStorage : userIdFromSessionStorage,
        content: commentContent,
      })
      const updatedComments = [...comments, response.data];
      setComments(updatedComments);
      fetchComments();
      setCommentContent('');
    } catch (error) {
      console.log('Error posting discussion post' + error)
    }
  }

  const handlePostReply = (commentId) => {
    setReplyCommentId(commentId);
  };

  const handleReplyContentChange = (event) => {
    setReplyContent(event.target.value);
  };

  const handleSubmitReply = async (event) => {
    event.preventDefault();
    const url = "http://localhost:8000/api/course/"
      + courseIdFromSessionStorage + "/forum/"
      + replyCommentId + "/reply";

    try {
      const response = await axios.post(url, {
        headers: {
          'Authorization': `Bearer ${tokenFromSessionStorage}`
        },
        userId: roleFromSessionStoage === 'Instructor' ? instructorIdFromSessionStorage : userIdFromSessionStorage,
        content: replyContent,
      }).then((response) => {

        const updatedComments = [...comments, response.data];
        setComments(updatedComments);
        setReplyCommentId(null);
        setReplyContent('');
        setReplyCommentId('');
      })
    } catch (error) {
      console.error('Error posting reply:', error);
    }
  };


  return (
    <div>
      <form onSubmit={handleSubmitComment}>
        <input type="text" value={commentContent} onChange={handleCommentContentChange} />
        <button className="p-2 loginButton post-button" type="submit">Post Comment</button>
      </form>
      {comments.map((comment) => (
        <div key={comment._id} className="comment-container">
          <p className="comment-content">{comment.content}</p>
          <div className="replies-container">
            {comment.replies.map(reply => (
              <p key={reply._id} className="reply-content">{reply.content}</p>
            ))}
            <button className="p-2 loginButton reply-button" onClick={() => handlePostReply(comment._id)}>Reply</button>
          </div>
          {replyCommentId === comment._id && (
            <form className="reply-form" onSubmit={handleSubmitReply}>
              <input type="text" value={replyContent} onChange={handleReplyContentChange} name="" id="" />
              <button className="p-2 loginButton post-button" type="submit">Post Reply</button>
            </form>
          )}
        </div>
      ))}
    </div>
  )

};
