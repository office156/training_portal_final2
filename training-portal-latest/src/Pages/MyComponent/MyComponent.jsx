import React from 'react';

const MyComponent = ({ dictionary }) => {
  // Check if the dictionary exists and is not empty
  if (dictionary && Object.keys(dictionary).length > 0) {
    return (
      <div>
        <h2>Dictionary:</h2>
        <ul>
          {Object.entries(dictionary).map(([key, value]) => (
            <li key={key}>{`${key}: ${value}`}</li>
          ))}
        </ul>
      </div>
    );
  } else {
    // Render something else or nothing if the dictionary doesn't exist or is empty
    return <div>No dictionary available.</div>;
  }
};

export default MyComponent;
