import React, { useEffect, useState } from 'react';
import { json } from 'react-router-dom';
import Game from './Game';
import axios from 'axios';


const PlayQuizEntry = () => {

  const [seq, setSeq] = useState("")  
  const quizsInitial = []
  const [quizs, setQuizs] = useState(quizsInitial)
  const lesson = sessionStorage.getItem('selectedLesson');
  var [val, setVal] = useState('')


  // var TEST = localStorage.getItem("val");

  // var windowsvariable = sessionStorage.getItem(window.val);

  useEffect(() => {
    fetchallquiz()
  }, []);

  const fetchallquiz = async () => {
    try {
      const response = await axios.get(`http://localhost:8000/api/quiz/fetchallquiz/${lesson}`);
      const json = response.data;
      console.log(json, "AXIOS");
      setSeq("1");
      setQuizs(json);
      sessionStorage.setItem("val", 0);
    } catch (error) {
      console.error(error);
    }
  };

  console.log(seq);

  const myFunction = () =>{
    console.log(sessionStorage.getItem("val"))
    setVal(sessionStorage.getItem("val"))
    const disableBtn=()=> {
      document.getElementById('btn').disabled = true;
    }
    disableBtn();
  }


  return (
    <div className='container'>

      <h2>Quiz</h2>

    {quizs.map((quiz) => {
          return (
            <Game quiz={quiz} key={quiz._id} />
            
          );
    })}
  
    <button className={seq=='1' ? 'mx-2' : 'd-none mx-2' } id="btn" onClick={myFunction}>  GENERATE SCORE </button>
    
    <div className={seq=='1' ? 'd-flex' : 'd-none' }> Your Score is : {val} </div>
  
    {/* <button >GENERATE SCORE</button>  */}
    <div>
    <a href="http://localhost:3000/play-quiz" class="my-2" tabIndex="-1" role="button">RESET</a>
    </div>
    </div>
  )
}

export default PlayQuizEntry;
