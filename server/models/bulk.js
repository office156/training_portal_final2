import mongoose from "mongoose";
const { Schema } = mongoose;

const bulkSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            trim: true,
            required: true,
        },
        email: {
            type: String,
            trim: true,
            unique: true,
        },
        aadharcard: {
            type: String,
        },
        excel: {
            type: String,
        }
    }
);

export default mongoose.model("Bulk", bulkSchema);
