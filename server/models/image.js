import mongoose from "mongoose";
const { Schema } = mongoose;

const imageSchema = new mongoose.Schema(
    {
        image: {
            type: String,
        }
    }
);

export default mongoose.model("Image", imageSchema);
