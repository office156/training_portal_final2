import React, { useState } from 'react'
import "./CourseBasic.css"
import axios from 'axios'
import { ToastContainer , toast } from 'react-toastify'
import { loadStripe } from '@stripe/react-stripe-js'
import { useLocation, useNavigate } from 'react-router-dom'
import { useEffect } from 'react'

export default function CourseBasic1() {

    const userFromSessionStorage = sessionStorage.getItem('userId');
    const courseFromSessionStorage = sessionStorage.getItem('selectedCourseId');
    const token = sessionStorage.getItem('Token');
    // const [selectedCourse , setSelectedCourse] = useState();
    const [courseName , setCourseName] = useState();
    const [courseDescription , setCourseDescription] = useState();
    const [coursePrice , setCoursePrice] = useState();
    const [coursewhatLearn , setCoursewhatLearn] = useState();
    const [coursePrerequisites , setCoursePrerequisites] = useState();
    const [courseCategory , setCourseCategory] = useState();
    const navigate = useNavigate();
    // const location = useLocation();
    // const courseId = props.state;



    useEffect(() => {
        // const userFromSessionStorage = sessionStorage.getItem('userId');
        // console.log(courseFromSessionStorage)
        const url = "http://localhost:8000/api/coursedetails/" + courseFromSessionStorage 
        axios.get(url).then((response) => {
        //   console.log(response.data)
        //   setSelectedCourse(response.data)
        setCourseName(response.data.name);
        setCoursePrice(response.data.price);
        setCourseCategory(response.data.category);
        setCoursePrerequisites(response.data.prerequisites);
        setCourseCategory(response.data.category);
        setCourseDescription(response.data.description);
        setCoursewhatLearn(response.data.whatyouwilllearn);
        })
          .catch((error) => {
            console.log(error.data)
          });
        },[]);


    async function enroll() {
        
        const value = sessionStorage.getItem("userId");
        if (value) {
            try {
                const url2 = "http://localhost:8000/api/course/enroll/" + courseFromSessionStorage
                const {data} = await axios.post(url2,null,{
                    headers: {
                      'Authorization': `Bearer ${token}`
                    }
                  })
    
    
                  console.log(data)
                  window.location.href = data
                // navigate("/user-profile")
        
            } catch (error) {
                toast.error(error)
            }
          
        } else {
        //   toast.error("Please login first")
          navigate("/login")
        }



    }



  return (
    <div className='CourseBasicTopHeader container'>

        <div className="InfoAndCard row mb-5">
            <div className="InfoCourseBasic col">
            <p className='CourseVideoTitle mb-5'>
                {courseName}
            </p>
                <div className="InfoCoursePart1 p-3">
                <p className="AboutThisCourse">About this course</p>
                <p className="InfoAboutCourse">
                    {courseDescription}
                </p>
                </div>

                <div className="InfoCoursePart2 p-3">
                <p className="WhatYouLearn">What you’ll learn in this course</p>
                <p className="LearningCourseList d-flex justify-content-start">
                    {coursewhatLearn}
                </p>
                </div>
            </div>
            <div className="videoWithTitle col-md-auto">
            <video controls src="https://cdn.pixabay.com/vimeo/804706404/Beach%20-%20153167.mp4?width=1280&hash=5420e7514e7134ab9749b3eaf2896033a745523a" className="CourseVideo">Something</video>

        </div>
        </div>

        <div className="row">
        <div className="CoursePrerequisites col-8 mb-4">
            <div className='prereq p-3'>
            <div className="PrerequisitesHeader">
            Prerequisites
            </div>
            <div className="prerequitesText">
                {coursePrerequisites}
            </div>
            </div>
        </div>
        <div className="InfoCourseBasic col-md-3">
                <div className="InfoCourseCard p-3">
                    <div className='d-flex flex-column align-items-center'>
                    <div className="CourseCard d-flex justify-content-center align-items-center">
                    <i class="fa-solid fa-circle-radiation"></i>
</div>

                    </div>
                    <div className="CardHeading">
                      Rs : {coursePrice}
                    </div>
                    <div className="cardList">
{coursewhatLearn}
                    </div>
                    <div className="CardButton text-center" onClick={enroll}>Enroll Now</div>
                    <div className="CourseCardLink text-center">Having any issue? Contact us</div>
                </div>
            </div>

        </div>

        {/* <div className="CoursePrerequisites">
            <div className="PrerequisitesHeader">
            Prerequisites
            </div>
            <div className="prerequitesText">
                {coursePrerequisites}
            </div>
        </div> */}


<ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
    </div>
  )
}
