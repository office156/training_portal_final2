import User from "../models/user";
const stripe = require('stripe')(process.env.STRIPE_SECRET);
import jwt from "jsonwebtoken";
const queryString = require('query-string');

export const makeInstructor = async (req, res) => {
  try {

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res.status(401).json({ error: 'Error : No authorization header found' });
    }

    const token = authHeader.split(' ')[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    // Find the user
    const user = await User.findById(userId).exec();

    // if user don't have stripe account id yet, then create new
    if (!user.stripe_account_id) {
      const account = await stripe.accounts.create({ type: 'standard' });
      user.stripe_account_id = account.id;
    }

    // add "Instructor" to the user's role
    if (!user.role.includes("Instructor")) {
      user.role.push("Instructor");
    }

    // save the user object
    await user.save();

    // create account link based on account id (to be used at frontend to complete onboarding)
    let accountLink = await stripe.accountLinks.create({
      account: user.stripe_account_id,
      refresh_url: process.env.STRIPE_REDIRECT_URL,
      return_url: process.env.STRIPE_REDIRECT_URL,
      type: "account_onboarding"
    })

    // prefill any information such as email, then send url response to frontend
    accountLink = Object.assign(accountLink, {
      "stripe_user[email]": user.email,
    })

    // send the account link as json as response to frontend
    res.send(`${accountLink.url}?${queryString.stringify(accountLink)}`)
  }
  catch (err) {
    console.log(err);
  }
};

export const getAccountStatus = async (req, res) => {
  try {
    const user = await User.findById(req.auth._id).exec();
    const account = await stripe.accounts.retrieve(user.stripe_account_id);

    if (!account.charges_enabled) {
      return res.status(401).send("Unauthorized request");
    } else {
      const statusUpdated = await User.findByIdAndUpdate(
        user._id,
        {
          stripe_seller: account,
          $addToSet: { role: "Instructor" },
        },
        { new: true }
      )
        .select("-password")
        .exec();

      res.send(statusUpdated);
    }
  } catch (err) {
    console.log(err);
  }
};

export const currentInstructor = async (req, res) => {
  try {

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res.status(401).json({ error: 'Error : No authorization header found' });
    }

    const token = authHeader.split(' ')[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    let user = await User.findById(userId).select('-password').exec();

    if (!user.role.includes('Instructor')) {
      return res.sendStatus(403)
    }
    else {
      res.json(user)
    }
  } catch (err) {
    console.log(err);
  }
}