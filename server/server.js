import cors from 'cors'
import express from 'express'
import { readdirSync } from 'fs';
import mongoose from 'mongoose';
import csrf from "csurf";
import cookieParser from "cookie-parser";
import bodyParser from 'body-parser';

const morgan = require('morgan')
require("dotenv").config();

const csrfProtection = csrf({ cookie: true });

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// mongoose connect
mongoose.connect(process.env.DATABASE, {
}).then(() => console.log("DB is connected"))
  .catch((err) => console.log("DB is not connected => ", err));

// middlewares
app.use(cors());
app.use(express.json({ limit: "5mb" }));
app.use(cookieParser());
app.use(morgan("dev"));

// load routes
readdirSync("./routes").map((r) =>
  app.use("/api", require(`./routes/${r}`))
);

csrf
app.use(csrfProtection);

app.get("/api/csrf-token", (req, res) => {
  res.json({ csrfToken: req.csrfToken() });
});

// port and listen
const port = process.env.PORT || 8000;
app.listen(port, () => console.log(`server is running on port ${port}`));
