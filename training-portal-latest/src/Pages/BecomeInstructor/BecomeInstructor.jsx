import axios from 'axios'
import React from 'react'
import { ToastContainer , toast } from 'react-toastify'

export default function BecomeInstructor() {
    

    const becomeInstru = () =>{
        axios.post("http://localhost:8000/api/make-instructor").then((res)=>{
            console.log(res);
            window.location.href = res.data
        }).catch(error=>{
            console.log(error.response)
            toast.error("Stripe failed")
        })
    }

  return (
    <div>
        <h1>Become Instructor</h1>
        <button onClick={becomeInstru}>Start</button>
        <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
    </div>
  )
}
