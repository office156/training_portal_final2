import React, { useState } from 'react'
import SidebarInstructor from '../../../Components/SidebarInstructor/SidebarInstructor';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { useEffect } from 'react';

export default function InstrucEnrolledCourses() {


    const navigate = useNavigate()
    const [myArray , setMyArray] = useState([]);

    useEffect(() => {
        const userFromSessionStorage = sessionStorage.getItem('InstructorId');
        // console.log(courseFromSessionStorage)
        
        const url = "http://localhost:8000/api/instructor/" + userFromSessionStorage + "/enrolledCourses"
        axios.get(url).then((response) => {
          console.log(response.data)
          setMyArray(response.data)
        })
          .catch((error) => {
            console.log(error.data)
          });
        },[]);


    const handleCourseClick = (itemID) => {
      sessionStorage.setItem('selectedCourseId', itemID);
      console.log("Item ID= " + itemID);
      navigate('/course-info');
  
    }


  return (
    <div className='userListTop'>
        <SidebarInstructor className="adminPanelSidebartag"></SidebarInstructor>
            <div className='userCoursesTop userListSecond ms-5'>
      <h1 className="userEnrolledCourses text-center">
        Enrolled Courses
      </h1>
      <div className="AllEnrolledCourseListTop">
      <div>
      {myArray && myArray.map((item) => {
        return (<div className="EachEnrolledCourse d-flex" onClick={()=>handleCourseClick(item._id)}>

        <img className="courseImageTest" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToQeYeClZMrqqq2aaTT2UdbuWtVktz8jQ9gw&usqp=CAU" alt=""  />

        <div className="enrolledCourseInfo">
          <div className="enrolledCourse">
          <div className="courseMaintitle mb-2">
          {item.name}
          </div>
          <span className="enrolledCourseSubtext">
            {item.category}
          </span>
          </div>
        </div>

        <div className="nextPageIcon">
        <i class="fa-solid fa-angle-right"></i>
        </div>
      </div>);
      })}
    </div>
    </div>
    </div>
    </div>
  )
}
