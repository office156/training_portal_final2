import React from 'react'
import "./Message.css"

export default function Message() {
  return (
    <div className='MessagePageTop container d-flex justify-content-center align-items-center'>
        <div className="messageHeadingTop">
            <div className="messageHeading text-center">
            Verification Pending
            </div>
            <div className="subtextMessage">
            Dear user , Thank you for registering with us. Currently your account is inactive. You will be able to login after 
            <br />
Photo and Aadhar card verification. You will get a confirmation email on your registered email id after successful verification.
            </div>
        
        <div className="middleText">
        Meanwhile, You can access our free content and resources
        </div>
        <div className="contentLinksMessagePage">
            <div className="row">
                <div className="col">
                    <div className="linkTag">OpenMP</div>
                    <div className="linkTag">Message Passing Interface (MPI)</div>
                    <div className="linkTag">CUDA Programming</div>
                </div>
                <div className="col">
                    <div className="linkTag">Profiling</div>
                    <div className="linkTag">SPACK Utility</div>
                    <div className="linkTag">Julia Programming</div>
                </div>
                <div className="col">
                    <div className="linkTag">DL/ AI/ ML</div>
                    <div className="linkTag">SLURM</div>
                    <div className="linkTag">HPC basics</div>
                </div>
            </div>
        </div>
        </div>
    </div>
  )
}
