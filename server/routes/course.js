import express from "express";
const router = express.Router();

const bodyParser = require("body-parser");
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

const multer = require("multer");
const path = require("path");

const Client = require('ssh2-sftp-client');
const sftp = new Client();

router.use(express.static("public"));

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(
            null,
            path.join(__dirname, "../public/userImages"),
            function (error, success) {
                if (error) throw err;
            }
        );
    },
    filename: function (req, file, cb) {
        const name = Date.now() + "-" + file.originalname;
        cb(null, name, function (error1, success1) {
            if (error1) throw error1;
        });
    },
});

const upload = multer({ storage: storage });

import { createCourse, enrollCourse, stripeSuccess, listEnrolledCourses, addLesson, getAllLessons, getAllLessonVideos, getAllLessonPdfs, markAsCompleted, allCoursesList, countTotalLessonInCourse, courseDetails, allCoursesListSort, oneVideoOfCourse, instructorAllCourses, getCourseImage, updateLesson, deleteLesson, deleteCourse, updateCourse, onePdfOfCourse, discussionPost, discussionPostReply, allDiscussionPosts, postAnnouncement, allAnnouncements, editAnnouncement, deleteAnnouncement, toggleLessonCompletion, generateDummyCertificate, enrollCourseWithoutFees, instructorCourseCount, updateLessonAdmin, deleteLessonAdmin, updateCourseAdmin, deleteCourseAdmin, instructorEnrolledCourses, topCourses, topUsers, approveCourse, countTotalCourses, coursesMonthWise, adminGetAllLessonVideos, adminOneVideoOfCourse, adminOnePdfOfCourse, adminCourseDetails, adminAddSuggestionToLesson, getSuggestionOfLesson, getAllSuggestionOfCourse, adminGetAllSuggestionOfCourse, getAllUnapprovedCourse, unapproveCourse, instructorRequestCoursePublish, adminApproveCoursePublishRequest, adminUnapproveCoursePublishRequest, adminListCoursePublishRequest, deleteSuggestionOfLesson, allPublishedCoursesList, adminAddSuggestionToCourse, getSuggestionOnCourse, deleteSuggestionOnCourse, getAllSuggestionsCombined, deleteSuggestion, addAssignment, uploadAssignment, updateAssignment, deleteAssignment, getAssignmentDetailsOfLessonWithSubmission, getAssignmentDetailsOfLessonWithoutSubmission, getAllAssignmentsDetailsWithoutSubmission, getAllAssignmentsDetailsWithSubmission, getAllSubmissionsForLesson, getAllUserSubmissions, getAllCourseSubmissions, downloadAssignmentPdf, downloadSubmissionPdf, generateCertificateRequest, getCertificateRequestsByInstructorId, approveCertificateRequest, disapproveCertificateRequest, getCertificateRequestDetails, getApprovedCertificates, getDisapprovedCertificates, getCertificateRequestByUser, instructorRequestCourseUnPublish, getLessonCompletion, getUserProgress, addFinalAssignment, downloadFinalAssignmentPdf, getFinalAssignment, addquiz, getallquiz, deleteQuiz, checkCertificateRequest  } from '../controllers/course';

// middlewares
import { isInstructor, isAdmin } from "../middlewares";
import { generateCertificate, ummarkAsCompleted } from "../controllers/course";

router.post("/create", upload.single("image"), isInstructor, createCourse);
router.post("/course/enroll/:id", enrollCourse);

// router.post("/course/enroll/:id", enrollCourseWithoutFees);
router.post("/course/enroll/success/:courseId", stripeSuccess);
router.get("/enrolledcourses/:id", listEnrolledCourses);
router.get("/instructor/:instructorId/enrolledCourses", instructorEnrolledCourses);

router.post("/course/:courseId/addlesson", upload.fields([{ name: "video" }, { name: "pdf" }]), isInstructor, addLesson);
router.get("/course/:courseId/getlessons", getAllLessons);
router.get("/course/:courseId/getlessonsvideos", getAllLessonVideos);
router.get("/course/:courseId/getlessonspdfs", getAllLessonPdfs);

// router.post("/course/progress/:courseId/lesson/:lessonId/completed", markAsCompleted);

router.post("/course/progress/:courseId/lesson/:lessonId/user/:userId/completed", markAsCompleted);
router.post("/course/progress/:courseId/lesson/:lessonId/not-completed", ummarkAsCompleted);

router.post("/togglelessoncompletion/:courseId/:lessonId/:userId", toggleLessonCompletion);
router.post("/getLessonCompletion/:courseId/:userId", getLessonCompletion);
router.post("/getLessonCompletion/:courseId", getFinalAssignment);

// testing phase
router.post("/quiz/addquiz", addquiz);
router.get("/quiz/fetchallquiz/:lessonId", getallquiz);
router.delete("/quiz/deletequiz/:quizId", deleteQuiz);



router.get("/getUserProgress/:courseId", getUserProgress);

router.get("/allcourses", allCoursesList);
router.get("/all-courses-published", allPublishedCoursesList);
router.get("/totalcourses", countTotalCourses);

router.get("/allcourseslistsort", allCoursesListSort);
router.get("/coursedetails/:courseId", courseDetails);
router.get("/admin/coursedetails/:courseId", adminCourseDetails);

router.get("/course/:courseId/lessons/count", countTotalLessonInCourse);
router.get("/course/:courseId/lesson/:lessonId/video", oneVideoOfCourse);
router.get("/course/:courseId/lesson/:lessonId/pdf", onePdfOfCourse);
router.get("/course/instructor/:instructorId", instructorAllCourses);
router.get("/course/image/:courseId", getCourseImage);

router.patch("/course/:courseId/lesson/:lessonId/patch", upload.fields([{ name: "video" }, { name: "pdf" }]), isInstructor, updateLesson);
router.delete("/course/:courseId/lesson/:lessonId/delete-lesson", isInstructor, deleteLesson);

// router.patch("/admin/course/:courseId/lesson/:lessonId/patch", upload.fields([{ name: "video" }, { name: "pdf" }]), isAdmin, updateLessonAdmin);
// router.delete("/admin/course/:courseId/lesson/:lessonId/delete-lesson", isAdmin, deleteLessonAdmin);

router.patch("/admin/course/:courseId/lesson/:lessonId/patch", upload.fields([{ name: "video" }, { name: "pdf" }]), updateLessonAdmin);
router.delete("/admin/course/:courseId/lesson/:lessonId/delete-lesson", deleteLessonAdmin);

router.patch("/course/:courseId/delete-course", isInstructor, updateCourse);
router.delete("/course/:courseId/delete-course", isInstructor, deleteCourse);

// router.patch("/admin/course/:courseId/update-course", isAdmin, updateCourseAdmin);
// router.delete("/admin/course/:courseId/delete-course", isAdmin, deleteCourseAdmin);

router.patch("/admin/course/:courseId/update-course", updateCourseAdmin);
router.delete("/admin/course/:courseId/delete-course", deleteCourseAdmin);

router.post("/course/:courseId/forum", discussionPost);
router.post("/course/:courseId/forum/:postId/reply", discussionPostReply); 
router.get("/course/:courseId/forum/posts", allDiscussionPosts);
router.post("/course/:courseId/announcement/post", postAnnouncement);
router.get("/course/:courseId/announcements", allAnnouncements);
router.patch("/course/:courseId/announcement/:announcementId/edit", editAnnouncement);
router.delete("/course/:courseId/announcement/:announcementId/delete", deleteAnnouncement);
router.post("/certificate/course/:courseId/", generateCertificate); 
router.post("/dummycertificate/", generateDummyCertificate);

router.get("/instructor/:instructorId/courseCount", instructorCourseCount);

router.get("/top-courses", topCourses);
router.get("/top-users", topUsers);

router.post("/admin/approve-course/:id", approveCourse);
router.get("/admin/unapproved-courses", getAllUnapprovedCourse);
router.post("/admin/unapprove-course/:id", unapproveCourse);

router.get("/courses/month-wise", coursesMonthWise);

router.get("/admin/course/:courseId/getlessonsvideos", adminGetAllLessonVideos);
router.get("/admin/course/:courseId/lesson/:lessonId/video", adminOneVideoOfCourse);
router.get("/admin/course/:courseId/lesson/:lessonId/pdf", adminOnePdfOfCourse);
router.get("/admin/coursedetails/:courseId", adminCourseDetails);

// course publish (instructor - admin)
router.post("/instructor/request-publish/:courseId", instructorRequestCoursePublish);
router.post("/instructor/request-unpublish/:courseId", instructorRequestCourseUnPublish);
router.get("/admin/list-requests", adminListCoursePublishRequest);
router.post("/admin/approve-publish/:courseId", adminApproveCoursePublishRequest);
router.post("/admin/unapprove-publish/:courseId", adminUnapproveCoursePublishRequest);

// suggestion on lesson
router.post("/admin/suggestion/course/:courseId/lesson/:lessonId", adminAddSuggestionToLesson);
router.get("/lesson/:lessonId/suggestions", getSuggestionOfLesson);
router.get("/courses/:courseId/suggestions", getAllSuggestionOfCourse);
router.get("/admin/courses/:courseId/suggestions", adminGetAllSuggestionOfCourse);
// router.delete("/delete-suggestion-on-lesson/:suggestionId", deleteSuggestionOfLesson);

// suggestion on course overall
router.post("/admin/suggestion/course/:courseId/", adminAddSuggestionToCourse);
router.get("/course-suggestion/:courseId/suggestions", getSuggestionOnCourse);
// router.delete("/delete-suggestion-on-course/:suggestionId", deleteSuggestionOnCourse);
router.delete("/delete-suggestion/:suggestionId", deleteSuggestion);
router.get("/course-all-suggestions/:courseId/", getAllSuggestionsCombined);

// assignment
router.post('/course/:courseId/lesson/:lessonId/add-assignment', upload.single('assignmentPdf'), addAssignment);
router.post('/course/:courseId/add-assignment', upload.single('assignmentPdf'), addFinalAssignment);

router.post('/course/:courseId/lesson/:lessonId/upload', upload.single('file'), uploadAssignment);

router.patch('/course/:courseId/lesson/:lessonId/update-assignment', upload.single('assignmentPdf'), updateAssignment);
router.delete('/course/:courseId/lesson/:lessonId/delete-assignment', deleteAssignment);

router.get('/course/:courseId/lesson/:lessonId/assignment-details', getAssignmentDetailsOfLessonWithSubmission);
router.get('/course/:courseId/lesson/:lessonId/assignment-details-without-submission', getAssignmentDetailsOfLessonWithoutSubmission);

router.get('/course/:courseId/assignment-details-with-submission', getAllAssignmentsDetailsWithSubmission);
router.get('/course/:courseId/assignment-details-without-submission', getAllAssignmentsDetailsWithoutSubmission);

// get all submission for lesson of a particular course
router.get('/course/:courseId/lesson/:lessonId/submissions', getAllSubmissionsForLesson);

// get all submission for a particular course
router.get('/course/:courseId/assignment/submissions', getAllCourseSubmissions);

// get all submission by a particular user
router.get('/user/:userId/all-submissions', getAllUserSubmissions);

// download assignment pdfs
router.get('/course/:courseId/lesson/:lessonId/download-assignment', downloadAssignmentPdf);
router.get('/course/:courseId/download-assignment', downloadFinalAssignmentPdf);
router.get('/course/:courseId/lesson/:lessonId/submission/:submissionId/download-submission', downloadSubmissionPdf);

// generate certificate request
router.post('/course/:courseId/user/:userId/generate-certificate', generateCertificateRequest);
router.post('/course/:courseId/user/:userId/check-certificate', checkCertificateRequest);
router.get('/user/:userId/:courseId/instructor/generate-certificate-requests', getCertificateRequestsByInstructorId);

// update generate certificate request
router.post('/instructor/:requestId/approve-certificate', approveCertificateRequest);
router.post('/instructor/:requestId/disapprove-certificate', disapproveCertificateRequest);

// get details of a particular certificate request
router.get('/instructor/:requestId/certificate-request-details', getCertificateRequestDetails)

// get all approved certificates of a particular instructor
router.get('/instructor/:instructorId/approved-certificate-requests', getApprovedCertificates);

// get all disapproved certificates of a particular instructor
router.get('/instructor/:instructorId/disapproved-certificate-requests', getDisapprovedCertificates);

// get details of a certificate request by a particular user
router.get('/user/:userId/certificate-request-details', getCertificateRequestByUser);

module.exports = router;