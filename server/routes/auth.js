import express from "express";
const router = express.Router();

const bodyParser = require("body-parser");
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

const multer = require("multer");
const path = require("path");

const Client = require('ssh2-sftp-client');
const sftp = new Client();

router.use(express.static("public"));

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(
      null,
      path.join(__dirname, "../public/userImages"),
      function (error, success) {
        if (error) throw err;
      }
    );
  },
  filename: function (req, file, cb) {
    const name = Date.now() + "-" + file.originalname;
    cb(null, name, function (error1, success1) {
      if (error1) throw error1;
    });
  },
});

const upload = multer({ storage: storage });

// controllers
import {
  registeruser,
  login,
  logout,
  currentUser,
  approveUser,
  getAllUsers,
  getAllApprovedUsers,
  getAllUnapprovedUsers,
  countUnapprovedUsers,
  countTotalUsers,
  countApprovedUsers,
  bulkRegister,
  getUserRegistrationDocumentsPhoto,
  getUserRegistrationDocumentsAadhar,
  getAllUserDocuments,
  getUserBulkRegistrationDocumentsAadhar,
  getUserBulkRegistrationDocumentsExcel,
  getAllBulkDocs,
  downloadBulkDocs,
  downloadUserDocs,
  sendEmailToUser,
  getUserDetailsByEmail,
  getUserDetailsById,
  updateUserDetailsById,
  unapproveUser,
  getAllInstructores,
  getInTouch,
  allGetInTouchEmails,
  usersMonthWise,
  loginadminuser
} from "../controllers/auth";

import {
  getAccountStatus,
  currentInstructor
} from "../controllers/instructor";
import { instructorsList } from "../controllers/course";

// middlewares
// import { requireSignin } from "../middlewares";

router.post("/registeruser", upload.fields([{ name: "photo" }, { name: "aadharcard" }]), registeruser);
router.post("/login", login);
router.post("/admin-login", loginadminuser);
router.get("/logout", logout);
router.get("/instructor-list", instructorsList);
router.get("/current-user", currentUser);
router.get("/admin/allusers", getAllUsers);
router.get("/allinstructors", getAllInstructores);
router.get("/admin/allapprovedusers", getAllApprovedUsers);
router.get("/admin/allunapprovedusers", getAllUnapprovedUsers);
router.post("/admin/approveUser/:email", approveUser);
router.post("/admin/unapproveUser/:email", unapproveUser);
router.get("/admin/allusers/count", countTotalUsers);
router.get("/admin/allusers/approved/count", countApprovedUsers);
router.get("/admin/allusers/unapproved/count", countUnapprovedUsers);
router.get("/admin/user/:userId/alldocs", getAllUserDocuments);
router.get("/admin/bulk/excel/:bulkId/", getUserBulkRegistrationDocumentsExcel);
router.get("/admin/bulk/aadhar/:bulkId/", getUserBulkRegistrationDocumentsAadhar);
router.get("/admin/allbulkdocs", getAllBulkDocs);
router.get("/admin/user/photo/:userId", getUserRegistrationDocumentsPhoto);
router.get("/admin/user/aadhar/:userId", getUserRegistrationDocumentsAadhar);
router.post("/register/bulk", upload.fields([{ name: "aadharcard" }, { name: "excel" }]), bulkRegister);
router.get("/userdocs/download/:id", downloadUserDocs);
router.get("/bulkdocs/download/:id", downloadBulkDocs);

// stripe : user account status
// router.post("/get-account-status", requireSignin, getAccountStatus);
// router.post("/current-instructor", requireSignin, currentInstructor);

router.post("/admin/user/send/:email", sendEmailToUser);
router.get("/admin/user/details/email/:email", getUserDetailsByEmail);
router.get("/admin/user/details/id/:id", getUserDetailsById);
router.patch("/admin/user/details/update/:id", updateUserDetailsById);

router.post("/get-in-touch", getInTouch);
router.get("/admin/all-get-in-touch", allGetInTouchEmails);

router.get("/users/month-wise", usersMonthWise);

module.exports = router;