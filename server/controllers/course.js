import User from "../models/user";
import Image from "../models/image";
import Lesson from "../models/lesson";
import Course from "../models/course";
import Assignment from "../models/assignment";
import Progress from "../models/progress";
import DiscussionPost from "../models/discussion";
import jwt from "jsonwebtoken";
import mongoose from "mongoose";
import CertificateRequest from '../models/certificateRequest';
const moment = require("moment");
// const pdf = require("pdf-creator-node");

const stripe = require("stripe")(process.env.STRIPE_SECRET);
const queryString = require("query-string");

import PDFDocument from 'pdfkit';
import fs from 'fs';
import path from 'path';
import nodemailer from 'nodemailer';
import Quiz  from '../models/quiz';

// const generateCertificatePDF = (name, courseName, completionDate) => {
//   // Create a new PDF document
//   const pdf = new PDFDocument();
//   const filePath = path.join(__dirname, "certificate.pdf"); // Define the path where the PDF will be saved

//   // Pipe the PDF content to a file
//   const stream = fs.createWriteStream(filePath);
//   pdf.pipe(stream);

//   // Create the PDF content
//   pdf.fontSize(20).text("Certificate of Completion", { align: "center" });
//   pdf.moveDown(0.5);
//   pdf.fontSize(16).text(`This is to certify that`, { align: "left" });
//   pdf.moveDown(0.5);
//   pdf.fontSize(18).text(`${name}`, { align: "center" });
//   pdf.moveDown(0.5);
//   pdf
//     .fontSize(16)
//     .text(`has successfully completed the course`, { align: "center" });
//   pdf.moveDown(0.5);
//   pdf.fontSize(18).text(`${courseName}`, { align: "center" });
//   pdf.moveDown(0.5);
//   pdf.fontSize(16).text(`on ${completionDate}.`, { align: "center" });

//   // End the PDF document
//   pdf.end();

//   return filePath;
// };

// export default generateCertificatePDF;

const generateCertificatePDF = (name, courseName, completionDate) => {
  // Create a new PDF document
  const pdf = new PDFDocument();
  const filePath = path.join(__dirname, "certificate.pdf"); // Define the path where the PDF will be saved

  // Pipe the PDF content to a file
  const stream = fs.createWriteStream(filePath);
  pdf.pipe(stream);

  // Background color
  pdf.rect(0, 0, 612, 792).fill('#f7f7f7');

  // Add an image or logo if needed
  // pdf.image('path/to/logo.png', 100, 50, { width: 100 });

  // Create the PDF content
  pdf.fontSize(28).fillColor('#333').text("Certificate of Completion", { align: "center" });
  pdf.moveDown(0.5);
  pdf.fontSize(20).fillColor('#555').text(`This is to certify that`, { align: "left" });
  pdf.moveDown(0.5);
  pdf.fontSize(24).fillColor('#333').text(`${name}`, { align: "center" });
  pdf.moveDown(0.5);
  pdf
    .fontSize(20)
    .fillColor('#555')
    .text(`has successfully completed the course`, { align: "center" });
  pdf.moveDown(0.5);
  pdf.fontSize(24).fillColor('#333').text(`${courseName}`, { align: "center" });
  pdf.moveDown(0.5);
  pdf.fontSize(20).fillColor('#555').text(`on ${completionDate}.`, { align: "center" });

  // Certificate border
  pdf.lineWidth(3).rect(50, 150, 512, 400).stroke('#333');

  // End the PDF document
  pdf.end();

  return filePath;
};

export default generateCertificatePDF;

const transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 465,
  secure: false,
  service: "Gmail",
  auth: {
    user: process.env.MAIL_USERNAME,
    pass: process.env.MAIL_PASSWORD,
  },
});

export { transporter };

export const createCourse = async (req, res) => {
  try {
    const {
      name,
      description,
      whatyouwilllearn,
      prerequisites,
      price,
      category,
      instructor,
      published,
    } = req.body;

    const image = req.file.filename;

    if (!name) {
      return res.status(400).json({ message: "Name is required" });
    }

    if (!description) {
      return res.status(400).json({ message: "Description is required" });
    }

    if (!whatyouwilllearn) {
      return res
        .status(400)
        .json({ message: "what you will learn field is required" });
    }

    if (!prerequisites) {
      return res.status(400).json({ message: "Prerequisites are required" });
    }

    if (!price) {
      return res.status(400).json({ message: "Price is required" });
    }

    if (!category) {
      return res.status(400).json({ message: "Catogory is required" });
    }

    if (!published) {
      return res.status(400).json({ message: "Published status is required" });
    }

    // Split the whatyouwilllearn string by commas and trim the values
    const whatyouwilllearnArray = whatyouwilllearn
      .split(",")
      .map((item) => item.trim());

    // Split the whatyouwilllearn string by commas and trim the values
    const prerequisitesArray = prerequisites
      .split(",")
      .map((item) => item.trim());

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error : No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    const course = new Course({
      name,
      description,
      whatyouwilllearn: whatyouwilllearnArray,
      prerequisites: prerequisitesArray,
      price,
      category,
      instructor: userId,
      published,
      image: image,
    });

    const newCourse = await course.save();
    res.status(201).json(newCourse);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const enrollCourse = async (req, res) => {
  try {
    const { id } = req.params;
    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error : No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    // const course = await Course.findById(id);
    const course = await Course.findById(id).populate("instructor").exec();

    const user = await User.findById(userId).exec();
    const enrolledCourses = user.enrolledCourses || [];

    // Check if the user is already enrolled in the course
    if (enrolledCourses.includes(id)) {
      return res.status(400).json({
        error: "Already enrolled in this course",
      });
    }

    const fee = (course.price * 0) / 100;

    const session = await stripe.checkout.sessions.create({
      payment_method_types: ["card"],
      mode: "payment",

      // purchase details
      line_items: [
        {
          price_data: {
            currency: "inr",
            product_data: {
              name: course.name,
            },
            unit_amount: Math.round(course.price.toFixed(2) * 100),
          },
          quantity: 1,
        },
      ],

      // charge buyer and transfer balance to seller
      // payment_intent_data: {
      //   application_fee_amount: Math.round(course.price.toFixed(2) * 100),
      //   transfer_data: {
      //     destination: course.instructor.stripe_account_id,
      //   },
      // },

      // redirect url after successful payment
      success_url: "http://localhost:3000/checkout-success",
      cancel_url: "http://localhost:3000/checkout-cancel",
    });
    console.log("SESSION ID = ", session);

    // Retrieve the session object from Stripe
    const retrievedSession = await stripe.checkout.sessions.retrieve(
      session.id
    );

    // Extract the payment URL from the session object
    const paymentUrl = retrievedSession.url;

    await User.findByIdAndUpdate(userId, { stripeSession: session }).exec();
    // res.send(session.id);

    res.send(paymentUrl);
  } catch (error) {
    console.log(error);
    return res.status(400).send("Enrollment failed for the course");
  }
};

export const stripeSuccess = async (req, res) => {
  try {
    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error : No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    // const course = await Course.findById(req.params.courseId).exec();
    const course = await Course.findById(req.params.courseId)
      .populate("instructor", "name")
      .exec();
    // const user = await User.findById(req.auth._id).exec();
    const user = await User.findById(userId).exec();

    if (!user.stripeSession.id) return res.sendStatus(400);

    const session = await stripe.checkout.sessions.retrieve(
      user.stripeSession.id
    );

    console.log("STRIPE SUCCESS");

    if (session.payment_status === "paid") {
      await User.findByIdAndUpdate(user._id, {
        $addToSet: { enrolledCourses: course._id },
        $set: { stripeSession: {} },
      }).exec();
    }

    res.json({ success: true, course });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const enrollCourseWithoutFees = async (req, res) => {
  try {
    const { id } = req.params;
    const authHeader = req.headers.authorization;

    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    // Find the course
    const course = await Course.findById(id).populate("instructor").exec();

    // Check if the course exists
    if (!course) {
      return res.status(404).json({
        error: "Course not found",
      });
    }

    const user = await User.findById(userId).exec();
    const enrolledCourses = user.enrolledCourses || [];

    // Check if the user is already enrolled in the course
    if (enrolledCourses.includes(id)) {
      return res.status(400).json({
        error: "Already enrolled in this course",
      });
    }

    user.enrolledCourses.push(id);
    await user.save();

    course.enrolledUsers.push(userId);
    await course.save();

    res.json({ success: true, course });
  } catch (error) {
    console.error(error);
    return res.status(400).send("Enrollment failed");
  }
};

export const listEnrolledCourses = async (req, res) => {
  try {
    const id = req.params.id;

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error : No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    const user = await User.findById(userId).populate("enrolledCourses");
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    res.status(200).json(user.enrolledCourses);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const addLesson = async (req, res) => {
  try {
    const { courseId } = req.params;
    const { title, content } = req.body;

    const videoFile = req.files["video"][0];
    // const pdfFile = req.files["pdf"][0];
    const pdfFile = req.files["pdf"] ? req.files["pdf"][0] : null;

    // const assignmentPdf = req.file ? req.file.filename : null; // Check if a file is provided

    // Check file types
    if (videoFile.mimetype !== "video/mp4") {
      return res
        .status(400)
        .json({ message: "Only MP4 video files are allowed in video field" });
    }

    if (pdfFile && pdfFile.mimetype !== "application/pdf") {
      return res
        .status(400)
        .json({ message: "Only PDF files are allowed in pdf field" });
    }

    const video = videoFile.filename;
    // const pdf = pdfFile.filename;
    const pdf = pdfFile ? pdfFile.filename : null;

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error : No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Check if the authenticated user is the instructor of the course
    if (course.instructor.toString() !== userId) {
      return res
        .status(403)
        .json({
          message: "Only the instructor can add lessons to this course",
        });
    }

    // Create a new lesson
    const lesson = new Lesson({
      title,
      content,
      video,
      pdf,
      course: courseId,
    });

    // Add the new lesson to the course
    course.lessons.push(lesson);
    await course.save();

    // Save the lesson
    await lesson.save();

    res.status(201).json(course);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const getAllLessons = async (req, res) => {
  const { courseId } = req.params;
  const { userId } = req.params; // Assuming you have the userId in the request

  try {
    // Find the course by its ID
    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Find the user's progress for the specified course
    const progress = await Progress.findOne({ user: userId, course: courseId });

    // Create a map to store lesson completion status
    const lessonCompletionStatus = {};

    // Initialize the lessonCompletionStatus map with lessons set as not completed
    // course.lessons.forEach((lesson) => {
    //   lessonCompletionStatus[lesson._id.toString()] = false;
    // });

    // If progress exists, update the completion status for completed lessons
    if (progress) {
      progress.lessonProgress.forEach((lp) => {
        lessonCompletionStatus[lp.lesson.toString()] = lp.completed;
      });
    }

    // Combine lessons and completion status
    const lessonsWithCompletionStatus = course.lessons.map((lesson) => ({
      ...lesson.toObject(),
      completed: lessonCompletionStatus[lesson._id.toString()],
    }));

    res.status(200).json(lessonsWithCompletionStatus);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const getAllLessonVideos = async (req, res) => {
  const { courseId } = req.params;

  try {
    // Find the course by its ID
    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    const lessonVideos = course.lessons.map((lesson) => lesson.video);

    for (let i = 0; i < lessonVideos.length; i++) {
      res.sendFile(
        path.join(__dirname, "../public/userImages", lessonVideos[i])
      );
    }
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const getAllLessonPdfs = async (req, res) => {
  const { courseId } = req.params;

  try {
    // Find the course by its ID
    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    const lessonPdfs = course.lessons.map((lesson) => lesson.pdf);

    for (let i = 0; i < lessonPdfs.length; i++) {
      res.sendFile(path.join(__dirname, "../public/userImages", lessonPdfs[i]));
    }
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const markAsCompleted = async (req, res) => {
  try {
    const { courseId, lessonId, userId } = req.params;

    // Find the progress object for the specified user and course
    let progress = await Progress.findOne({ user: userId, course: courseId });

    if (!progress) {
      progress = new Progress({
        user: userId,
        course: courseId,
        lessonProgress: [],
      });
    }

    // Find the lesson progress object for the specified lesson
    let lessonProgress = progress.lessonProgress.find(
      (lp) => lp.lesson.toString() === lessonId
    );

    if (!lessonProgress) {
      // If the user has not started this lesson yet, add a new lesson progress object
      lessonProgress = { lesson: lessonId, completed: true };
      progress.lessonProgress.push(lessonProgress);
    } else {
      // If the user has started this lesson, mark it as completed
      lessonProgress.completed = true;
    }

    // Save the updated progress object to the database
    await progress.save();

    // Update the lesson completion status
    const lesson = await Lesson.findById(lessonId);
    if (lesson) {
      lesson.completed = true;
      await lesson.save();
    }

    // Update the course completion status
    const course = await Course.findById(courseId);
    if (course) {
      const lessonsCompleted = progress.lessonProgress.filter(
        (lp) => lp.completed
      ).length;

      course.completed = lessonsCompleted === course.lessons.length;
      await course.save();
    }

    res.status(200).send({ message: "Lesson marked as completed" });
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: "Internal server error" });
  }
};

export const countTotalCourses = async (req, res) => {
  try {
    const totalCourses = await Course.countDocuments({});
    res.status(200).json({ totalCourses });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Server error" });
  }
};

export const countTotalLessonInCourse = async (req, res) => {
  try {
    const course = await Course.findById(req.params.courseId);
    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }
    const totalLessons = course.lessons ? course.lessons.length : 0;
    res.status(200).json({ totalLessons });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Server error" });
  }
};

export const courseDetails = async (req, res) => {
  try {
    const course = await Course.findById(req.params.courseId)
      .populate("instructor")
      .populate("lessons");
    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }
    return res.json(course);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Server error" });
  }
};

export const adminCourseDetails = async (req, res) => {
  try {
    const course = await Course.findById(req.params.courseId)
      .populate("instructor")
      .populate("lessons");
    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }
    return res.json(course);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Server error" });
  }
};

export const allCoursesListSort = async (req, res) => {
  try {
    let sort = {};

    if (req.query.sort === "asc") {
      sort = { name: 1 }; // sort by name in ascending order
    } else if (req.query.sort === "desc") {
      sort = { name: -1 }; // sort by name in descending order
    }

    const courses = await Course.find()
      .populate("instructor", "name email")
      .select("-__v")
      .sort(sort);

    res.status(200).json(courses);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const oneVideoOfCourse = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;

    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }

    const lesson = course.lessons.id(lessonId);

    if (!lesson) {
      return res.status(404).json({ error: "Lesson not found" });
    }

    if (!lesson.video) {
      return res.status(404).json({ error: "Video not found" });
    }

    // return res.json({ videoUrl: lesson.video });
    return res.sendFile(
      path.join(__dirname, "../public/userImages", lesson.video)
    );
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Server error" });
  }
};

export const onePdfOfCourse = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;

    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }

    const lesson = course.lessons.id(lessonId);

    if (!lesson) {
      return res.status(404).json({ error: "Lesson not found" });
    }

    if (!lesson.video) {
      return res.status(404).json({ error: "Video not found" });
    }

    // return res.json({ videoUrl: lesson.video });
    return res.sendFile(
      path.join(__dirname, "../public/userImages", lesson.pdf)
    );
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Server error" });
  }
};

export const adminOneVideoOfCourse = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;

    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }

    const lesson = course.lessons.id(lessonId);

    if (!lesson) {
      return res.status(404).json({ error: "Lesson not found" });
    }

    if (!lesson.video) {
      return res.status(404).json({ error: "Video not found" });
    }

    // return res.json({ videoUrl: lesson.video });
    return res.sendFile(
      path.join(__dirname, "../public/userImages", lesson.video)
    );
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Server error" });
  }
};

export const adminOnePdfOfCourse = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;

    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }

    const lesson = course.lessons.id(lessonId);

    if (!lesson) {
      return res.status(404).json({ error: "Lesson not found" });
    }

    if (!lesson.video) {
      return res.status(404).json({ error: "Video not found" });
    }

    // return res.json({ videoUrl: lesson.video });
    return res.sendFile(
      path.join(__dirname, "../public/userImages", lesson.pdf)
    );
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: "Server error" });
  }
};

export const instructorAllCourses = async (req, res) => {
  try {
    const courses = await Course.find({ instructor: req.params.instructorId });
    res.status(200).json({ courses });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Server error" });
  }
};

export const getCourseImage = async (req, res) => {
  try {
    const course = await Course.findById(req.params.courseId);
    if (!course) {
      return res.status(404).send("Course not found");
    }

    res.sendFile(path.join(__dirname, "../public/userImages", course.image));
  } catch (err) {
    console.log(err);
    return res.status(400).send("Error in fetching user photo");
  }
};

export const updateLesson = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;
    const { title, content } = req.body;

    const videoFile = req.files && req.files["video"] && req.files["video"][0];
    const pdfFile = req.files && req.files["pdf"] && req.files["pdf"][0];

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Check if the lesson exists in the course
    const lesson = course.lessons.find(
      (lesson) => lesson._id.toString() === lessonId
    );

    if (!lesson) {
      return res.status(404).json({ message: "Lesson not found" });
    }

    // Check if the authenticated user is the instructor of the course
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    if (course.instructor.toString() !== userId) {
      return res
        .status(403)
        .json({
          message: "Only the instructor can update lessons in this course",
        });
    }

    // Update the lesson details
    lesson.title = title;
    lesson.content = content;

    // Update the video file if provided
    if (videoFile) {
      // Check file type
      if (videoFile.mimetype !== "video/mp4") {
        return res
          .status(400)
          .json({ message: "Only MP4 video files are allowed in video field" });
      }

      const video = videoFile.filename;
      lesson.video = video;
    }

    // Update the PDF file if provided
    if (pdfFile) {
      // Check file type
      if (pdfFile.mimetype !== "application/pdf") {
        return res
          .status(400)
          .json({ message: "Only PDF files are allowed in pdf field" });
      }

      const pdf = pdfFile.filename;
      lesson.pdf = pdf;
    }

    // Save the updated lesson in the lesson schema
    await lesson.save();

    // Save the updated course
    await course.save();

    res.status(200).json({ message: "Lesson updated successfully", lesson });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const deleteLesson = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Check if the lesson exists in the course
    const lessonIndex = course.lessons.findIndex(
      (lesson) => lesson._id.toString() === lessonId
    );

    if (lessonIndex === -1) {
      return res.status(404).json({ message: "Lesson not found" });
    }

    // Check if the authenticated user is the instructor of the course
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    if (course.instructor.toString() !== userId) {
      return res
        .status(403)
        .json({
          message: "Only the instructor can delete lessons from this course",
        });
    }

    // Remove the lesson from the course
    course.lessons.splice(lessonIndex, 1);

    // Save the updated course
    await course.save();

    // Delete the lesson from the lesson table
    await Lesson.findByIdAndDelete(lessonId);

    res.status(200).json({ message: "Lesson deleted successfully" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const deleteCourse = async (req, res) => {
  try {
    const courseId = req.params.courseId;

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Check if the authenticated user is the instructor of the course
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    if (course.instructor.toString() !== userId) {
      return res
        .status(403)
        .json({ message: "Only the instructor can delete this course" });
    }

    // Delete the lessons associated with the course from the lessons schema
    const lessonIds = course.lessons.map((lesson) => lesson._id);

    await Lesson.deleteMany({ _id: { $in: lessonIds } });

    // Delete the course
    await Course.findByIdAndDelete(courseId);

    res
      .status(200)
      .json({ message: "Course and its lessons deleted successfully" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const updateCourse = async (req, res) => {
  try {
    const courseId = req.params.courseId;
    const {
      name,
      description,
      whatyouwilllearn,
      prerequisites,
      price,
      image,
      category,
      published,
    } = req.body;

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Check if the authenticated user is the instructor of the course
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    if (course.instructor.toString() !== userId) {
      return res
        .status(403)
        .json({ message: "Only the instructor can update this course" });
    }

    // Update the course detail
    // course.name = name || course.name;
    course.name = name || course.name;
    course.description = description || course.description;
    course.whatyouwilllearn = whatyouwilllearn || course.whatyouwilllearn;
    course.prerequisites = prerequisites || course.prerequisites;
    course.price = price || course.price;
    course.image = image || course.image;
    course.category = category || course.category;
    course.published = published !== undefined ? published : course.published;

    await course.save();

    res
      .status(200)
      .json({ message: "Course details updated successfully", course });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const discussionPost = async (req, res) => {
  try {
    const courseId = req.params.courseId;
    const { userId, content } = req.body;

    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }

    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }

    const discussionPost = new DiscussionPost({
      user: userId,
      username: user.name,
      course: courseId,
      content,
    });

    const savedPost = await discussionPost.save();

    // Update the course with the forum post
    course.forum = savedPost._id;
    await course.save();

    res.json(savedPost);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
};

export const discussionPostReply = async (req, res) => {
  try {
    const courseId = req.params.courseId;
    const postId = req.params.postId;
    const { userId, content } = req.body;

    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }

    const discussionPost = await DiscussionPost.findById(postId);
    if (!discussionPost) {
      return res.status(404).json({ error: "Discussion post not found" });
    }

    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ error: "User not found" });
    }

    const reply = {
      user: userId,
      username: user.name,
      content,
    };

    discussionPost.replies.push(reply);
    await discussionPost.save();

    res.json(discussionPost);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
};

export const allDiscussionPosts = async (req, res) => {
  try {
    const courseId = req.params.courseId;

    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }

    const discussionPosts = await DiscussionPost.find({ course: courseId })
      .populate("user", "name") // Populates the 'user' field and includes only the 'name' property
      .populate("replies.user", "name"); // Populates the 'replies.user' field and includes only the 'name' property

    res.json(discussionPosts);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
};

export const postAnnouncement = async (req, res) => {
  try {
    const courseId = req.params.courseId;
    const { title, text } = req.body;

    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }

    // Check if the user making the request is the instructor of the course
    // onst userId = req.user.id;

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error : No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    if (course.instructor.toString() !== userId) {
      return res
        .status(403)
        .json({ error: "You are not authorized to perform this action" });
    }

    const announcement = {
      title,
      text,
    };

    course.announcements.push(announcement);
    await course.save();

    res.json(course);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
};

export const allAnnouncements = async (req, res) => {
  try {
    const courseId = req.params.courseId;

    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }

    const announcements = course.announcements;

    res.json(announcements);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
};

export const editAnnouncement = async (req, res) => {
  try {
    const courseId = req.params.courseId;
    const announcementId = req.params.announcementId;
    const { title, text } = req.body;

    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }

    // Check if the user making the request is the instructor of the course
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    if (course.instructor.toString() !== userId) {
      return res
        .status(403)
        .json({ error: "You are not authorized to perform this action" });
    }

    const announcement = course.announcements.id(announcementId);
    if (!announcement) {
      return res.status(404).json({ error: "Announcement not found" });
    }

    announcement.title = title;
    announcement.text = text;

    await course.save();

    res.json(course);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
};

export const deleteAnnouncement = async (req, res) => {
  try {
    const courseId = req.params.courseId;
    const announcementId = req.params.announcementId;

    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ error: "Course not found" });
    }

    // Check if the user making the request is the instructor of the course
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    if (course.instructor.toString() !== userId) {
      return res
        .status(403)
        .json({ error: "You are not authorized to perform this action" });
    }

    const announcement = course.announcements.id(announcementId);
    if (!announcement) {
      return res.status(404).json({ error: "Announcement not found" });
    }

    announcement.remove();
    await course.save();

    res.json(course);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
};

export const generateCertificate = async (req, res) => {
  const courseId = req.params.courseId;

  try {
    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    const course = await Course.findById(courseId);

    // Find the user's progress for this course
    const progress = await Progress.findOne({ user: userId, course: courseId });

    if (!progress) {
      return res
        .status(400)
        .json({ error: "User has not started this course yet" });
    }

    const completedLessonsCount = progress.lessonProgress.filter(
      (lp) => lp.completed
    ).length;

    if (completedLessonsCount !== course.lessons.length) {
      return res
        .status(400)
        .json({ error: "All course lessons not completed" });
    }

    const user = await User.findById(userId);

    // Create a new PDF document
    const pdf = new PDFDocument();
    const filePath = path.join(__dirname, "certificate.pdf"); // Define the path where the PDF will be saved

    // Pipe the PDF content to a file
    const stream = fs.createWriteStream(filePath);
    pdf.pipe(stream);

    // Define data for the certificate
    const data = {
      name: user.name,
      courseName: course.name,
      completionDate: new Date().toLocaleDateString(),
    };

    // Create the PDF content
    pdf.fontSize(20).text("Certificate of Completion", { align: "center" });
    pdf.moveDown(0.5);
    pdf.fontSize(16).text(`This is to certify that`, { align: "left" });
    pdf.moveDown(0.5);
    pdf.fontSize(18).text(`${data.name}`, { align: "center" });
    pdf.moveDown(0.5);
    pdf
      .fontSize(16)
      .text(`has successfully completed the course`, { align: "center" });
    pdf.moveDown(0.5);
    pdf.fontSize(18).text(`${data.courseName}`, { align: "center" });
    pdf.moveDown(0.5);
    pdf.fontSize(16).text(`on ${data.completionDate}.`, { align: "center" });

    // End the PDF document
    pdf.end();

    // Set response headers for the PDF download
    res.setHeader(
      "Content-Disposition",
      'attachment; filename="certificate.pdf"'
    );
    res.setHeader("Content-Type", "application/pdf");

    // Pipe the file to the response for download
    const fileStream = fs.createReadStream(filePath);
    fileStream.pipe(res);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const ummarkAsCompleted = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    // Find the progress object for the specified user and course
    const progress = await Progress.findOne({ user: userId, course: courseId });

    if (!progress) {
      return res
        .status(400)
        .json({ error: "User has not started this course yet" });
    }

    // Find the lesson progress object for the specified lesson
    const lessonProgress = progress.lessonProgress.find(
      (lp) => lp.lesson.toString() === lessonId
    );

    if (!lessonProgress) {
      return res
        .status(400)
        .json({ error: "Lesson is not marked as completed" });
    }

    // Unmark the lesson as completed
    lessonProgress.completed = false;

    // Calculate the percentage of lessons completed
    const course = await Course.findById(courseId);
    const totalLessons = course.lessons.length;
    const completedLessons = progress.lessonProgress.filter(
      (lp) => lp.completed
    ).length;
    const percentageComplete = Math.round(
      (completedLessons / totalLessons) * 100
    );

    // Update the progress object with the percentage complete
    progress.completed = completedLessons === totalLessons;
    progress.percentageComplete = percentageComplete;

    // Save the updated progress object to the database
    await progress.save();

    res
      .status(200)
      .send({ message: "Lesson unmarked as completed", percentageComplete });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
};

export const toggleLessonCompletion = async (req, res) => {
  try {
    const { courseId, lessonId, userId } = req.params;

    // Find the progress object for the specified user and course
    let progress = await Progress.findOne({ user: userId, course: courseId });

    if (!progress) {
      progress = new Progress({
        user: userId,
        course: courseId,
        lessonProgress: [],
      });
    }

    // Find the lesson progress object for the specified lesson
    let lessonProgress = progress.lessonProgress.find(
      (lp) => lp.lesson.toString() === lessonId
    );

    if (!lessonProgress) {
      // If the lesson progress object doesn't exist, create a new one
      lessonProgress = { lesson: lessonId, completed: true };
      progress.lessonProgress.push(lessonProgress);
    }

    // Toggle the `completed` property
    lessonProgress.completed = !lessonProgress.completed;

    // Calculate the percentage of lessons completed
    const course = await Course.findById(courseId);
    const totalLessons = course.lessons.length;
    const completedLessons = progress.lessonProgress.filter(
      (lp) => lp.completed
    ).length;
    const percentageComplete = Math.round(
      (completedLessons / totalLessons) * 100
    );

    const completedLessonsArray = progress.lessonProgress;

    // Update the progress object with the percentage complete
    progress.completed = completedLessons === totalLessons;
    progress.percentageComplete = percentageComplete;

    // Save the updated progress object to the database
    await progress.save();

    if (lessonProgress.completed) {
      res
        .status(200)
        .send({ message: "Lesson marked as completed", percentageComplete , completedLessonsArray});
    } else {
      res
        .status(200)
        .send({ message: "Lesson unmarked as completed", percentageComplete , completedLessonsArray});
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
};

export const getLessonCompletion = async (req, res) => {
  try {
    const { courseId, userId } = req.params;

    // Find the progress object for the specified user and course
    let progress = await Progress.findOne({ user: userId, course: courseId });

    const allLessons = progress.lessonProgress;

    const course = await Course.findById(courseId);
    const finalAssignment = course.finalAssignment;
    const totalLessons = course.lessons.length;
    const completedLessons = progress.lessonProgress.filter(
      (lp) => lp.completed
    ).length;
    const percentageComplete = Math.round(
      (completedLessons / totalLessons) * 100
    );

    res.status(200).send({allLessons,percentageComplete,finalAssignment});
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
};


export const getFinalAssignment = async (req, res) => {
  try {
    const { courseId } = req.params;

    const course = await Course.findById(courseId);
    const finalAssignment = course.finalAssignment;


    res.status(200).send(finalAssignment);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
};

export const getallquiz = async (req, res) => {
  try {
    const { lessonId } = req.params;

    const quizs = await Quiz.find({ lesson: lessonId });
    res.json(quizs);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
};

export const deleteQuiz = async (req, res) => {
  try {
    const quizId = req.params.quizId;

    // Delete the course
    await Quiz.findByIdAndDelete(quizId);

    res
      .status(200)
      .json({ message: "quiz deleted successfully" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const addquiz = async (req, res) => {
  try {
    const { question, option1, option2, option3, option4, answer, lesson , course } = req.body;

    const quiz = new Quiz({
      question,
      option1,
      option2,
      option3,
      option4,
      answer,
      lesson
    });

    const savedQuiz = await quiz.save();

    const savedLesson = await Lesson.findById(lesson);
    console.log(course);
    const courseId = await Course.findById(course);
  
    // Find the lesson by its _id
    const lessonChange = await courseId.lessons.find(lesson1 => lesson1._id.toString() === lesson);
  
    // Update the quiz field of the lesson
    lessonChange.quiz = true;

    await courseId.save();
    // console.log(lessonChange)
    // console.log(courseId.lessons)


    savedLesson.quiz = true;

    await savedLesson.save();

    // to send the saved quiz in the response
    res.json(savedQuiz);
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Internal Server Error");
  }
};


export const getUserProgress = async (req, res) => {
  try {
    const { courseId } = req.params;
    // Fetch documents from the MongoDB collection and include only the '_id' field
    const ids = await User.find({}, '_id');
    const Names = await User.find({}, 'name');

    // Extract only the 'id' field from each document and return as an array
    const idArray = ids.map(doc => doc._id.toString()); // Convert ObjectId to string if needed


    const progressArray = []
    for (let i = 0; i < idArray.length; i++) {
      let progress = await Progress.findOne({ user: idArray[i], course: courseId });
      if (!progress) {
        continue;
      }
      const course = await Course.findById(courseId);
      const totalLessons = course.lessons.length;
      const completedLessons = progress.lessonProgress.filter(
        (lp) => lp.completed
      ).length;
      const percentageComplete = Math.round(
        (completedLessons / totalLessons) * 100
      );
      const doc = await User.findById(idArray[i]);
      const Username = doc.name
      progressArray.push({userID: idArray[i], name : Username, progress : percentageComplete});

    }



    // Return the array of ids as JSON response
    res.send(progressArray);
  } catch (error) {
    console.error('Error fetching ids from MongoDB:', error);
    res.status(500).json({ error: 'Internal server error' }); // Return 500 status in case of error
  }
};

export const generateDummyCertificate = async (req, res) => {
  try {
    // Create a new PDF document
    const pdf = new PDFDocument();

    // Define data for the certificate
    const data = {
      name: "Himanshu",
      courseName: "XYZ",
      completionDate: new Date().toLocaleDateString(),
    };

    // Pipe the PDF content to a file
    const filePath = path.join(__dirname, "certificate.pdf"); // Define the path where the PDF will be saved
    const stream = fs.createWriteStream(filePath);
    pdf.pipe(stream);

    // Create the PDF content
    pdf.fontSize(20).text("Certificate of Completion", { align: "center" });
    pdf.moveDown(0.5);
    pdf.fontSize(16).text(`This is to certify that`, { align: "left" });
    pdf.moveDown(0.5);
    pdf.fontSize(18).text(`${data.name}`, { align: "center" });
    pdf.moveDown(0.5);
    pdf
      .fontSize(16)
      .text(`has successfully completed the course`, { align: "center" });
    pdf.moveDown(0.5);
    pdf.fontSize(18).text(`${data.courseName}`, { align: "center" });
    pdf.moveDown(0.5);
    pdf.fontSize(16).text(`on ${data.completionDate}.`, { align: "center" });

    // End the PDF document
    pdf.end();

    // Set response headers for the PDF to open in a new tab
    res.setHeader("Content-Disposition", 'inline; filename="certificate.pdf"');
    res.setHeader("Content-Type", "application/pdf");

    // Send the PDF file
    // const fileStream = fs.createReadStream(filePath);
    // fileStream.pipe(res);

    res.sendFile(filePath, (err) => {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Server error" });
      }
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const instructorsList = async (req, res) => {
  try {
    const instructors = await User.find({ role: "Instructor" }).exec();
    const instructorData = [];

    for (const instructor of instructors) {
      const courseCount = await Course.countDocuments({
        instructor: instructor._id,
      });
      instructorData.push({
        instructorName: instructor.name,
        instructorEmail: instructor.email,
        courseCount,
      });
    }

    res.status(200).json(instructorData);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Server error" });
  }
};

export const instructorCourseCount = async (req, res) => {
  try {
    const instructorId = req.params.instructorId;

    const courseCount = await Course.countDocuments({
      instructor: instructorId,
    }).exec();

    res.status(200).json({ courseCount });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Server error" });
  }
};

export const updateLessonAdmin = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;
    const { title, content } = req.body;

    const videoFile = req.files && req.files["video"] && req.files["video"][0];
    const pdfFile = req.files && req.files["pdf"] && req.files["pdf"][0];

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Check if the lesson exists in the course
    const lesson = course.lessons.find(
      (lesson) => lesson._id.toString() === lessonId
    );

    if (!lesson) {
      return res.status(404).json({ message: "Lesson not found" });
    }

    // // Check if the authenticated user is the instructor of the course
    // const authHeader = req.headers.authorization;
    // if (!authHeader) {
    //   return res.status(401).json({ error: 'Error: No authorization header found' });
    // }

    // const token = authHeader.split(' ')[1];
    // const decoded = jwt.verify(token, process.env.JWT_SECRET);
    // const userId = decoded._id;

    // if (course.instructor.toString() !== userId) {
    //   return res.status(403).json({ message: "Only the instructor can update lessons in this course" });
    // }

    // Update the lesson details
    lesson.title = title;
    lesson.content = content;

    // Update the video file if provided
    if (videoFile) {
      // Check file type
      if (videoFile.mimetype !== "video/mp4") {
        return res
          .status(400)
          .json({ message: "Only MP4 video files are allowed in video field" });
      }

      const video = videoFile.filename;
      lesson.video = video;
    }

    // Update the PDF file if provided
    if (pdfFile) {
      // Check file type
      if (pdfFile.mimetype !== "application/pdf") {
        return res
          .status(400)
          .json({ message: "Only PDF files are allowed in pdf field" });
      }

      const pdf = pdfFile.filename;
      lesson.pdf = pdf;
    }

    // Save the updated lesson in the lesson schema
    await lesson.save();

    // Save the updated course
    await course.save();

    res.status(200).json({ message: "Lesson updated successfully", lesson });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const deleteLessonAdmin = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Check if the lesson exists in the course
    const lessonIndex = course.lessons.findIndex(
      (lesson) => lesson._id.toString() === lessonId
    );

    if (lessonIndex === -1) {
      return res.status(404).json({ message: "Lesson not found" });
    }

    // // Check if the authenticated user is the instructor of the course
    // const authHeader = req.headers.authorization;
    // if (!authHeader) {
    //   return res.status(401).json({ error: 'Error: No authorization header found' });
    // }

    // const token = authHeader.split(' ')[1];
    // const decoded = jwt.verify(token, process.env.JWT_SECRET);
    // const userId = decoded._id;

    // if (course.instructor.toString() !== userId) {
    //   return res.status(403).json({ message: "Only the instructor can delete lessons from this course" });
    // }

    // Remove the lesson from the course
    course.lessons.splice(lessonIndex, 1);

    // Save the updated course
    await course.save();

    // Delete the lesson from the lesson table
    await Lesson.findByIdAndDelete(lessonId);

    res.status(200).json({ message: "Lesson deleted successfully" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const deleteCourseAdmin = async (req, res) => {
  try {
    const courseId = req.params.courseId;

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // // Check if the authenticated user is the instructor of the course
    // const authHeader = req.headers.authorization;
    // if (!authHeader) {
    //   return res.status(401).json({ error: 'Error: No authorization header found' });
    // }

    // const token = authHeader.split(' ')[1];
    // const decoded = jwt.verify(token, process.env.JWT_SECRET);
    // const userId = decoded._id;

    // if (course.instructor.toString() !== userId) {
    //   return res.status(403).json({ message: "Only the instructor can delete this course" });
    // }

    // Delete the lessons associated with the course from the lessons schema
    const lessonIds = course.lessons.map((lesson) => lesson._id);

    await Lesson.deleteMany({ _id: { $in: lessonIds } });

    // Delete the course
    await Course.findByIdAndDelete(courseId);

    res
      .status(200)
      .json({ message: "Course and its lessons deleted successfully" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const updateCourseAdmin = async (req, res) => {
  try {
    const courseId = req.params.courseId;
    const {
      name,
      description,
      whatyouwilllearn,
      prerequisites,
      price,
      image,
      category,
      published,
    } = req.body;

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Check if the authenticated user is the instructor of the course
    // const authHeader = req.headers.authorization;
    // if (!authHeader) {
    //   return res.status(401).json({ error: 'Error: No authorization header found' });
    // }

    // const token = authHeader.split(' ')[1];
    // const decoded = jwt.verify(token, process.env.JWT_SECRET);
    // const userId = decoded._id;

    // if (course.instructor.toString() !== userId) {
    //   return res.status(403).json({ message: "Only the instructor can update this course" });
    // }

    // Update the course detail
    // course.name = name || course.name;
    course.name = name || course.name;
    course.description = description || course.description;
    course.whatyouwilllearn = whatyouwilllearn || course.whatyouwilllearn;
    course.prerequisites = prerequisites || course.prerequisites;
    course.price = price || course.price;
    course.image = image || course.image;
    course.category = category || course.category;
    course.published = published !== undefined ? published : course.published;

    await course.save();

    res
      .status(200)
      .json({ message: "Course details updated successfully", course });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const instructorEnrolledCourses = async (req, res) => {
  try {
    const instructorId = req.params.instructorId;

    const instructor = await User.findById(instructorId);

    if (!instructor) {
      return res.status(404).json({ message: "Instructor not found" });
    }

    if (!instructor.role.includes("Instructor")) {
      return res
        .status(400)
        .json({ error: "The provided ID does not belong to an instructor" });
    }

    const enrolledCourses = await User.findById(instructorId).populate(
      "enrolledCourses"
    );

    res.status(200).json(enrolledCourses.enrolledCourses);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Server error" });
  }
};

export const topCourses = async (req, res) => {
  try {
    const topCourses = await Course.aggregate([
      {
        $project: {
          name: 1,
          enrolledUsersCount: {
            $size: { $ifNull: ["$enrolledUsers", []] },
          },
        },
      },
      { $sort: { enrolledUsersCount: -1 } },
      { $limit: 5 },
    ]);

    res.json({ topCourses });
  } catch (error) {
    console.error(error);
    return res.status(500).send("Error while fetching top courses");
  }
};

export const topUsers = async (req, res) => {
  try {
    const topUsers = await User.aggregate([
      {
        $project: {
          name: 1,
          enrolledCoursesCount: { $size: "$enrolledCourses" },
        },
      },
      { $sort: { enrolledCoursesCount: -1 } },
      { $limit: 5 },
    ]);

    res.json(topUsers);
  } catch (err) {
    res
      .status(500)
      .json({ error: "An error occurred while fetching top users" });
  }
};

export const approveCourse = async (req, res) => {
  try {
    const { id } = req.params;

    await Course.findOneAndUpdate({ _id: id }, { isApproved: true });

    // const mailOptions = {
    //   from: "himanshumuj@gmail.com",
    //   to: `${email}`,
    //   subject: "Account Verification - HPCTraining Portal",
    //   text: `Dear User,\n\nYour Course is approved and is active now.\n\nBest regards,\nHPCTraining Team`,
    // };

    // transporter.sendMail(mailOptions, function (error, info) {
    //   if (error) {
    //     console.log(error);
    //   } else {
    //     console.log("Email sent: " + info.response);
    //   }
    // });

    res.send("Course is approved");
  } catch (error) {
    res.status(400).send(error.message);
  }
};

export const getAllUnapprovedCourse = async (req, res) => {
  try {
    // Find all courses where isApproved is false
    const unapprovedCourses = await Course.find({ isApproved: false });

    res.status(200).json(unapprovedCourses);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export const unapproveCourse = async (req, res) => {
  try {
    const { id } = req.params;

    // Find the course by its ID and set isApproved to false
    await Course.findByIdAndUpdate(id, { isApproved: false });

    res.status(200).json({ message: "Course is unapproved" });
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export const coursesMonthWise = async (req, res) => {
  try {
    const courseCounts = await Course.aggregate([
      {
        $group: {
          _id: {
            year: { $year: "$createdAt" },
            month: { $month: "$createdAt" },
          },
          count: { $sum: 1 },
        },
      },
      {
        $sort: { "_id.year": 1, "_id.month": 1 },
      },
    ]);

    const courseCountsFormatted = courseCounts.map((entry) => ({
      month: moment({
        year: entry._id.year,
        month: entry._id.month - 1,
      }).format("MMMM YYYY"),
      count: entry.count,
    }));

    res.json(courseCountsFormatted);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
};

export const adminGetAllLessonVideos = async (req, res) => {
  const { courseId } = req.params;

  try {
    // Find the course by its ID
    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    const lessonVideos = course.lessons.map((lesson) => lesson.video);

    for (let i = 0; i < lessonVideos.length; i++) {
      res.sendFile(
        path.join(__dirname, "../public/userImages", lessonVideos[i])
      );
    }
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const adminAddSuggestionToLesson = async (req, res) => {
  try {
    const { text } = req.body;
    const lessonId = req.params.lessonId;
    const courseId = req.params.courseId;

    const lesson = await Lesson.findById(lessonId);

    if (!lesson) {
      return res.status(404).json({ message: "Lesson not found" });
    }

    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    const suggestion = {
      text,
    };

    lesson.suggestions.push(suggestion);

    await lesson.save();

    res.status(201).json({ message: "Suggestion added successfully" });
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export const getSuggestionOfLesson = async (req, res) => {
  try {
    const lessonId = req.params.lessonId;

    // Find the lesson by its ID and populate the suggestions
    const lesson = await Lesson.findById(lessonId).populate("suggestions");

    if (!lesson) {
      return res.status(404).json({ message: "Lesson not found" });
    }

    res.status(200).json(lesson.suggestions);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export const getAllSuggestionOfCourse = async (req, res) => {
  try {
    const courseId = req.params.courseId;

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Create an array to store all suggestions for the course
    const courseSuggestions = [];

    for (const lessonId of course.lessons) {
      // Find the lesson by its ID
      const lesson = await Lesson.findById(lessonId);

      if (lesson) {
        // Add the suggestions of the lesson to the courseSuggestions array
        const lessonSuggestions = lesson.suggestions.map((suggestion) => ({
          lessonId: lesson._id,
          suggestion: suggestion,
        }));

        courseSuggestions.push(...lessonSuggestions);
      }
    }

    res.status(200).json(courseSuggestions);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export const adminGetAllSuggestionOfCourse = async (req, res) => {
  try {
    const courseId = req.params.courseId;

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Create an array to store all suggestions for the course
    const courseSuggestions = [];

    for (const lessonId of course.lessons) {
      // Find the lesson by its ID
      const lesson = await Lesson.findById(lessonId);

      if (lesson) {
        // Add the suggestions of the lesson to the courseSuggestions array
        const lessonSuggestions = lesson.suggestions.map((suggestion) => ({
          lessonId: lesson._id,
          suggestion: suggestion,
        }));

        courseSuggestions.push(...lessonSuggestions);
      }
    }

    res.status(200).json(courseSuggestions);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export const deleteSuggestionOfLesson = async (req, res) => {
  try {
    const lessonId = req.params.lessonId;
    const suggestionId = req.params.suggestionId;

    const lesson = await Lesson.findById(lessonId);

    if (!lesson) {
      return res.status(404).json({ message: "Lesson not found" });
    }

    const suggestionIndex = lesson.suggestions.findIndex(
      (suggestion) => suggestion._id == suggestionId
    );

    if (suggestionIndex === -1) {
      return res.status(404).json({ message: "Suggestion not found" });
    }

    lesson.suggestions.splice(suggestionIndex, 1);

    await lesson.save();

    res.status(200).json({ message: "Suggestion deleted successfully" });
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export const instructorRequestCoursePublish = async (req, res) => {
  try {
    const { courseId } = req.params;

    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    course.adminApproval = false;

    course.requested = true;

    course.requestDate = Date.now();

    await course.save();

    res.status(201).json(course);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const instructorRequestCourseUnPublish = async (req, res) => {
  try {
    const { courseId } = req.params;

    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    course.adminApproval = false;

    await course.save();

    res.status(201).json(course);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const adminApproveCoursePublishRequest = async (req, res) => {
  try {
    const { courseId } = req.params;

    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    course.adminApproval = true;
    course.requested = false;
    await course.save();

    res.status(200).json({ message: "Course approval status set to true" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const adminUnapproveCoursePublishRequest = async (req, res) => {
  try {
    const { courseId } = req.params;

    const course = await Course.findById(courseId);
    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    course.adminApproval = false;
    course.requested = false;
    await course.save();

    res.status(200).json({ message: "Course approval status set to false" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const adminListCoursePublishRequest = async (req, res) => {
  try {
    const listCourses = await Course.find({ requested: true });

    res.status(200).json(listCourses);
  } catch (error) {
    res.status(500).json({ message: "Error in fetching unapproved courses" });
  }
};

export const allCoursesList = async (req, res) => {
  try {
    const courses = await Course.find()
      .populate("instructor", "name email")
      .select("-__v");
    res.status(200).json(courses);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const allPublishedCoursesList = async (req, res) => {
  try {
    const courses = await Course.find({ adminApproval: true })
      .populate("instructor", "name email")
      .select("-__v");
    res.status(200).json(courses);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const adminAddSuggestionToCourse = async (req, res) => {
  try {
    const { text } = req.body;
    const courseId = req.params.courseId;

    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    const suggestion = {
      text,
    };

    course.courseSuggestions.push(suggestion);

    await course.save();

    res
      .status(201)
      .json({ message: "Suggestion added to course successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Internal server error" });
  }
};

export const getSuggestionOnCourse = async (req, res) => {
  try {
    const courseId = req.params.courseId;

    const course = await Course.findById(courseId).populate(
      "courseSuggestions"
    );

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    res.status(200).json(course.courseSuggestions);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export const deleteSuggestionOnCourse = async (req, res) => {
  try {
    const suggestionId = req.params.suggestionId;

    const course = await Course.findOne({
      "courseSuggestions._id": suggestionId,
    });

    if (!course) {
      return res.status(404).json({ message: "Suggestion not found" });
    }

    const suggestionIndex = course.courseSuggestions.findIndex(
      (suggestion) => suggestion._id == suggestionId
    );

    if (suggestionIndex === -1) {
      return res.status(404).json({ message: "Suggestion not found" });
    }

    course.courseSuggestions.splice(suggestionIndex, 1);

    await course.save();

    res.status(200).json({ message: "Course Suggestion deleted successfully" });
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

function generateRandomString(length) {
  let result = "";
  const characters = "abcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

export const getAllSuggestionsCombined = async (req, res) => {
  try {
    const courseId = req.params.courseId;

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Create an array to store all suggestions for the course
    const courseSuggestions = [];

    for (const lessonId of course.lessons) {
      // Find the lesson by its ID
      const lesson = await Lesson.findById(lessonId);

      if (lesson) {
        // Add the suggestions of the lesson to the courseSuggestions array with lesson ID
        const lessonSuggestions = lesson.suggestions.map((suggestion) => ({
          lessonId: lesson._id,
          suggestion: suggestion,
        }));

        courseSuggestions.push(...lessonSuggestions);
      }
    }

    // Fetch suggestions on the entire course
    const courseSuggestionsOnCourse = course.courseSuggestions;

    // Generate a random 24-character lesson ID
    const randomLessonId = generateRandomString(24);

    // Add the random lesson ID to each suggestion in courseSuggestionsOnCourse
    const randomizedCourseSuggestions = courseSuggestionsOnCourse.map(
      (suggestion) => ({
        lessonId: randomLessonId,
        suggestion: suggestion,
      })
    );

    // Combine the suggestions from both sources
    const combinedSuggestions = [
      ...courseSuggestions,
      ...randomizedCourseSuggestions,
    ];

    res.status(200).json(combinedSuggestions);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export const deleteSuggestion = async (req, res) => {
  try {
    const suggestionId = req.params.suggestionId;

    const lesson = await Lesson.findOne({ "suggestions._id": suggestionId });
    const course = await Course.findOne({
      "courseSuggestions._id": suggestionId,
    });

    if (!lesson && !course) {
      return res.status(404).json({ message: "Suggestion not found" });
    }

    if (lesson) {
      const suggestionIndex = lesson.suggestions.findIndex(
        (suggestion) => suggestion._id == suggestionId
      );
      lesson.suggestions.splice(suggestionIndex, 1);
      await lesson.save();
      return res
        .status(200)
        .json({ message: "Suggestion deleted successfully" });
    } else {
      const suggestionIndex = course.courseSuggestions.findIndex(
        (suggestion) => suggestion._id == suggestionId
      );
      course.courseSuggestions.splice(suggestionIndex, 1);
      await course.save();
      return res
        .status(200)
        .json({ message: "Suggestion deleted successfully" });
    }
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
};

export const addAssignment = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;
    const { title, description } = req.body;
    const assignmentPdf = req.file ? req.file.filename : null; // Check if a file is provided

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    // Find the course and lesson by their IDs
    const course = await Course.findById(courseId);
    const lesson = await Lesson.findById(lessonId);

    if (!course || !lesson) {
      return res.status(404).json({ message: "Course or Lesson not found" });
    }

    // Check if the authenticated user is the instructor of the course
    if (course.instructor.toString() !== userId) {
      return res
        .status(403)
        .json({
          message: "Only the instructor can add assignments to this lesson",
        });
    }

    // Check if title and description are provided
    if (!title || !description) {
      return res
        .status(400)
        .json({
          message:
            "Both title and description are required for adding an assignment",
        });
    }

    // Check if the lesson already has an assignment
    // if (lesson.assignment && lesson.assignment.title) {
    //   return res.status(400).json({ message: "Assignment already exists for this lesson" });
    // }

    // Add the assignment to the lesson
    lesson.assignment = {
      title,
      description,
      assignmentPdf,
      submissions: [],
    };

    // Save the assignment details in the lesson
    await lesson.save();

    // Save the assignment details in the course
    course.lessons.forEach((courseLesson) => {
      if (courseLesson._id.toString() === lessonId) {
        courseLesson.assignment = {
          title,
          description,
          assignmentPdf,
          submissions: [],
        };
      }
    });

    // Save the course
    await course.save();

    res
      .status(201)
      .json({ message: "Assignment added successfully to lesson and course" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const addFinalAssignment = async (req, res) => {
  try {
    const { courseId } = req.params;
    const { title, description } = req.body;
    const assignmentPdf = req.file ? req.file.filename : null; // Check if a file is provided

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    // Find the course and lesson by their IDs
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Check if the authenticated user is the instructor of the course
    if (course.instructor.toString() !== userId) {
      return res
        .status(403)
        .json({
          message: "Only the instructor can add assignments to this lesson",
        });
    }

    // Check if title and description are provided
    if (!title || !description) {
      return res
        .status(400)
        .json({
          message:
            "Both title and description are required for adding an assignment",
        });
    }

    // Check if the lesson already has an assignment
    // if (lesson.assignment && lesson.assignment.title) {
    //   return res.status(400).json({ message: "Assignment already exists for this lesson" });
    // }

    // Add the assignment to the lesson
    course.finalAssignment = {
      title,
      description,
      assignmentPdf,
      submissions: [],
    };

    // Save the assignment details in the lesson
    await course.save();

    res
      .status(201)
      .json({ message: "Assignment added successfully to lesson and course" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const uploadAssignment = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    // Find the course and lesson by their IDs
    const course = await Course.findById(courseId);
    const lesson = await Lesson.findById(lessonId);

    if (!course || !lesson) {
      return res.status(404).json({ message: "Course or Lesson not found" });
    }

    // Check if the user is enrolled in the course
    // if (!course.enrolledUsers.includes(userId)) {
    //   return res.status(403).json({ message: "Only enrolled users can upload assignments" });
    // }

    // Check if the assignment exists for the lesson
    if (!lesson.assignment) {
      return res
        .status(404)
        .json({ message: "No assignment found for this lesson" });
    }

    // Add the assignment submission to the lesson
    lesson.assignment.submissions.push({
      user: userId,
      file: req.file.filename,
    });

    // Save the lesson
    await lesson.save();

    // Add the assignment submission to the course
    const courseLessonIndex = course.lessons.findIndex(
      (courseLesson) => courseLesson._id.toString() === lessonId
    );
    if (courseLessonIndex !== -1) {
      course.lessons[courseLessonIndex].assignment.submissions.push({
        user: userId,
        file: req.file.filename,
      });
    }

    // Save the course
    await course.save();

    res.status(201).json({ message: "Assignment submitted successfully" });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const getAllAssignmentsDetailsWithSubmission = async (req, res) => {
  try {
    const { courseId } = req.params;

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Get all lessons for the course, along with their assignments
    const lessonsWithAssignments = await Lesson.find({
      _id: { $in: course.lessons },
    }).populate("assignment.submissions.user");

    // Extract assignment details from each lesson
    const assignments = lessonsWithAssignments.reduce((acc, lesson) => {
      if (lesson.assignment) {
        acc.push({
          lessonId: lesson._id,
          title: lesson.assignment.title,
          description: lesson.assignment.description,
          assignmentPdf: lesson.assignment.assignmentPdf,
          submissions: lesson.assignment.submissions.map((submission) => ({
            userId: submission.user._id,
            userName: submission.user.name, // Assuming user has a 'name' field
            file: submission.file,
            submittedAt: submission.submittedAt,
          })),
        });
      }
      return acc;
    }, []);

    res.status(200).json(assignments);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const getAllAssignmentsDetailsWithoutSubmission = async (req, res) => {
  try {
    const { courseId } = req.params;

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Get all lessons for the course, along with their assignments
    const lessonsWithAssignments = await Lesson.find({
      _id: { $in: course.lessons },
    });

    // Extract assignment details from each lesson
    const assignments = lessonsWithAssignments.reduce((acc, lesson) => {
      if (lesson.assignment) {
        acc.push({
          lessonId: lesson._id,
          title: lesson.assignment.title,
          description: lesson.assignment.description,
        });
      }
      return acc;
    }, []);

    res.status(200).json(assignments);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const updateAssignment = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;
    const { title, description } = req.body;
    const assignmentPdf = req.file ? req.file.filename : null; // Check if a file is provided

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    // Find the course and lesson by their IDs
    const course = await Course.findById(courseId);
    const lesson = await Lesson.findById(lessonId);

    if (!course || !lesson) {
      return res.status(404).json({ message: "Course or Lesson not found" });
    }

    // Check if the authenticated user is the instructor of the course
    if (course.instructor.toString() !== userId) {
      return res
        .status(403)
        .json({
          message: "Only the instructor can update assignments for this lesson",
        });
    }

    // Check if the lesson already has an assignment
    if (!lesson.assignment || !lesson.assignment.title) {
      return res
        .status(400)
        .json({ message: "No assignment exists for this lesson to update" });
    }

    // Update the assignment details in the lesson
    lesson.assignment.title = title || lesson.assignment.title;
    lesson.assignment.description =
      description || lesson.assignment.description;
    lesson.assignment.assignmentPdf =
      assignmentPdf || lesson.assignment.assignmentPdf;

    // Save the updated assignment details in the lesson
    await lesson.save();

    // Update the assignment details in the course
    course.lessons.forEach((courseLesson) => {
      if (courseLesson._id.toString() === lessonId) {
        courseLesson.assignment = lesson.assignment;
      }
    });

    // Save the course
    await course.save();

    res
      .status(200)
      .json({
        message: "Assignment updated successfully for lesson and course",
      });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const deleteAssignment = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res
        .status(401)
        .json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

    // Find the course and lesson by their IDs
    const course = await Course.findById(courseId);
    const lesson = await Lesson.findById(lessonId);

    if (!course || !lesson) {
      return res.status(404).json({ message: "Course or Lesson not found" });
    }

    // Check if the authenticated user is the instructor of the course
    if (course.instructor.toString() !== userId) {
      return res
        .status(403)
        .json({
          message: "Only the instructor can delete assignments for this lesson",
        });
    }

    // Check if the lesson already has an assignment
    if (!lesson.assignment || !lesson.assignment.title) {
      return res
        .status(400)
        .json({ message: "No assignment exists for this lesson to delete" });
    }

    // Delete the assignment from the lesson
    lesson.assignment = undefined;

    // Save the updated lesson without the assignment
    await lesson.save();

    // Delete the assignment from the course
    course.lessons.forEach((courseLesson) => {
      if (courseLesson._id.toString() === lessonId) {
        courseLesson.assignment = undefined;
      }
    });

    // Save the updated course without the assignment
    await course.save();

    res
      .status(200)
      .json({
        message: "Assignment deleted successfully for lesson and course",
      });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const getAssignmentDetailsOfLessonWithSubmission = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;

    // Find the lesson by its ID
    const lesson = await Lesson.findById(lessonId);

    if (!lesson) {
      return res.status(404).json({ message: "Lesson not found" });
    }

    // Check if the lesson has an assignment
    if (!lesson.assignment || !lesson.assignment.title) {
      return res
        .status(404)
        .json({ message: "No assignment found for this lesson" });
    }

    // Return assignment details
    res.status(200).json({
      title: lesson.assignment.title,
      description: lesson.assignment.description,
      assignmentPdf: lesson.assignment.assignmentPdf,
      submissions: lesson.assignment.submissions,
    });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const getAssignmentDetailsOfLessonWithoutSubmission = async (
  req,
  res
) => {
  try {
    const { courseId, lessonId } = req.params;

    // Find the lesson by its ID
    const lesson = await Lesson.findById(lessonId);

    if (!lesson) {
      return res.status(404).json({ message: "Lesson not found" });
    }

    // Check if the lesson has an assignment
    if (!lesson.assignment || !lesson.assignment.title) {
      return res
        .status(404)
        .json({ message: "No assignment found for this lesson" });
    }

    // Return assignment details without submissions
    const { submissions, ...assignmentDetails } = lesson.assignment;
    res.status(200).json(assignmentDetails);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const getAllSubmissionsForLesson = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;

    // Find the lesson by its ID
    const lesson = await Lesson.findById(lessonId);

    if (!lesson) {
      return res.status(404).json({ message: "Lesson not found" });
    }

    // Check if the lesson has an assignment
    if (!lesson.assignment || !lesson.assignment.title) {
      return res
        .status(404)
        .json({ message: "No assignment found for this lesson" });
    }

    // Check if submissions exist
    if (
      !lesson.assignment.submissions ||
      lesson.assignment.submissions.length === 0
    ) {
      return res
        .status(404)
        .json({ message: "No submissions found for this lesson" });
    }

    // Return all submissions for the lesson
    res.status(200).json({ submissions: lesson.assignment.submissions });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const getAllUserSubmissions = async (req, res) => {
  try {
    const { userId } = req.params;

    // Find all lessons that have submissions by the user
    const lessonsWithSubmissions = await Lesson.find({
      "assignment.submissions.user": mongoose.Types.ObjectId(userId),
    });

    // Collect all submissions for the user
    const allSubmissions = [];

    lessonsWithSubmissions.forEach((lesson) => {
      if (lesson.assignment && lesson.assignment.submissions) {
        const userSubmissions = lesson.assignment.submissions.filter(
          (submission) => submission.user.toString() === userId
        );

        userSubmissions.forEach((submission) => {
          allSubmissions.push({
            course: lesson.course, // Assuming there is a reference to the course in the lesson schema
            lesson: lesson.title,
            assignment: lesson.assignment.title,
            submission,
          });
        });
      }
    });

    // Return all submissions for the user
    res.status(200).json({ userSubmissions: allSubmissions });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const getAllCourseSubmissions = async (req, res) => {
  try {
    const { courseId } = req.params;

    // Find the course by its ID
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Check if the course has lessons with assignments and submissions
    const allSubmissions = [];

    for (const lesson of course.lessons) {
      if (
        lesson.assignment &&
        lesson.assignment.submissions &&
        lesson.assignment.submissions.length > 0
      ) {
        allSubmissions.push({
          lesson: lesson.title,
          assignment: lesson.assignment.title,
          submissions: lesson.assignment.submissions,
        });
      }
    }

    // Return all submissions for the course
    res.status(200).json({ courseSubmissions: allSubmissions });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

export const downloadAssignmentPdf = async (req, res) => {
  try {
    const { courseId, lessonId } = req.params;

    // Find the course and lesson by their IDs
    const course = await Course.findById(courseId);
    const lesson = await Lesson.findById(lessonId);

    if (!course || !lesson) {
      return res.status(404).json({ message: "Course or Lesson not found" });
    }

    // Check if the lesson has an assignment with a PDF
    if (!lesson.assignment || !lesson.assignment.assignmentPdf) {
      return res
        .status(404)
        .json({ message: "No assignment PDF found for this lesson" });
    }

    // Provide the assignment PDF for download
    res.sendFile(
      path.join(
        __dirname,
        "../public/userImages",
        lesson.assignment.assignmentPdf
      )
    );
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "Error in fetching assignment PDF" });
  }
};

export const downloadFinalAssignmentPdf = async (req, res) => {
  try {
    const { courseId } = req.params;

    // Find the course and lesson by their IDs
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: "Course not found" });
    }

    // Check if the lesson has an assignment with a PDF
    if (!course.finalAssignment || !course.finalAssignment.assignmentPdf) {
      return res
        .status(404)
        .json({ message: "No assignment PDF found for this course" });
    }

    // Provide the assignment PDF for download
    res.sendFile(
      path.join(
        __dirname,
        "../public/userImages",
        course.finalAssignment.assignmentPdf
      )
    );
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "Error in fetching assignment PDF" });
  }
};

export const downloadSubmissionPdf = async (req, res) => {
  try {
    const { courseId, lessonId, submissionId } = req.params;

    // Find the course and lesson by their IDs
    const course = await Course.findById(courseId);
    const lesson = await Lesson.findById(lessonId);

    if (!course || !lesson) {
      return res.status(404).json({ message: "Course or Lesson not found" });
    }

    // Check if the lesson has an assignment with submissions
    if (
      !lesson.assignment ||
      !lesson.assignment.submissions ||
      lesson.assignment.submissions.length === 0
    ) {
      return res
        .status(404)
        .json({ message: "No submissions found for this lesson" });
    }

    // Find the submission by its ID
    const submission = lesson.assignment.submissions.find(
      (submission) => submission._id.toString() === submissionId
    );

    if (!submission || !submission.file) {
      return res
        .status(404)
        .json({ message: "Submission or submission PDF not found" });
    }

    // Provide the submission PDF for download
    res.sendFile(path.join(__dirname, "../public/userImages", submission.file));
  } catch (err) {
    console.log(err);
    return res
      .status(400)
      .json({ message: "Error in fetching submission PDF" });
  }
};

export const generateCertificateRequest = async (req, res) => {
  try {
    const { courseId } = req.params;

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    // if (!authHeader) {
    //   return res.status(401).json({ error: "Error: No authorization header found" });
    // }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

      // Check if a certificate request already exists
      const existingRequest = await CertificateRequest.findOne({ user: userId, course: courseId });
      console.log(existingRequest);

      // if (existingRequest.status == "approved") {
        
      // }
      
      // if(existingRequest){
      //   return res.status(400).json({ error: "Certificate request already exists" });
      // }

      if(existingRequest == null){
      // Create a new certificate request
      const certificateRequest = new CertificateRequest({
        user: userId,
        course: courseId,
      });

      await certificateRequest.save();
      }else{
        return res.status(400).json({ error: "Certificate request already exists" });
      }



      return res.status(200).json({ message: "Certificate request generated successfully" });

  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Server error" });
  }
};
export const checkCertificateRequest = async (req, res) => {
  try {
    const { courseId } = req.params;

    // Get the user ID from the token
    const authHeader = req.headers.authorization;
    // if (!authHeader) {
    //   return res.status(401).json({ error: "Error: No authorization header found" });
    // }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const userId = decoded._id;

      // Check if a certificate request already exists
      const existingRequest = await CertificateRequest.findOne({ user: userId, course: courseId });
      // console.log(existingRequest.status);
      if(existingRequest == null){
        return res.status(200).json({ message: "nothing" });
      }else{
        return res.status(200).json({ message: existingRequest.status });
      }
      

  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Server error" });
  }
};
// export const generateCertificateRequest = async (req, res) => {
//   try {
//     const { courseId } = req.params;

//     // Get the user ID from the token
//     const authHeader = req.headers.authorization;
//     if (!authHeader) {
//       return res.status(401).json({ error: "Error: No authorization header found" });
//     }

//     const token = authHeader.split(" ")[1];
//     const decoded = jwt.verify(token, process.env.JWT_SECRET);
//     const userId = decoded._id;

//     // Find the user's progress for this course
//     const progress = await Progress.findOne({ user: userId, course: courseId });

//     if (!progress) {
//       return res.status(400).json({ error: "User has not started this course yet" });
//     }

//     const course = await Course.findById(courseId);

//     // Check if all lessons are completed
//     const completedLessonsCount = progress.lessonProgress.filter((lp) => lp.completed).length;

//     if (completedLessonsCount !== course.lessons.length) {
//       return res.status(400).json({ error: "All course lessons not completed" });
//     }

//     // Check if all assignments are submitted for lessons with assignments
//     const lessonsWithAssignments = course.lessons.filter((lesson) => lesson.assignment && lesson.assignment.submissions.length > 0);

//     const lessonsWithoutSubmissions = lessonsWithAssignments.filter((lesson) => {
//       return (
//         lesson.assignment &&
//         !lesson.assignment.submissions.some((submission) => submission.user.toString() === userId)
//       );
//     });

//     const allAssignmentsSubmitted = lessonsWithoutSubmissions.length === 0;

//     if (allAssignmentsSubmitted) {
//       // Check if a certificate request already exists
//       const existingRequest = await CertificateRequest.findOne({ user: userId, course: courseId });

//       if (existingRequest) {
//         return res.status(400).json({ error: "Certificate request already exists" });
//       }

//       // Create a new certificate request
//       const certificateRequest = new CertificateRequest({
//         user: userId,
//         course: courseId,
//       });

//       await certificateRequest.save();

//       return res.status(200).json({ message: "Certificate request generated successfully" });
//     } else {
//       return res.status(400).json({ error: "User has not submitted all assignments" });
//     }
//   } catch (error) {
//     console.log(error);
//     res.status(500).json({ error: "Server error" });
//   }
// };

export const getCertificateRequestsByInstructorId = async (req, res) => {
  try {
    const { userId , courseId } = req.params;

    // Check if the user ID from the token matches the provided instructor ID
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res.status(401).json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const requestingUserId = decoded._id;

    if (requestingUserId !== userId) {
      return res.status(403).json({ error: "You are not authorized to view these requests" });
    }

    // Check if the user is an instructor
    const user = await User.findById(userId);
    if (!user || !user.role.includes('Instructor')) {
      return res.status(403).json({ error: "You are not an instructor" });
    }

    // Get certificate requests for the instructor
    const certificateRequests = await CertificateRequest.find({ instructor: userId })
      .populate('user', 'name email') // Populate user details
      .populate('course', 'name'); // Populate course details

    res.status(200).json({ certificateRequests });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const approveCertificateRequest = async (req, res) => {
  try {
    const { requestId } = req.params;

    // Find the certificate request
    const request = await CertificateRequest.findById(requestId).populate('user course');

    if (!request) {
      return res.status(404).json({ error: 'Certificate request not found' });
    }

    // Check if the user ID from the token matches the instructor ID
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      return res.status(401).json({ error: "Error: No authorization header found" });
    }

    const token = authHeader.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const instructorId = decoded._id;

    if (instructorId !== request.course.instructor.toString()) {
      return res.status(403).json({ error: "You are not authorized to approve this request" });
    }

    // Update the status to approved
    request.status = 'approved';
    await request.save();

    res.status(200).json({ message: 'Certificate request approved successfully' });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const disapproveCertificateRequest = async (req, res) => {
  try {
    const { requestId } = req.params;

    // Find the certificate request
    const request = await CertificateRequest.deleteOne({ _id: requestId });

    res.status(200).json({ message: 'Certificate request disapproved successfully' });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Server error" });
  }
};

export const getCertificateRequestDetails = async (req, res) => {
  try {
    const { requestId } = req.params;
    
    // Find the certificate request by ID
    const certificateRequest = await CertificateRequest.findById(requestId)
      .populate('user') // Populate the user details
      .populate('course'); // Populate the course details

    if (!certificateRequest) {
      return res.status(404).json({ message: 'Certificate request not found' });
    }

    res.status(200).json(certificateRequest);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

export const getApprovedCertificates = async (req, res) => {
  try {
    const { instructorId } = req.params;

    // Find all approved certificates for the specified instructor
    const certificates = await CertificateRequest.find({
      status: 'approved',
      instrtuctor: instructorId,
    })
    .populate('user')
    .populate('course');

    res.status(200).json({ certificates });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

export const getDisapprovedCertificates = async (req, res) => {
  try {
    const { instructorId } = req.params;

    // Find all disapproved certificates for the specified instructor
    const certificates = await CertificateRequest.find({
      status: 'disapproved',
      instructorId: instructorId,
    })
    .populate('user')
    .populate('course');

    res.status(200).json({ certificates });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

// get details of a certificate request by a particular user
export const getCertificateRequestByUser = async (req, res) => {
  try {
    const { userId } = req.params;

    // Find the certificate request for a particular user
    const certificateRequest = await CertificateRequest.findOne({ user: userId })
      .populate('user') // Populate the user details
      .populate('course'); // Populate the course details

    if (!certificateRequest) {
      return res.status(404).json({ message: 'Certificate request not found' });
    }

    res.status(200).json(certificateRequest);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};
