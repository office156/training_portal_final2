import React, { useState } from 'react';
import "./UserList.css";
import Sidebar from '../Sidebar/Sidebar';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import ImageModal from '../ImageModal/ImageModal';
import SearchBar from '../Testing/SearchBar';
import SearchResults from '../Testing/SearchResults';

function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ display: "flex", justifyContent: "center" }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};





export default function UserList() {


  const navigate = useNavigate();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [rows, setRows] = useState([])
  const [selectedEmail, setSelectedEmail] = useState();
  const [subject , setSubject] = useState("");
  const [message , setMessage] = useState("");
  const [adminname , setadminname] = useState("")
  const [adminmail , setadminmail] = useState("")

  

  const check = "E:/training-portal/src/Images/ales.jpg"

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  useEffect(() => {
    axios.get('http://localhost:8000/api/admin/allusers').then((response) => {
      console.log(response.data)
      setRows(response.data)
      setFilteredItems(response.data)
      setadminmail(sessionStorage.getItem("email"))
      setadminname(sessionStorage.getItem("name"))
    })
      .catch((error) => {
        // console.log(error)
        // console.log(error.response.data)
        // alert()
      });
  }, []);


  function ApproveUser(email1) {
    // console.log("something")
    const url = 'http://localhost:8000/api/admin/approveUser/' + email1
    axios.post(url)
      .then(response => {
        console.log(response.data);
        axios.get('http://localhost:8000/api/admin/allusers').then((response) => {
          console.log(response.data)
          setRows(response.data)
        })
          .catch((error) => {
            console.log(error)
          });

      })
      .catch(error => {
        console.log(error);
      });

  }

  function DisapproveUser(email1) {
    // console.log("something")
    const url = 'http://localhost:8000/api/admin/unapproveUser/' + email1
    axios.post(url)
      .then(response => {
        console.log(response.data);
        axios.get('http://localhost:8000/api/admin/allusers').then((response) => {
          console.log(response.data)
          setRows(response.data)
        })
          .catch((error) => {
            console.log(error)
          });

      })
      .catch(error => {
        console.log(error);
      });

  }

  function SendEmail (email1) {
    // event.prevent
    if (subject === "" || message === "") {
      toast.error("Please enter all the details")
    } else {
      const data = { "name": subject, "email": message };
      
      const url = "http://localhost:8000/api/admin/user/send/" + email1
      axios.post(url, data).then((response) => {
        console.log(response)
        setMessage('');
        setSubject('');
        toast.success(response.data)
      })
        .catch((error) => {
          toast.error(error.response.data)

        });
    }
  }

  // function GetAdhaar(id) {
  //   // const url = "http://localhost:8000/api/userdocs/download/" + id
  //   const url = "http://localhost:8000/api/admin/user/"+ id + "/aadharcard"
  //   axios.get(url)
  //     .then(response => {
  //       console.log(response.data);
  //     })
  //     .catch(error => {
  //       console.log(error);
  //     });
  // }

  const handleDownloadPhoto = (id) => {
    axios({
      url: "http://localhost:8000/api/admin/user/photo/"+ id,
      method: 'GET',
      responseType: 'blob',
    }).then((response) => {
      console.log(response)
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'photo.jpg');
      document.body.appendChild(link);
      link.click();
    });
  };

  const handleDownloadAadharcard = (id) => {
    axios({
      url: "http://localhost:8000/api/admin/user/aadhar/" + id,
      method: 'GET',
      responseType: 'blob',
    }).then((response) => {
      console.log(response)
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'aadharcard.jpg');
      document.body.appendChild(link);
      link.click();
    });
  };

  const [filteredItems, setFilteredItems] = useState(rows);
  
  const handleSearch = (query) => {
    console.log(query)
    const filteredResults = rows.filter((item) =>
      item.email.toLowerCase().includes(query.toLowerCase())
    );
    setFilteredItems(filteredResults);
  };

  return (
    <div className='userListTop'>
      <Sidebar className="adminPanelSidebartag" name={adminname} mail={adminmail}></Sidebar>
      <div className="userListSecond">
        <div className="topLineUserLIstPage py-5 container d-flex justify-content-between">
          <div className="headingUserList">
            Users
          </div>
          <div className="UserListLinks d-flex justify-content-between">
            <div className="UserListTopLink1 px-3">Download in excel file</div>
            <div className="UserListTopLink1">Create New User</div>
          </div>
        </div>


        <div className='container mb-5'>
      <SearchBar onSearch={handleSearch} />
      {/* <SearchResults results={filteredItems} /> */}
    </div>


        <div className="tableUsers container d-flex justify-content-center">
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell align="right">Email</TableCell>
                  <TableCell align="right">Phone</TableCell>
                  <TableCell align="right">Institute</TableCell>
                  <TableCell align="right">Photo</TableCell>
                  <TableCell align="right">Aadhar card</TableCell>
                  <TableCell align="right">Status</TableCell>
                  <TableCell align="right">Action</TableCell>
                  <TableCell align="right">Email</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {(rowsPerPage > 0
                  ? filteredItems.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  : filteredItems
                ).map((row) => (
                  <TableRow
                    key={row._id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align="right">{row.email}</TableCell>
                    <TableCell align="right">{row.phone}</TableCell>
                    <TableCell align="right">{row.institute}</TableCell>
                    <TableCell align="right" className='UserListAdmin' onClick={() => handleDownloadPhoto(row._id)}>View</TableCell>
                    <TableCell align="right" className='UserListAdmin' onClick={() => handleDownloadAadharcard(row._id)}>View</TableCell>
                    <TableCell align="right">
                      {row.isApproved ? 'Active' : 'Inactive'}
                    </TableCell>
                    <TableCell align="right">
                      <button type='button' className='ApproveUser' >
                        {row.isApproved ? <i class="fa-solid fa-minus" onClick={() => DisapproveUser(row.email)}></i> : <i class="fa-solid fa-check" onClick={() => ApproveUser(row.email)}></i>}

                      </button>
                    </TableCell>
                    <TableCell align="right">
                    <button type="button" class="border-0" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    <i class="fa-solid fa-envelope" onClick={() => {
                      setSelectedEmail(row.email)
                    }}  ></i>
</button>
                    </TableCell>
                  </TableRow>
                ))}

                {/* {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )} */}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                    // colSpan={3}
                    count={filteredItems.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputProps: {
                        'aria-label': 'rows per page',
                      },
                      native: true,
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </TableContainer>
        </div>

        {/* <!-- Modal --> */}
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">New message</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Recipient: {selectedEmail}</label>
            {/* <input type="text" class="form-control" id="recipient-name"></input> */}
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Subject:</label>
            <input type="text" class="form-control" id="email-subject" value={subject} onChange={e => setSubject(e.target.value)}></input>
          </div>
          <div class="mb-3">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="email-text" value={message} onChange={e => setMessage(e.target.value)}></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="" data-bs-dismiss="modal">Close</button>
        <button type="button" class="" onClick={()=>SendEmail(selectedEmail)}>Send email</button>
        
      </div>
    </div>
  </div>
</div>
      </div>

      <ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>
      
    </div>
  )
}
