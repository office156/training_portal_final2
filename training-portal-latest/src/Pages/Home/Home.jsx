import React from 'react';
import Cards from '../../Components/Cards/Cards';
import Cards2 from '../../Components/Cards2/Cards2';
import Faq from '../../Components/FAQ/Faq';
import Footer from '../../Components/Footer/Footer';
import ImageSlider from '../../Components/ImageSlider/ImageSlider';
import InstructorSlider from '../../Components/InstructorSlider/InstructorSlider';
import LPathHome from '../../Components/LPathHome/LPathHome';
import Navbar from '../../Components/Navbar/Navbar';

export default function Home() {
  return <div className='HomePageTop'>
      <Navbar></Navbar>
      {/* <NavbarTest></NavbarTest> */}
      <ImageSlider></ImageSlider>

<div className='container my-4'>

  <h2 className='text-center'>Let's learn</h2>

  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus nulla incidunt perferendis obcaecati iusto magnam similique, molestiae doloribus asperiores aut dolorum laborum at sequi maiores, dolor velit voluptatibus deserunt excepturi?</p>

</div>

      {/* <Cards></Cards> */}
      <Cards2></Cards2>
      <LPathHome></LPathHome>
      {/* <InstructorSlider></InstructorSlider> */}
      <Faq></Faq>
      <Footer></Footer>
  </div>;
}
