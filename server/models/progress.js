import mongoose from "mongoose";
const { ObjectId } = mongoose.Schema.Types;

const progressSchema = new mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    course: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Course",
      required: true,
    },
    lessonProgress: [
      {
        lesson: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Lesson",
          required: true,
        },
        // lesson completion
        completed: {
          type: Boolean,
          default: false,
        },
      },
    ],
    // course completion
    completed: {
      type: Boolean,
      default: false,
    },
    percentageCompleted: {
      type: Number,
      default: 0,
    },
  },
  { timestamps: true }
);
  
export default mongoose.model("Progress", progressSchema);