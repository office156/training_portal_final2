import mongoose from "mongoose";
const { ObjectId } = mongoose.Schema.Types;

const quizSchema = new mongoose.Schema({
  lesson: {
    type: String,
    required: true,
  },
  question: {
    type: String,
    required: true,
  },
  option1: {
    type: String,
    required: true,
  },
  option2: {
    type: String,
    required: true,
  },
  option3: {
    type: String,
    required: true,
  },
  option4: {
    type: String,
    required: true,
  },
  answer: {
    type: String,
    required: true,
  },
},
{ timestamps: true }
);

export default mongoose.model("Quiz", quizSchema);
