import mongoose from "mongoose";
const { ObjectId } = mongoose.Schema.Types;
import Lesson from "./lesson.js";
import Assignment from "../models/assignment";

const courseSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      minlength: 3,
      maxlength: 320,
      required: true,
    },
    description: {
      type: {},
      minlength: 200,
      required: true,
    },
    whatyouwilllearn: {
      type: [String],
      minlength: 200,
      required: true,
    },
    prerequisites: {
      type: [String],
      minlength: 200,
      required: true,
    },
    price: {
      type: Number,
      default: 0.00,
    },
    image: {
      type: String,
    },
    category: String,
    requested: {
      type: Boolean,
      default: false,
    },
    instructor: {
      type: ObjectId,
      ref: "User",
      required: true,
    },
    lessons: [Lesson.schema],
    finalAssignment: {
      title: {
        type: String,
        required: false,
      },
      description: {
        type: String,
        required: false,
      },
      assignmentPdf: {
        type: String,
        required: false,
      },
      submissions: [
        {
          user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
          },
          file: {
            type: String,
          },
          submittedAt: {
            type: Date,
            default: Date.now,
          },
        },
      ],
    },
    isApproved: {
      type: Boolean,
      default: false,
    },
    forum: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "DiscussionPost",
    },
    enrolledUsers: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
    ],
    announcements: [
      {
        title: {
          type: String,
          required: true,
        },
        text: {
          type: String,
          required: true,
        },
        createdAt: {
          type: Date,
          default: Date.now,
        },
      },
    ],

    adminApproval: {
      type: Boolean,
      default: false,
    },
    requestDate: {
      type: Date,
      default: Date.now,
    },

    courseSuggestions: [
      {
        text: {
          type: String,
          required: true,
        },
        createdAt: {
          type: Date,
          default: Date.now,
        },
      },
    ], 

    // assignments: [
    //   {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: 'Assignment',
    //   },
    // ],
    
  },
  { timestamps: true }
);

export default mongoose.model("Course", courseSchema);