import mongoose from "mongoose";
const { ObjectId } = mongoose.Schema.Types;

const certificateRequestSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  course: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Course',
    required: true,
  },
  status: {
    type: String,
    enum: ['pending', 'approved', 'disapproved'],
    default: 'pending',
  },
});

export default mongoose.model("CertificateRequest", certificateRequestSchema);
