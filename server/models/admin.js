import mongoose from "mongoose";
const { Schema } = mongoose;

const adminSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: true,
    },
    email: {
      type: String,
      trim: true,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      min: 8,
      max: 64,
    },
    role: {
      type: [String],
      default: ["Admin"],
    },
  },
  { timestamps: true }
);

export default mongoose.model("Admin", adminSchema);
