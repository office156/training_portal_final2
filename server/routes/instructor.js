import express from "express";
const router = express.Router();

var bodyParser = require('body-parser');
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

// middlewares
// import { requireSignin } from "../middlewares";

import { makeInstructor, currentInstructor } from "../controllers/instructor";

router.post("/make-instructor", makeInstructor);
router.get("/current-instructor", currentInstructor);

module.exports = router;
