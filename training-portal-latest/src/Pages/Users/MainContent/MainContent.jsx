import React from 'react'
import ProfileForm from '../ProfileForm/ProfileForm';
import MyDocuments from '../MyDocuments/MyDocuments';
import MyCourses from '../MyCourses/MyCourses';
import Reset from '../PasswordReset/Reset';
import { useState } from 'react';

export default function MainContent() {
    const [openContent, setOpenContent] = useState('myCourses');


  return (
    <div className="UserMainContent">
      {openContent === 'profileForm' && <ProfileForm />}
      {openContent === 'passwordReset' && <Reset />}
      {openContent === 'myDocuments' && <MyDocuments />}
      {openContent === 'myCourses' && <MyCourses />}
    </div>
  )
}
