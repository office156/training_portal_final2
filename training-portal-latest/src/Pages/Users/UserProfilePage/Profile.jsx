import React, { useState } from 'react'
import UserProfileLeft from '../../../Components/Users/UserProfileLeft/UserProfileLeft'
import ProfileDetails from '../../../Components/Users/ProfileDetails/ProfileDetails'
import "./Profile.css"
import MainContent from '../MainContent/MainContent'
import ProfileForm from '../ProfileForm/ProfileForm'
import MyDocuments from '../MyDocuments/MyDocuments'
import MyCourses from '../MyCourses/MyCourses'
import Reset from '../PasswordReset/Reset'
import { useEffect } from 'react'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import { AuthContext } from '../../../App'
import { useContext } from 'react'

export default function Profile() {

  // const [showProfile,setShowProfile] = useState(true);
  // const [showDocuments,setShowDocuments] = useState(false);
  // const [showPassword,setShowPassword] = useState(false);
  // const [showCourses,setShowCourses] = useState(false);
  // const [imageUrl, setImageUrl] = useState(null);
  const [name, setname] = useState();
  const [email, setemail] = useState();
  const [institute, setinstitute] = useState();
  const [address, setaddress] = useState();
  const [HodEmail, setHodEmail] = useState();
  const [phone, setphone] = useState();
  const [domain, setdomain] = useState();
  const [eduQualification, seteduQualification] = useState();
  const [myCourses , setMyCourses] = useState([]);
  const [myValue, setMyValue] = useState(null);
  const [selectedComponent, setSelectedComponent] = useState('about');
  const [value, setvalue] = useState('courses');
  const { loggedIn , setLoggedIn} = useContext(AuthContext);

  
  const userFromSessionStorage = sessionStorage.getItem('userId');
  const token = sessionStorage.getItem('Token');

  const navigate = useNavigate();

  const handleComponentClick = (componentName) => {
    setSelectedComponent(componentName);
    setvalue(componentName);
  };

  useEffect(() => {
    // console.log(loggedIn)
    const user = sessionStorage.getItem('userId')
    const role = sessionStorage.getItem('Role')
    if (user === null) {
      setLoggedIn(false)
      // navigate("/login")
    }else if (role === "Learner") {
      setLoggedIn(true)
    }
    // if(!loggedIn){
    //   navigate("/login")
    // }
    
    const url = "http://localhost:8000/api/admin/user/details/id/" + userFromSessionStorage
    axios.get(url).then((response) => {
      console.log(response.data)
      setname(response.data.name)
      setemail(response.data.email)
      setphone(response.data.phone)
      setaddress(response.data.address)
      setinstitute(response.data.institute)
      seteduQualification(response.data.eduqua)
      setdomain(response.data.domain)
      setHodEmail(response.data.hodemail)
      // const timer = setTimeout(() => {
      //   setMyValue('Hello world');
      // }, 2000);
  
      // return () => clearTimeout(timer);
    })
      .catch((error) => {
        console.log(error.data)
      });
    
      
      const token = sessionStorage.getItem('Token');

// testing 
      if (token === null) {
        navigate("/login")
      }

      // console.log(token)
      console.log(userFromSessionStorage)
        const url1 = "http://localhost:8000/api/enrolledcourses/" +  userFromSessionStorage
        axios.get(url1, {
          headers: {
            'Authorization': `Bearer ${token}`
          }
        }).then((response) => {
          console.log(response.data)
          setMyCourses(response.data.filter(item => item['adminApproval'] === true))
        })
          .catch((error) => {
            console.log(error.data)
          });


  }, []);


  // function myFunction() {
  //   var profile = document.getElementById("userProfile");
  //   var documents = document.getElementById("userDocuments");
  //   var password = document.getElementById("userPasswordReset");
  //   var courses = document.getElementById("userCourses");

  //   documents.classList.remove("active-sidebar-content");
  //   password.classList.remove("active-sidebar-content");
  //   courses.classList.remove("active-sidebar-content");
  //   profile.classList.add("active-sidebar-content");
  //   // element.classList.add("active-sidebar-content");

  //   setShowProfile(true)
  //   setShowDocuments(false)
  //   setShowPassword(false)
  //   setShowCourses(false)
  // }
  // function myFunction1() {
  //   var profile = document.getElementById("userProfile");
  //   var documents = document.getElementById("userDocuments");
  //   var password = document.getElementById("userPasswordReset");
  //   var courses = document.getElementById("userCourses");

  //   documents.classList.add("active-sidebar-content");
  //   password.classList.remove("active-sidebar-content");
  //   courses.classList.remove("active-sidebar-content");
  //   profile.classList.remove("active-sidebar-content");
  //   // element.classList.add("active-sidebar-content");

  //   setShowProfile(false)
  //   setShowDocuments(true)
  //   setShowPassword(false)
  //   setShowCourses(false)
  // }

  // function myFunction2() {
  //   var profile = document.getElementById("userProfile");
  //   var documents = document.getElementById("userDocuments");
  //   var password = document.getElementById("userPasswordReset");
  //   var courses = document.getElementById("userCourses");

  //   documents.classList.remove("active-sidebar-content");
  //   password.classList.add("active-sidebar-content");
  //   courses.classList.remove("active-sidebar-content");
  //   profile.classList.remove("active-sidebar-content");
  //   // element.classList.add("active-sidebar-content");
  //   setShowProfile(false)
  //   setShowDocuments(false)
  //   setShowPassword(true)
  //   setShowCourses(false)
  // }

  // function myFunction3() {
  //   var profile = document.getElementById("userProfile");
  //   var documents = document.getElementById("userDocuments");
  //   var password = document.getElementById("userPasswordReset");
  //   var courses = document.getElementById("userCourses");

  //   documents.classList.remove("active-sidebar-content");
  //   password.classList.remove("active-sidebar-content");
  //   courses.classList.add("active-sidebar-content");
  //   profile.classList.remove("active-sidebar-content");
  //   // element.classList.add("active-sidebar-content");
  //   setShowProfile(false)
  //   setShowDocuments(false)
  //   setShowPassword(false)
  //   setShowCourses(true)
  // }


  return (
    <div className='ProfileTop container'>
        <div className="row d-flex justify-content-between">
            <div className="UserProfilePageLeft col-md-3 d-flex">
            <div className='UserProfileLeft'>
      <div className="UserLeftTop d-flex flex-column align-items-center pb-5 courseCard mb-2 py-3">
      <div className="">
        <img  className='UserProfilePhotoImage' src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAJYAyAMBIgACEQEDEQH/xAAcAAAABwEBAAAAAAAAAAAAAAAAAQIDBAUGBwj/xABBEAABAwIEAwUEBgkCBwAAAAABAAIDBBEFEiExBkFREyIyYXEUgbHRByNCcpHBFVJTYoKSk6HhFiQzRGODosLx/8QAGgEAAwEBAQEAAAAAAAAAAAAAAAECAwQFBv/EACQRAAICAQQCAgMBAAAAAAAAAAABAhEDBBQhMRJBE1EyYfFx/9oADAMBAAIRAxEAPwCzZKpDH33VY2RPslX0588WIyu0IUKqrKeDEaWgkDu1qg4xkDTu6m6djmKocUmc7jTBrDNkikJ8gQdf7KJukaQSk6ZoHwWCaMJvoVK7QEaoAt6KhEMwHoiyWU4hpSCwHZAyEW3SC1S3RnkEy9hCQDVigUqxB2SSgAroIkEAGiNkRRapACyACCQ+aKNwa+RjXEXALgi0MUWJtzEftMI3lZ/MERqqc3AlaSOQuUcBTG3NTZBS31EfJsh9I3fJNOqW5srYZSd/CB8Sp4HTDRJp07+VO/3ub800+eVov2TdwPH1NuiVh4k5psiUEzz/AKkYHm4lBHkLxLNqdaUhjU81t1dkCmuVNOc3GlGf1aU/FyuHGNnjkY31cAs9JURf6xZI0l7GU1iW9bn5qMj6/wBNMa7f6NYHI86rBiNwSyF29ruNvL8ky7FXDV0lPFYkam/xTc0JRZdZ0WfVZz9MRZbyVtzc6MHyCivxinDLPdNI7zvZS8sUWsUmaqSdrB33tb6uTRxCmtpIHDq3X4LI/puCIHJC2/VxAuosnErWXDZKZpvzff8ANQ9TBey/gl9G0dXwEkBsjrb2amTWFzsrYDe1+88D5rEO4pa2+Wpj11OVt9VHk4oBN/aJSfJih6qJW2kbiWpnA0ZG25A1JKPtZucrAPJnzK58/iPNzqX+9RZsZEhuYHmxuMzlk9XEtaVnRvavqw6Sqa2/K7RZMOrqYuF64lttbSn8lzx+NSnRtOz3uukfpapAGVkQ9xKndova/s6E6uw7YydofO7viqt9dRtx6OdjR2LafLYM+1c8ljX4tVk+KP8AkS21dQ6mfK5/fDsoIGwUvVWUtOom8OO0gJyQyH3AJs49G0vc2BxuebvJc/dW1fOofr6Js1FQd6iQn7yndyHtYnQX8QEbUw97/wDCYOPzBxc2BmotbMVgjJKd5pD/ABlJObm8/wAyl6uZS00Ddvx6oO0cQHvKjS45UP0JhABB8PQ3WLLRzIRZW+Sl6mbGtPE2DsaqP28YHoEFjtPJBLcTH8ETqk+OR5SY6pgcNmxsuRr56KFUcRRAjM+R1t878oPuC54+plfcunkd/FZNF7Sbmx9Tcq5ayXohaWPs3EnFcMbnOiEIJAGl3Kvnxp7qg4k1zy4jKMgAPRUdNhuI1lvZKCqmvsWQucFbQcP4rVtGHRUj21bBmdE+zSB1/uElkyTK+KERqXiCofoGPN7nvSKM7Fqo+Ext911OruE8QweBs+LRsa2QlrA19zfdP8KcIHiKoljbWGmEbA//AIWfML26i3JRWVui14JFI/EKt29QR5AAJl08jvHM8n7xXTI/oopW27XEal555Gtb81Mj+jPCI/G2pl9ZbfBWtNlf9J+aCOSGztyT6orNA0bYLszOBMChAzYdn++9x/NSWcL4HH4cIo/4ogfirWjm+2iXqYr0ziIe0c2/ilNfmPdAPou6swqgiH1VBSs+7C0fknBTMb4I2N9GgK1oX7ZG6X0cLbBUP8FPMfuxuP5J1mHYjJ4KKpP/AGiu2uYRs38E07N0KvZL7Ierf0cdbgeLv8NBUH+CyOXh/GI4nSS0T42NFy57gB8V15wKpuJhfBagEAg5QdPMJS0cErsI6qTdUcyo6LtXAzkhv6oU5mDy1LXUmHML3nvWLrbeZUhsQYrbhSwxi51+rde59FhjhFujec2k2iiHBuOH/lox6zBKHBmNk6wQj1mC6f2jf1UO0HRdOzxnNu5nM28EYyfsUw9Zf8IzwPjHP2X+qfkulXaeQRXCNniFushzM8F4sOdN/VPyRHg3Feb6X+ofkukOy9SmX2RtMYLVZDnh4OxQfbpf6h+SC3b3BBLaYx7nIYLh52GvxaiOJxRGATML3uOUAX+1yI6rvOHQ4Y1jZMPpKPIdQ+BjbfiF5rY50ZBB26q+wHGqjD5xJRTOhfuW30d7lzYckemdc4v0eh7xuHfa4D0WLwVkMv0kYxc5WNhAH/iq/CONPackc0himds1x7rvQqPgWIP/ANY4pUX7zxb4Lrq6pmCnXaLH6XoGMw2gySB15X6A+Sh/RJTPlmquzG1OL+93+Ez9ItY6rpqRpIOVzjp7lK+imvbh0tY50WcPhYN9tSsZeUZ8cs1hKElb6N7JRzR/YPuUd2dh1CuIeIKV3jp3j33TNXXYbUOha4vZnlaLkeqaz5F+UCvhxy/GRWZydCEXYscdXhp81d+wYbLYtro29dUibCqLITBWtcelxqq3cP2Z7aV/ZSupANpR+CYfC9uxBCsK2l9np3vD87gxzgAOg8lHbHKRpG49bAraOVNWmZzwuLpohFrmjXZUFbxThlLXGjJklnDsuSJhcb9NFqJ7sjcXMIIF7kWXGOHq2Gn40pcSrJezhiq8732vYZT0U5MzjVCjhTts3ruJaRptJSVzPv0kg/8AVQMcxjD67DZIInubI4tt2kTmga33sunRYvPWH/aYXX1ALQ8OsyNpadj3nDoVU4viONw1EMDcEp2PqHObH21SNSBc3yg20WbzSlw2ZrjmjjjzEDlZNTzSDQRCZoJPoSCkGvqqR9o6Vsco5Atv8U3jGHVmKcQYrPN7NC72tzHtDiQHDM3u87XHTorCljqadsLJaaGsbHkDr7kBznG9xzDregC5Pyf0ejF1Hqw6XiiZgDagXI3BC02FYhTYg0AODJLeE81jjHh/1lLiNHJTzujjdHM3UxOyAE25tcbm3mosM1RQysa7me49huHAcx/lXHNPG+7RMsEMq6pnSzG1vMpt1uRVZhGMioY2Kq3GgerN7GnVpXfDJGatHnTxyg6Yy8hMSFPPjPIpl0auzMjPRpT2IkmyjltvJBrW3B1aeqO6AduOq8Sz1iZHVP7MtcGPH7ysqTF6jDfr4cjnvAae0u7+6oHWI1UusH+1it1HwWsZunRnKKfZdS4xPicd6gMGQ6Bum6l4Zj0mCNzMhEolFi0vy2t7vNZqgeQ2Ro19VIrXfVw3FtCrWSVXfJPguvRuIPpAhsO3oJR914d8lPj42wqoMGYyxFsgLg9nKx6LmcdO+Y/Vi/qp9Hh3anKHF5b4reEep+SqOfLIl4oo6dBxRhbpQxlQxzXm+lxZSpOJsPY4A1UTGg6kvWFoaJkAyxBuY7nTRJc+nEghrWNdE8fVvcNvK66PLjkjxro1tRxjhIoZIm1bnPMRaMgJuSEv/XeEs0EkzvVhWLqcDp5CXU0paTyOyq6jDamEmzQ5vVqylOa9FKvs6FUcd4Y+NzAZnXaRq3yXKDG41Rz3DSd97e5PuD26OaR66JJd5rCWRurNYqjWcO8QxYLG4sxKtzFtg1pkDW+gGicm4hhr6iKSbGcThIkBcDNKco5ltzdtx0WNLgpGHOBqDcAjKfiFSzXxRHguyMKuSGtqZHPlkbJK52ZxN3a3zXPW5VjBi+YBrTa5uU1VQTSHusisNtgVWPidC4F7CD1B0WTTTOlNUah8cGIsaXOEU9tJQL/iOagOw2qZIIZo9L3Eo2IUOlrDHbU+SvKLEXPOVxJHRHDHyuh+mp3RjQkEcyrWCtrxA1jIoHBosHOlIJHmLKE9/dDm6grK8RtLa0StNs7dbcyt4z+M58kPk7NpLW4kC0CnpSXf9U6JDqnFOVPTf1Cuch7x9p34pQnlG0j/AMVW6M9ub2SqxNrS50FMABc/WFBYP2mf9s/+YoI3Q1gQkokNOZS2Mc491pXF2bieSlVIvTssL/8AxOQYe5+riQFKDYoLZn5/LYLWMXTslsgU1HPIQY+75qeYoWW9od2kjRoxmg/FGx0s4EcTcrB5KdT0ccVneJx5laQgJsRDTmZre3tHF+ybpdWcUbY2BkbcoGw6Jprf1tU6zrYLeKohjsehN7bKLPC2aJ0bgdNR5FSADlJ03SBpICFQiJQ1JB7CocQ5ujXfNTywjxFV+IwgHtWDT8k5h9YCGwzkX2a4lQm1wwa4sflp2SjvAfgoE+EtdrHofLRXBZboiLUnFPsSkZaow6WM9ffZIoWPjmcZGloy2vy3Wqc0EatB9Qo8lLC/7OU9WhZ/GrsvyK4Wtmve6rcR78XcN9dwribDnHRjszefIqL7CIwA2QtIGz2ptNqgTIEMQMTZHCxtqpdM4B2h1TXZTscWvGaM/aCTJeI3bssKpm6dovKeosLOKr+ImCSmbM3djtR5HRMQ1Z5qVKDPC5g2c2ybfAjNkX8INkMoy6uselkp4kacrhYt0KQd9lAAtyCCAvyCCQFjBQ35X8zspYZBC3vWc4dNlFkqjKO5cDpyQbE6Sw/sFuqXRnz7HX1T5bti28tk7TUTpLOfqN0/S0gaQX6/kplwNGBaKHtktgja2JgawWS2pItzR3aOZWgh0Jy2UbhNNt1KVp1TAdBNgAmX3tdOEgAa/wB028ttuUAGActtDbkVWVcXYyAtsWOF/RWDXC6RNCJMzHc9ipkgQKCu8MMx02a4/mrLIs0MzTZwsdrKzw+utlhmJt9h19vIpRkTKPtFgR1SCbbJ15FtUw89LpiiEXX3QJzDVIJ8kYcRs1yChqWljcDlGU9W6Kor4XQuDT3mv2crzP8AuuTVQ1s0bo3xEhw5/FTKKaKjKjNBpYd1ZUFQ0Wa6xUCoa+KVzJN2myjSVBh8O/JcvRuO48AyqD4nCzx3h5qJEY3NJcTccgFHllfK7M8m6JjrG6mxEtwba7SLfq80EqOGV7A+PJIPI6oKqJsm08Rdo0ADqrKCFrBe2vVNxMyjonc2i6oqjGx0u5BBpTQN0sGyoB26G9ki6W1MY6Ci5pN0Y1TAWTdJKBNkjMkwCJAGqXmEjLg6hN31RtPJIZFrWF1pWAd3QnZRmm+5HoFZi19Ry/FV1VB2MlwPq3bfu+SzaoaLHD8RtaCpsRs155eRVm9gGxuFmOSscPxAgCCd2n2XH81Sd8MiUfaJ7tEV069gAFtUyRZUCYmaSOKJ0kzg1jRcm6q/0/Rh1gZPXKpGLwGfD5mjcDMPcsaT5LHJklHo0jFMkzVskrnudqXuJv0UUkk3JugguduzUCAQQSAl0EwjeWvdla7nbZBRLo1Sm0S42agHVLBTTSnAbLsOcWErRIujugYu6WCmwUrMgBd0oFM5kprtE7GOFyTdJJ6orosBRPRAO1tzSSivqgB07XRua2eEtPvum2uQD8kl+STAri1zHuY61whbRScRYJGmSN4L2bgDcKAx5LdVmUWtDX9nlhmJyHQOPLyVmQsyp1BXujIhnd3Bo13TyVKQmizcOqymL4W+ke6WIF0BPvb6rVuPVNyNbIxzHi7SCD5onHyQRdMwiCerKd1JUPhduDoeo6phcb4NwIIIIANBEggDStTgN0EF3HKLBQBuUEEDHAiJ1QQQALoye77wggkAZO6LkjQTGEUm+qCCTAUHaozqgggA4D3yba2UOqiEU9maBwuPJEgoY0MX1IHJJOqCCTGiywyqfcU8neFtDzCsHagE9EEFcXwJ9lFxJTtdGyoGjgch8ws8ggufLxI1h0BBBBZlAQQQQB//2Q==" alt="" />
      </div>
      <p className="UserNameProfile pt-3">
        {name}
      </p>
      </div>
      
      <div className="UserLeftBottom courseCard py-5">
        <div className={selectedComponent === 'profile' ? 'ps-4 mb-2 active-sidebar-content' : 'UserProfileOptions ps-4 mb-2'} onClick={() => handleComponentClick('profile')} id='userProfile'>Profile</div>
        <div className={selectedComponent === 'documents' ? 'ps-4 mb-2 active-sidebar-content' : 'UserProfileOptions ps-4 mb-2'} onClick={() => handleComponentClick('documents')} id='userDocuments'>Documents</div>
        <div className={selectedComponent === 'password' ? 'ps-4 mb-2 active-sidebar-content' : 'UserProfileOptions ps-4 mb-2'} onClick={() => handleComponentClick('password')} id='userPasswordReset'>Reset Password</div>
        <div className={selectedComponent === 'courses' ? 'ps-4 mb-2 active-sidebar-content' : 'UserProfileOptions ps-4 mb-2'} onClick={() => handleComponentClick('courses')} id='userCourses'>Courses</div>
      </div>

    </div>
            </div>
        
        <div className="UserProfilePageRight col-md-8">
        {/* <MainContent className=""></MainContent> */}
        {/* {showProfile && <ProfileForm name = {name} setname = {setname} email = {email} hodemail = {HodEmail} domain = {domain} eduqua = {eduQualification} address = {address} institute = {institute} phone = {phone}></ProfileForm>}
        {showDocuments && <MyDocuments></MyDocuments>} 
        {showCourses && <MyCourses arrayProp={myCourses}></MyCourses>}
        {showPassword && <Reset></Reset>} */}
        {value === 'profile' && <ProfileForm name = {name} setname = {setname} email = {email} hodemail = {HodEmail} domain = {domain} eduqua = {eduQualification} address = {address} institute = {institute} phone = {phone}></ProfileForm>}
        {value === 'documents' && <MyDocuments></MyDocuments>}
        {value === 'courses' && <MyCourses arrayProp={myCourses}></MyCourses>}
        {value === 'password' && <Reset></Reset>}
        </div>
        
        </div>
    </div>
  )
}
