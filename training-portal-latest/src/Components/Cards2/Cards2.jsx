import React from 'react';
import "./Cards2.css"

export default function Cards2() {
  return <div className='cardsTop'>
 <div className="container">
        <h3 className='cardsHeader'>Course Categories</h3>
        <div className="cards row gy-5">

        <div class="col">
            <div class="card card2 courseCard">
                <img src={require("../../Images/Mask.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">HPC</h5>
                    </div>
            </div>
        </div>

        <div class="col">
            <div class="card card2 courseCard">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">Machine Learning</h5>
                    </div>
            </div>
        </div>
        
        <div class="col">
            <div class="card card2 courseCard">
                <img src={require("../../Images/Mask.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">Deep Learning</h5>
                    </div>
            </div>
        </div>

        <div class="col">
            <div class="card card2 courseCard">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">GPU</h5>
                    </div>
            </div>
        </div>
        {/* <div class="col-md-6 col-lg-4 col-xl-3 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">HPC</h5>
                    </div>
            </div>
        </div>

        <div class="col-md-6 col-lg-4 col-xl-3 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">Machine Learning</h5>
                    </div>
            </div>
        </div>
        
        <div class="col-md-6 col-lg-4 col-xl-3 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">Deep Learning</h5>
                    </div>
            </div>
        </div>

        <div class="col-md-6 col-lg-4 col-xl-3 mx-auto mt-3">
            <div class="card card2">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title">GPU</h5>
                    </div>
            </div>
        </div> */}

        </div>
    </div>
  </div>;
}
