import express from "express";

const router = express.Router();

const bodyParser = require("body-parser");
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

const multer = require("multer");
const path = require("path");

const Client = require('ssh2-sftp-client');
const sftp = new Client();

router.use(express.static("public"));

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(
            null,
            path.join(__dirname, "../public/userImages"),
            function (error, success) {
                if (error) throw err;
            }
        );
    },
    filename: function (req, file, cb) {
        const name = Date.now() + "-" + file.originalname;
        cb(null, name, function (error1, success1) {
            if (error1) throw error1;
        });
    },
});

const upload = multer({ storage: storage });

// controllers
import {
    bulkRegisterExcelUpload,
    getExcelFormatDocument
} from "../controllers/excelFormat";

router.post("/admin/excel-format-upload", upload.single("excel"), bulkRegisterExcelUpload);
router.get("/admin/get-excel-format-document/:excelFormatId", getExcelFormatDocument);

module.exports = router;