import React, { useState } from 'react'
import { useEffect } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import "./AllCourses.css"



  

export default function AllCourses() {

  const [allcourses , setAllCourses] = useState([]);
  const navigate = useNavigate()




  useEffect(() => {
    // const userFromSessionStorage = sessionStorage.getItem('userId');
    const url = "http://localhost:8000/api/all-courses-published"
    axios.get(url).then((response) => {
      console.log(response.data)
      setAllCourses(response.data)
    })
      .catch((error) => {
        console.log(error.data)
      });
    },[]);


    const handleClick = (item) => {
      // console.log(item)
      sessionStorage
        .setItem("selectedCourseId",item)
        navigate('/courseEnroll');
    };

  return (
    <div className='AllCoursesTop pt-5'>
      <h2 className='text-center'>AllCourses</h2>

      <div className="container mt-5">

      <div className="row row-cols-2 gx-5">
      {allcourses && allcourses.map((course)=>(
        <div className='CourseInfoCardAll col'>
        <div className="EachEnrolledCourse d-flex courseCard">

<img className="courseImageTest" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToQeYeClZMrqqq2aaTT2UdbuWtVktz8jQ9gw&usqp=CAU" alt=""  />

<div className="enrolledCourseInfo" onClick={() => handleClick(course._id)}>
  <div className="enrolledCourse">
  <div className="courseMaintitle mb-2">
  {course.name}
  </div>
  <span className="enrolledCourseSubtext">
    {course.category}
  </span>
  </div>
</div>

<div className="nextPageIcon">
<i class="fa-solid fa-angle-right"></i>
</div>
</div>
</div>
      ))
      }
      {/* {allcourses && allcourses.map((course)=>(
        <div className='CourseInfoCardAll col'>
          <div className='border border-primary mb-2 d-flex justify-content-between courseCard'>
          <div>
          <div className="courseName">
            {course.name}
          </div>
          <div className='coursePrice'>
            {course.price}
          </div>
          </div>
          <div className="nextPageIcon mx-5">
          <i class="fa-solid fa-forward" onClick={() => handleClick(course._id)}></i>
          </div>
          </div>
        </div>
      ))
      } */}
      </div>

              
      </div>
      
    </div>
  )
}
