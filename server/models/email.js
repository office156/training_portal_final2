// emailSchema.js
import mongoose from "mongoose";
const { ObjectId } = mongoose.Schema.Types;
import User from "./user.js";
import Course from "./course.js";

const emailSchema = new mongoose.Schema({
  sender: {
    type: String,
    required: true,
  },
  subject: {
    type: String,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
}, { timestamps: true });


export default mongoose.model("Email", emailSchema);
