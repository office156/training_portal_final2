import React from 'react';
import "./Cards.css"

export default function Cards() {
  return <div className='cardsTop'>
 <div className="container">
        <h3 className='cardsHeader'>Categories</h3>
        <div className="cards">
            <div class="card">
                <img src={require("../../Images/Mask.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title">HPC</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
            </div>

            <div class="card">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title">Machine Learning</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
            </div>

            <div class="card">
                <img src={require("../../Images/Mask.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title">Deep Learning</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
            </div>

            <div class="card">
                <img src={require("../../Images/Mask2.png")} class="card-img-top" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title">GPU</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
            </div>

        </div>
    </div>
  </div>;
}
