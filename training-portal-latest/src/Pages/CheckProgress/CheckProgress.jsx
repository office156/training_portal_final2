import React from 'react'
import SidebarInstructor from '../../Components/SidebarInstructor/SidebarInstructor'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { useRef } from 'react';
import { CircularProgress, Button } from '@mui/material';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import { useState } from 'react';

function TablePaginationActions(props) {
    const theme = useTheme();
    const { count, page, rowsPerPage, onPageChange } = props;
    
  
  
    const handleFirstPageButtonClick = (event) => {
      onPageChange(event, 0);
    };
  
    const handleBackButtonClick = (event) => {
      onPageChange(event, page - 1);
    };
  
    const handleNextButtonClick = (event) => {
      onPageChange(event, page + 1);
    };
  
    const handleLastPageButtonClick = (event) => {
      onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };
  
    return (
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <IconButton
          onClick={handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="first page"
        >
          {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton
          onClick={handleBackButtonClick}
          disabled={page === 0}
          aria-label="previous page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
        </IconButton>
        <IconButton
          onClick={handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="next page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </IconButton>
        <IconButton
          onClick={handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="last page"
        >
          {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </Box>
    );
  }
  
  TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onPageChange: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
  };

export default function CheckProgress() {

    const [rows, setRows] = useState([])
    const [submissions, setsubmissions] = useState([])
    const [selectedLesson, setselectedLesson] = useState("")
    const navigate = useNavigate();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);
    const courseFromSessionStorage = sessionStorage.getItem('selectedCourseId');
    const token = sessionStorage.getItem('Token');


    const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

    useEffect(() => {

        getAllLessons()

      }, []);
    
      const getAllLessons = () =>{
        // localhost:8000/api/getUserProgress/657058a7ef404eec77e570ab
        const url = "http://localhost:8000/api/getUserProgress/" + courseFromSessionStorage
        // const url2 = "http://localhost:8000/api/coursedetails/" + courseFromSessionStorage
        axios.get(url).then((response) => {
          setRows(response.data)
          console.log(response.data)
        })
          .catch((error) => {
            toast.error(error)
          });
      }

      const getAllSubmissions = (id) =>{
        setselectedLesson(id)
        const url = "http://localhost:8000/api/course/" + courseFromSessionStorage + "/lesson/" + id + "/assignment-details"
        // const url2 = "http://localhost:8000/api/coursedetails/" + courseFromSessionStorage
        axios.get(url,{
          headers: {
            'Authorization': `Bearer ${token}`,
          }
        }).then((response) => {
          setsubmissions(response.data.submissions)
          console.log(response.data)
        })
          .catch((error) => {
            toast.error(error)
          });
      }

      const handleDownloadPDF = (id) => {
        

        // /course/:courseId/lesson/:lessonId/submission/:submissionId/download-submission
        const downloadUrl = 'http://localhost:8000/api/course/' + courseFromSessionStorage + "/lesson/" + selectedLesson + "/submission/" + id + "/download-submission"
    
        axios({
          url:downloadUrl ,
          method:'GET',
          responseType: 'blob',
        }).then((response)=>{
          // console.log(response.data);
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute('download', 'submission.pdf');
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
    
        }).catch((err)=>{
          console.error('Error downloading the file:', err);
        });
      };

  return (
    <div className='userListTop'>
    <SidebarInstructor className="adminPanelSidebartag"></SidebarInstructor>
    <div className="userListSecond">
        <div className="topLineUserLIstPage py-5 container d-flex justify-content-between">
          <div className="headingUserList">
            Progress
          </div>
        </div>
        <div className="tableUsers container d-flex justify-content-center">
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>User</TableCell>
                  <TableCell align="right">Progress</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {(rowsPerPage > 0
                  ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  : rows
                ).map((row) => (
                  <TableRow
                    key={row.userId}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align="right">
                      {row.progress} %
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    SelectProps={{
                      inputProps: {
                        'aria-label': 'rows per page',
                      },
                      native: true,
                    }}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                    ActionsComponent={TablePaginationActions}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </TableContainer>
        </div>


        {/* <div className='container pt-5'>
            <h4>Submissions</h4>
            <div>
            <ul>
        {submissions && submissions.map(item => (
          <li key={item.id}>{item.user} <button className='ms-5' onClick={()=>{
            handleDownloadPDF(item._id)
          }}>{item.file}</button></li>
        ))}
      </ul>
            </div>
        </div> */}
      
      </div>
        </div>
  )
}
