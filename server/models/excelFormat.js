import mongoose from "mongoose";
const { Schema } = mongoose;

const excelFormatSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            trim: true,
            required: true,
        },
        excel: {
            type: String,
        }
    }
);

export default mongoose.model("ExcelFormat", excelFormatSchema);
