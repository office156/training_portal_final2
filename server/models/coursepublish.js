const mongoose = require('mongoose');

const coursePublishSchema = new mongoose.Schema({
  courseId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Course',
    required: true,
  },
//   instructorId: {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: 'User',
//     required: true,
//   },
  adminApproval: {
    type: Boolean,
    default: false,
  },
  requestDate: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.model("CoursePublishSchema", coursePublishSchema);
