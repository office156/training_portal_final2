import React from 'react';

function CourseDetails({ courseData }) {
  return (
    <div className='container'>
      <p>Course name : {courseData.name}</p>
      <p>{courseData.description}</p>
      <p>Instructor: {courseData.instructor.name}</p>
      <p>Category: {courseData.category}</p>
      <p>Price: {courseData.price}</p>
      <p>What You Will Learn:</p>
      <ul>
        {courseData.whatyouwilllearn.map((item, index) => (
          <li key={index}>{item}</li>
        ))}
      </ul>
      <p>Prerequisites:</p>
      <ul>
        {courseData.prerequisites.map((item, index) => (
          <li key={index}>{item}</li>
        ))}
      </ul>
      <p>Published: {courseData.published ? 'Yes' : 'No'}</p>
      <p>Request Date: {courseData.requestDate}</p>
      <p>Requested: {courseData.requested ? 'Yes' : 'No'}</p>
      {/* <p>Course Image: <img src={courseData.image} alt="Course" /></p> */}
      {/* Add more properties as needed */}
    </div>
  );
}
        

export default CourseDetails;
