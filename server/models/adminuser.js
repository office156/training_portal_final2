import mongoose from "mongoose";
const { Schema } = mongoose;

const adminUserSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      required: true,
    },
    email: {
      type: String,
      trim: true,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      min: 8,
      max: 64,
    },
  },
  { timestamps: true }
);

export default mongoose.model("AdminUser", adminUserSchema);
