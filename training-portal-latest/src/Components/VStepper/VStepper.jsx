import React from 'react'

export default function VStepper() {
  return (
    <div className='row'>
        <div className="col-1 text-center border">1</div>
        <div className="col">
            <div>
                <div>
                Foundation (2 Courses)
                </div>
                <div>
                These foundation courses cover introductory part about HPC domain
                </div>
                <div className="row d-flex">
                <div class="col">
            <div class="card card2 CardCustomization">
                <img src={require("../../Images/Mask2.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title VerticalStepperCardTitle">CUDA programming</h5>
                        <p className='VerticalStepperCardSubtext'>CDAC</p>
                    </div>
            </div>
        </div>
        <div class="col">
            <div class="card card2 CardCustomization">
                <img src={require("../../Images/Mask2.png")} class="card-img-top learningPathImages" alt="..."></img>
                    <div class="card-body">
                        <h5 class="card-title card2-title VerticalStepperCardTitle">SPACK utility</h5>
                        <p className='VerticalStepperCardSubtext'>CDAC</p>
                    </div>
            </div>
        </div>
                </div>
            </div>
        </div>
    </div>
  )
}
