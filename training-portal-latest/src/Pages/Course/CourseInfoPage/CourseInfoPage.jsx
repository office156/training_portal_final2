import React, { useEffect, useRef, useState } from 'react'
import "./CourseInfoPage.css"
import { Link, useHistory, useNavigate } from 'react-router-dom';
import AboutCourse from '../../../Components/Courses/AboutCourse';
import CourseDiscussion from '../../../Components/Courses/CourseDiscussion';
import CourseLearning from '../../../Components/Courses/CourseLearning';
import CourseAnnouncements from '../../../Components/Courses/CourseAnnouncements';
import { ToastContainer, toast } from 'react-toastify';

import axios, * as others from 'axios'; 
import { PDFViewer } from "@react-pdf/renderer";
import LinearProgressWithLabel from '../../../Components/LinearProgressBar/ProgressBar';
import MyComponent from '../../MyComponent/MyComponent';


export default function CourseInfoPage() {
  
  
  
  const [courseInfoArray, setCourseInfoArray] = useState([]);
  const courseIdFromSessionStorage = sessionStorage.getItem('selectedCourseId');
  const userIdFromSessionStorage = sessionStorage.getItem('userId');
  const tokenFromSessionStorage = sessionStorage.getItem('Token');

  const [selectedLesson, setSelectedLesson ] = useState('');
  const [selectedComponent, setSelectedComponent] = useState('about');
  const [value, setvalue] = useState('about');
  const [progress, setprogress] = useState(0);
  const [pdf , setpdf] = useState(null);
  const fileInputRef2 = useRef(null);
  const [updateLesson,setupdateLesson] = useState();
  const [title,settitle] = useState();
  const [description,setdescription] = useState();
  const [assignmentPdf,setassignmentPdf] = useState(null);

  const [completedLessons, setCompletedLessons] = useState([]);
  const [finalAssign, setfinalAssi] = useState({});
  const [certificate , setCertificate] = useState('');
  const navigate = useNavigate();

  const handleComponentClick = (componentName) => {
    setSelectedComponent(componentName);
    setvalue(componentName);
  };



  useEffect(() => {
    getAllLessons()
    getLessonsStatus()
    checkCertificate()
  }, []);

  const getAllLessons = () =>{
    const url = "http://localhost:8000/api/course/" + courseIdFromSessionStorage + "/getlessons";
    // console.log(url);
      axios.get(url, {
        headers: {
          'Authorization': `Bearer ${tokenFromSessionStorage}`,
        }
      })
      .then((response) => {
        // console.log(response);
        console.log(response.data);
        setCourseInfoArray(response.data);
        
        if (response.data.length > 0) {
          const firstLessonId = response.data[0]._id;
          setSelectedLesson(`http://localhost:8000/api/course/${courseIdFromSessionStorage}/lesson/${firstLessonId}/video`);
        }
      })
      .catch((error) => {
        console.log(error + " errrrr");
      })
  }

  const getLessonsStatus = () =>{
    const url = "http://localhost:8000/api/getLessonCompletion/" + courseIdFromSessionStorage + "/" + userIdFromSessionStorage;
    // console.log(url);
      axios.post(url, {
        headers: {
          'Authorization': `Bearer ${tokenFromSessionStorage}`,
        }
      })
      .then((response) => {
        // console.log(response);
        console.log(response.data);
        setfinalAssi(response.data.finalAssignment)
        setCompletedLessons(response.data.allLessons);
        setprogress(response.data.percentageComplete)

      })
      .catch((error) => {
        console.log(error + " errrrr");
      })
  }

  const AddAssignment = async (id) => {
    
    const dataTest = {"file":pdf};
  console.log(dataTest);
  const url = "http://localhost:8000/api/course/" + courseIdFromSessionStorage + "/lesson/" + id +"/upload"
  await  axios.post(url, dataTest ,  {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${tokenFromSessionStorage}`
      }
    }).then((response) => {
      console.log(response)
      setpdf(null);
      fileInputRef2.current.value = '';
      toast.success("Answer uploaded successfully.")

      getAllLessons()

    })
    .catch((error)=>{
      toast.error(error.response.data.message)
      console.log(error);
    });



  
};

  const markAsComplete = async(lessonId) =>{
    // :courseId/:lessonId/:isCompleted
    const url = "http://localhost:8000/api/togglelessoncompletion/" + courseIdFromSessionStorage + "/" + lessonId + "/" + userIdFromSessionStorage;
    // console.log(url);
     await axios.post(url, {
        headers: {
          'Authorization': `Bearer ${tokenFromSessionStorage}`,
        }
      })
      .then((response) => {
        console.log(response.data)
        setprogress(response.data.percentageComplete)
        setCompletedLessons(response.data.completedLessonsArray)
      })
      .catch((error) => {
        console.log(error + " error");
        
      })
      console.log("marking ...")
  } 


  const renderList = () => {
    return courseInfoArray.map((lesson, index) => {
      
      let isCompleted = false
      if(completedLessons.find(item => item["lesson"] === lesson._id)){
        isCompleted = completedLessons.find(item => item["lesson"] === lesson._id).completed;
      }
      
      // console.log(completedLessons)
      // let isCompleted=false;

      // const result = data.find(item => item[conditionKey] === conditionValue);

      // const extractedVariable = result ? result.age : null;

      const dictionaryLength = Object.keys(lesson.assignment).length;
      // console.log(dictionaryLength)

      return (
        <>
        <div key={index} className="LessonInfo py-3 ">
          <input
            type="checkbox"
            checked={isCompleted}
            onChange={() => markAsComplete(lesson._id)}
          />
          <span className="lessonNo">{index + 1} : </span>
          <span
            onClick={() => {
              handleLessonVideoClick(lesson);
            }}
            className="FooterNavbarCommonLinks lessonTitle"
          >
            {lesson.title}
          </span>
          <i
            className="fa-solid fa-cloud-arrow-down FooterNavbarCommonLinks"
            onClick={() => handleDownloadPDF(lesson)}
          ></i>
        </div>
        <div>
        {/* <i class="fa-solid fa-file-word"></i> */}
        {dictionaryLength > 1 ? <div className='text-center border my-2 w-100 d-flex justify-content-between'
        >

          <button data-bs-toggle="modal" data-bs-target="#AddassignmentModal" onClick={()=>{
setupdateLesson(lesson._id)
settitle(lesson.assignment.title)
setdescription(lesson.assignment.description)
        }
          
        }>
            Assignment
          </button>

          {lesson.assignment.assignmentPdf && <button className='border border-none' onClick={() => handleDownloadPDFAssignment(lesson)}>
          <i className="fa-solid fa-cloud-arrow-down FooterNavbarCommonLinks border-none"></i>
          </button>}

        </div> : <></>}

        {lesson.quiz && <button onClick={()=>{
          sessionStorage.setItem('selectedLesson',lesson._id);
          navigate('/play-quiz')
        }}>Quiz</button>}
        {/* {dictionaryLength > 1 ? <p className='text-center border mt-2' data-bs-toggle="modal" data-bs-target="#AddassignmentModal" onClick={()=>{
setupdateLesson(lesson._id)
settitle(lesson.assignment.title)
setdescription(lesson.assignment.description)
setassignmentPdf(lesson.assignment.assignmentPdf)
        }
          
        }>Assignment<i class="fa-solid fa-file-word"></i></p> : <></>} */}
        </div>
        </>
      );
    });
  };

  const handleLessonCompletionToggle = (lessonId) => {
    setCompletedLessons((prevState) => ({
      ...prevState,
      [lessonId]: !prevState[lessonId],
    }));
    console.log("you clicked handleLessonCompletionToggle")
    markAsComplete(lessonId)
  };


  const handleDownloadPDF = (lesson) => {
    console.log("Lesson ID "+lesson._id);

    const downloadUrl = 'http://localhost:8000/api/course/' +courseIdFromSessionStorage +'/lesson/'
    + lesson._id+'/pdf' ; 

    axios({
      url:downloadUrl ,
      method:'GET',
      responseType: 'blob',
    }).then((response)=>{
      // console.log(response.data);
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute('download', 'file.pdf');
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);

    }).catch((err)=>{
      console.error('Error downloading the file:', err);
    });
  };

  const handleDownloadPDFAssignment = (lesson) => {
    
    // /course/:courseId/lesson/:lessonId/download-assignment

    console.log("you clicked download 2")
    const downloadUrl = 'http://localhost:8000/api/course/' +courseIdFromSessionStorage +'/lesson/'
    + lesson._id +'/download-assignment' ; 

    axios({
      url:downloadUrl ,
      method:'GET',
      responseType: 'blob',
    }).then((response)=>{
      // console.log(response.data);
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute('download', 'file.pdf');
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);

    }).catch((err)=>{
      console.error('Error downloading the file:', err);
    });
  };
  const handleDownloadPDFFinalAssignment = () => {
    
    // /course/:courseId/lesson/:lessonId/download-assignment

    console.log("you clicked download final 2")
    const downloadUrl = 'http://localhost:8000/api/course/' +courseIdFromSessionStorage + '/download-assignment' ; 

    axios({
      url:downloadUrl ,
      method:'GET',
      responseType: 'blob',
    }).then((response)=>{
      // console.log(response.data);
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute('download', 'file.pdf');
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);

    }).catch((err)=>{
      console.error('Error downloading the file:', err);
    });
  };

  const handleLessonVideoClick = (lesson) => {
    // console.log("THis is the video" + lesson._id);
    const lessonId = lesson._id;
    // console.log("http://localhost:8000/api/course/" + courseIdFromSessionStorage +"/lesson/" + lessonId + "/video");
    setSelectedLesson("http://localhost:8000/api/course/" + courseIdFromSessionStorage + "/lesson/" + lessonId + "/video")
  };


  const generateCertificate = async() =>{
    console.log("Generate certificate")
    const data = { "name" : "name", "description" : "description"};
    const url = "http://localhost:8000/api/course/" + courseIdFromSessionStorage+"/user/" + userIdFromSessionStorage  +"/generate-certificate"
    
    await  axios.post(url, data ,  {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${tokenFromSessionStorage}`
      }
    })
      .then((response) => {
        console.log(response.data)
        toast.success("Requested for certificate successfully.")
      })
      .catch((error) => {
        console.log(error);
        toast.error(error.response.data.error);
      })
  }

  const checkCertificate = async() =>{
    console.log("Check certificate")
    const data = { "name" : "name", "description" : "description"};
    const url = "http://localhost:8000/api/course/" + courseIdFromSessionStorage+"/user/" + userIdFromSessionStorage  +"/check-certificate"
    
    await  axios.post(url, data ,  {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': `Bearer ${tokenFromSessionStorage}`
      }
    })
      .then((response) => {
        console.log(response.data)
        setCertificate(response.data.message)
      })
      .catch((error) => {
        console.log(error);
        toast.error(error.response.data.error);
      })
  }

  const downloadCertificate = () => {
    axios({
      url: 'http://localhost:8000/api/dummycertificate',
      method: 'POST',
      responseType: 'blob',
    }).then((response) => {
      console.log(response)
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'certificate.pdf');
      document.body.appendChild(link);
      link.click();
    });
  };

  


  // const Component1 = () => {
  //   return (
  //     <div className={selectedComponent === 'Component1' ? 'selected' : ''} onClick={() => handleComponentClick('Component1')}>
  //       Component 1
  //     </div>
  //   );
  // };

  // const Component2 = () => {
  //   return (
  //     <div className={selectedComponent === 'Component2' ? 'selected' : ''} onClick={() => handleComponentClick('Component2')}>
  //       Component 2
  //     </div>
  //   );
  // };


  return (
    <div className='CourseInfoPageTop'>
      <p className="CourseInfoHeader text-center mb-5">
        Introduction to High performance computing and parallel programming
      </p>
      <div className="CourseInfoMaincontent">
        <div className="row w-100 m-0">
          <div className="CourseInfoAndVideo col-lg-8 p-0">
            <div className="courseJustVideo w-100 text-center">
              { 
                selectedLesson &&  
                <video className="CourseInfoVideo courseLessonVideo" src={selectedLesson} controls autoPlay></video>
              }
            </div>

            <div className="CourseInfoPageNav pt-2 d-flex justify-content-around">
              <div className={selectedComponent === 'about' ? 'selected' : 'about'} onClick={() => handleComponentClick('about')}>
                About
              </div>
              <div className={selectedComponent === 'discussion' ? 'selected' : 'discussion'} onClick={() => handleComponentClick('discussion')}>
                Discussion
              </div>
              <div className={selectedComponent === 'learning' ? 'selected' : 'learning'} onClick={() => handleComponentClick('learning')}>
                Learning Materials
              </div>
              <div className={selectedComponent === 'announcements' ? 'selected' : 'announcements'} onClick={() => handleComponentClick('announcements')}>
                Announcement
              </div>
            </div>

<div className='mt-5  p-2 text-center progressbardiv mx-3'>
  <span className='progresstitle'><b>Course  Progress</b></span>
  <LinearProgressWithLabel className='my-4' value={progress} />
</div>


            <div className="SelectedComponentValue pt-4">
              {value === 'learning' && <CourseLearning></CourseLearning>}
              {value === 'discussion' && <CourseDiscussion></CourseDiscussion>}
              {value === 'about' && <AboutCourse></AboutCourse>}
              {value === 'announcements' && <CourseAnnouncements />}
            </div>
          </div>
          

          <div className="CourseInfoLessons col-lg-4 d-flex flex-column justify-content-between">
            <div>
            <p className="LessonsHeader">
              Lessons
            </p>
            {renderList()}
            </div>

            {/* <div>
              {finalAssign && Object.keys(finalAssign).length > 0 ? (
        <button className='w-100 my-2' data-bs-toggle="modal" data-bs-target="#AddfinalassignmentModal">Submit Final Assignment</button>
      ) : (
        <p></p>
      )}
            </div> */}
            { progress == 100 ? <div className='text-center d-flex justify-content-center mb-5'>
              {certificate === "pending" ?
        <p className='text-success'><u> Already requested for certicate.</u></p> : certificate == "approved" ? <div><p>Congratulations!</p><button className='text-center button-63' onClick={downloadCertificate}> <i class="fa-solid fa-certificate me-2"></i> Download certificate</button></div> :
        <div> <p>Course completed</p> <button class="mb-3 button-63" onClick={generateCertificate}> <i class="fa-solid fa-certificate me-2"></i> Request for certificate</button></div>
      }
            </div> : <></> }

            

          </div>
        </div>
      </div>

              {/* <!--Add assignment Modal --> */}
              <div class="modal fade" id="AddassignmentModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Add answer</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Title:</label>
            <span className='ms-5'>{title}</span>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Description:</label>
            <span className='ms-5'>{description}</span>
          </div>
          {/* <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Download assignment pdf:</label>
            <span className='ms-5'>dummy7 text <button className='ms-2 border p-2' onClick={()=>{
              console.log("clicked on download")
              handleDownloadPDFAssignment()
            }}><i class="fa-solid fa-download"></i></button></span>
          </div> */}
          <div class="mb-3">
            <label for="message-text" class="col-form-label">Upload your answer:</label>
            <input type="file" name="" id="" className='form-control' onChange={e => setpdf(e.target.files[0])} ref={fileInputRef2}/>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        {/* <button type="button" class="" data-bs-dismiss="modal">Close</button> */}
        <button type="button" class="" data-bs-dismiss="modal" onClick={()=>{
          console.log("You clicked add assignment")
          AddAssignment(updateLesson);
        }}>Add</button>
      </div>
    </div>
  </div>
</div>

              {/* <!--Add final assignment Modal --> */}
              <div class="modal fade" id="AddfinalassignmentModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Add final answer</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Title:</label>
            <span className='ms-5'>{finalAssign.title}</span>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Description:</label>
            <span className='ms-5'>{finalAssign.description}</span>
          </div>
          <div class="mb-3">
            <label for="recipient-name" class="col-form-label">Download assignment pdf:</label>
            <span className='ms-5'><button className='ms-2 border p-2' onClick={()=>{
              console.log("clicked on download")
              handleDownloadPDFFinalAssignment()
            }}><i class="fa-solid fa-download"></i></button></span>
          </div>
          <div class="mb-3">
            <label for="message-text" class="col-form-label">Upload your answer:</label>
            <input type="file" name="" id="" className='form-control' onChange={e => setpdf(e.target.files[0])} ref={fileInputRef2}/>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        {/* <button type="button" class="" data-bs-dismiss="modal">Close</button> */}
        <button type="button" class="" data-bs-dismiss="modal" onClick={()=>{
          console.log("You clicked add assignment")
          AddAssignment(updateLesson);
        }}>Add</button>
      </div>
    </div>
  </div>
</div>


<ToastContainer
position="top-right"
autoClose={5000}
hideProgressBar={false}
newestOnTop
closeOnClick
rtl={false}
pauseOnFocusLoss
draggable
pauseOnHover
theme="dark"
/>

    </div>
  )
};
